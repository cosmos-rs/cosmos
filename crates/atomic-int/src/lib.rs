//! Traits for atomic integer
#![warn(rustdoc::missing_crate_level_docs)]
#![warn(missing_debug_implementations)]
#![deny(warnings)]
#![deny(clippy::pedantic)]

#[doc(hidden)]
mod deps {
    pub use ::num_traits;
    #[cfg(feature = "serde")]
    pub use ::serde;
}

use crate::deps::num_traits::PrimInt;

use std::sync::atomic::{
    AtomicI16,
    AtomicI32,
    AtomicI64,
    AtomicI8,
    AtomicIsize,
    AtomicU16,
    AtomicU32,
    AtomicU64,
    AtomicU8,
    AtomicUsize,
    Ordering,
};


pub trait AtomicInt
where
    Self: private::Sealed
        + std::fmt::Debug
        + std::default::Default
        + std::convert::From<<Self as AtomicInt>::Int>
        + std::panic::RefUnwindSafe
        + std::panic::UnwindSafe
        + std::marker::Send
        + std::marker::Sync
        + std::any::Any
        + std::borrow::Borrow<Self>
        + std::borrow::BorrowMut<Self>
        + std::convert::From<Self>,
{
    #[cfg(not(feature = "serde"))]
    type Int: PrimInt + std::fmt::Debug + std::fmt::Display;
    #[cfg(feature = "serde")]
    type Int: PrimInt
        + std::fmt::Debug
        + std::fmt::Display
        + crate::deps::num_traits::Zero
        + crate::deps::num_traits::One
        + crate::deps::serde::Serialize
        + for<'de> crate::deps::serde::Deserialize<'de>;

    fn set(
        &self,
        n: Self::Int,
    ) -> Self::Int;

    fn value(&self) -> Self::Int;

    fn add(
        &self,
        n: Self::Int,
    ) -> Self::Int;

    fn sub(
        &self,
        n: Self::Int,
    ) -> Self::Int;

    fn dec(&self) -> Self::Int {
        self.sub(<<Self as AtomicInt>::Int as crate::deps::num_traits::One>::one())
    }

    fn inc(&self) -> Self::Int {
        self.add(<<Self as AtomicInt>::Int as crate::deps::num_traits::One>::one())
    }

    fn reset(&self) -> Self::Int {
        self.set(<<Self as AtomicInt>::Int as crate::deps::num_traits::Zero>::zero())
    }

    #[must_use]
    fn min_value() -> Self::Int {
        <<Self as AtomicInt>::Int as crate::deps::num_traits::Bounded>::min_value()
    }

    #[must_use]
    fn max_value() -> Self::Int {
        <<Self as AtomicInt>::Int as crate::deps::num_traits::Bounded>::max_value()
    }
}


pub trait AtomicValue: private::Sealed {
    type Atomic: AtomicInt;
}


macro_rules! impl_atomic_int {
    ($atomic:ident, $prim:ty) => {
        impl AtomicInt for $atomic {
            type Int = $prim;

            #[inline(always)]
            fn set(
                &self,
                n: Self::Int,
            ) -> Self::Int {
                self.swap(n, Ordering::AcqRel)
            }

            #[inline(always)]
            fn value(&self) -> Self::Int {
                self.load(Ordering::Relaxed)
            }

            #[inline(always)]
            fn add(
                &self,
                n: Self::Int,
            ) -> Self::Int {
                self.fetch_add(n, Ordering::Relaxed)
            }

            #[inline(always)]
            fn sub(
                &self,
                n: Self::Int,
            ) -> Self::Int {
                self.fetch_sub(n, Ordering::Relaxed)
            }

            #[inline(always)]
            fn dec(&self) -> Self::Int {
                self.sub(1 as $prim)
            }

            #[inline(always)]
            fn inc(&self) -> Self::Int {
                self.add(1 as $prim)
            }

            #[inline(always)]
            fn reset(&self) -> Self::Int {
                self.set(0 as $prim)
            }
        }

        impl AtomicValue for $prim {
            type Atomic = $atomic;
        }
    };
}

impl_atomic_int!(AtomicI16, i16);
impl_atomic_int!(AtomicI32, i32);
impl_atomic_int!(AtomicI64, i64);
impl_atomic_int!(AtomicI8, i8);
impl_atomic_int!(AtomicIsize, isize);
impl_atomic_int!(AtomicU16, u16);
impl_atomic_int!(AtomicU32, u32);
impl_atomic_int!(AtomicU64, u64);
impl_atomic_int!(AtomicU8, u8);
impl_atomic_int!(AtomicUsize, usize);


mod private {
    use super::{
        AtomicI16,
        AtomicI32,
        AtomicI64,
        AtomicI8,
        AtomicIsize,
        AtomicU16,
        AtomicU32,
        AtomicU64,
        AtomicU8,
        AtomicUsize,
    };
    pub trait Sealed {}
    impl Sealed for AtomicI8 {}
    impl Sealed for AtomicI16 {}
    impl Sealed for AtomicI32 {}
    impl Sealed for AtomicI64 {}
    impl Sealed for AtomicIsize {}
    impl Sealed for AtomicU8 {}
    impl Sealed for AtomicU16 {}
    impl Sealed for AtomicU32 {}
    impl Sealed for AtomicU64 {}
    impl Sealed for AtomicUsize {}
    impl Sealed for i8 {}
    impl Sealed for i16 {}
    impl Sealed for i32 {}
    impl Sealed for i64 {}
    impl Sealed for isize {}
    impl Sealed for u8 {}
    impl Sealed for u16 {}
    impl Sealed for u32 {}
    impl Sealed for u64 {}
    impl Sealed for usize {}
}
