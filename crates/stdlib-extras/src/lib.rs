//! Traits to extend functionality of stdlib types
#![warn(rustdoc::missing_crate_level_docs)]
#![warn(missing_debug_implementations)]
#![deny(warnings)]
#![deny(clippy::pedantic)]

#[macro_use]
pub mod macros;


pub mod any_ext;
pub mod emplace_back;
pub mod iterator_ext;
pub mod split_uint;
pub mod unchecked_index;

pub use self::unchecked_index::{
    UncheckedIndex,
    UncheckedIndexMut,
};

pub use self::emplace_back::{
    EmplaceBack,
    EmplaceBackMut,
};
