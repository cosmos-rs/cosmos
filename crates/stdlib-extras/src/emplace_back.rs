//! # Emplace Back - Initialize a value within a vector

pub trait EmplaceBack<T> {
    fn emplace_back(
        &mut self,
        value: T,
    ) -> &T;
}

pub trait EmplaceBackMut<T> {
    fn emplace_back_mut(
        &mut self,
        value: T,
    ) -> &mut T;
}


impl<T> EmplaceBack<T> for std::vec::Vec<T> {
    #[inline]
    fn emplace_back(
        &mut self,
        value: T,
    ) -> &T {
        let last = self.len();
        self.push(value);
        unsafe { &*self.as_ptr().add(last) }
    }
}


impl<T> EmplaceBackMut<T> for std::vec::Vec<T> {
    #[inline]
    fn emplace_back_mut(
        &mut self,
        value: T,
    ) -> &mut T {
        let last = self.len();
        self.push(value);
        unsafe { &mut *self.as_mut_ptr().add(last) }
    }
}


mod sealed {
    pub trait Sealed {}
    impl<T> Sealed for std::vec::Vec<T> {}
}
