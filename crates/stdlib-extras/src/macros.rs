use std::fmt;

/// Used to get the name of the current function
#[macro_export]
macro_rules! function {
    () => {{
        fn __function__() {}
        let name = $crate::any_ext::type_name_of_val(&__function__);
        let start = (module_path!().len() + 2);
        let end = name.len() - "::__function__".len();
        &name[start..end]
    }};
}



/// get the name of the type of an expression, the current function, or the current module, without
/// the parent module path prefix
///
/// ```ignore
///   let x = Some(23i32)
///   assert_eq!(name_of!(x), "Option<i32>");
///   mod a { mod b { mod c { struct Foo; }}}
///   assert_eq!(name_of!(Foo), "Foo");
/// ```
#[macro_export]
macro_rules! name_of {
    (type $t:ty) => {{
        $crate::macros::TypeName::new(::std::any::type_name::<$t>())
    }};
    (mod, $n:literal) => {{
        let path = module_path!();
        let mut idx = path.len();
        'name_of_mod_search: for _ in 0usize..($n as usize) {
            match &path[..idx].rfind("::") {
                Some(i) => idx = *i,
                None => {
                    idx = 0;
                    break 'name_of_mod_search;
                }
            }
        }
        if idx == 0 || idx == path.len() {
            path
        } else {
            &path[(idx + 2)..]
        }
    }};
    (mod) => {{
        $crate::name_of!(mod, 1)
    }};
    (fn) => {{
        $crate::function!()
    }};
    (crate) => {{
        ::core::env!(
            "CARGO_CRATE_NAME",
            "name_of!(crate) will fail because the CARGO_CRATE_NAME was not available at compile time"
        )
    }};
    ($e:expr) => {{
        $crate::macros::TypeName::new($crate::any_ext::type_name_of_val(&$e))
    }};
}


/// Return the full parent module path of a given fully qualified rust language item name
///
/// ```ignore
/// assert_eq!(parent_module_path("a::b::c::d::FoobaricusMaximus", "a::b::c::d"));
/// ```
pub fn parent_module_path(module_path: &str) -> &str {
    let mut iter = ByteScanner::new(module_path);
    let mut start = 0;
    let mut end = 0;

    // get the next char
    while let Some(ch) = iter.peek() {
        match ch {
            b':' => {
                // looks like the start of a '::' module path
                // so advance and store the current position
                let pinned_position = iter.position();
                iter.advance();

                // then check the next character is ':' as a sanity check
                if iter.advance() != Some(b':') {
                    panic!(
                        "module paths should not contain single ':', path={:?}",
                        module_path
                    );
                }

                end = pinned_position; // this will be the position before the '::'
            }
            b'<' => {
                // if '<' it's the start of a generic type so break and use the last found '::'
                // as the position
                break;
            }
            b'&' if start == iter.position() => {
                // skip the leading & when types are references
                start += 1;
                end = start;
                iter.advance();
            }
            _ => {
                // otherwise advance
                iter.advance();
            }
        };
    }

    &module_path[start..end]
}


#[doc(hidden)]
#[inline(always)]
fn reference_depth(s: &str) -> usize {
    s.bytes().take_while(|ch| *ch == b'&').count()
}

/// TypeName does not do perform nontrivial work immediately. It will only parse
/// the typename when an operation is performed so name_of!() invocations
/// do not eagerly perform string slicing work when not used for logging.
#[derive(Copy, Clone, PartialEq, Eq)]
pub struct TypeName<'a> {
    name: &'a str,
}

impl<'a> TypeName<'a> {
    #[inline]
    pub const fn new<'b>(name: &'b str) -> TypeName<'b> {
        TypeName { name }
    }

    #[inline]
    fn parse(&self) -> ParsedTypeName<'_> {
        let parent_name = crate::macros::parent_module_path(self.name);
        if parent_name.is_empty() {
            ParsedTypeName {
                reference_depth: 0,
                name:            self.name,
            }
        } else {
            let reference_depth = reference_depth(self.name);
            let start = parent_name.len() + 2 + reference_depth;

            ParsedTypeName {
                reference_depth,
                name: &self.name[start..],
            }
        }
    }

    pub const fn as_str(&self) -> &'a str {
        self.name
    }
}


impl fmt::Debug for TypeName<'_> {
    #[inline]
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        self.parse().fmt(f)
    }
}


impl fmt::Display for TypeName<'_> {
    #[inline]
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        self.parse().fmt(f)
    }
}


impl std::cmp::PartialEq<str> for TypeName<'_> {
    #[inline]
    fn eq(
        &self,
        other: &str,
    ) -> bool {
        self.parse().eq(other)
    }
}

impl std::cmp::PartialEq<&str> for TypeName<'_> {
    #[inline]
    fn eq(
        &self,
        other: &&str,
    ) -> bool {
        self.parse().eq(other)
    }
}


#[derive(Copy, Clone, PartialEq, Eq)]
struct ParsedTypeName<'a> {
    reference_depth: usize,
    name:            &'a str,
}


impl fmt::Debug for ParsedTypeName<'_> {
    #[inline]
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        use fmt::Write;
        f.write_char('"')?;
        fmt::Display::fmt(self, f)?;
        f.write_char('"')
    }
}


impl fmt::Display for ParsedTypeName<'_> {
    #[inline]
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        use fmt::Write;

        for _ in 0..self.reference_depth {
            f.write_char('&')?;
        }

        f.write_str(self.name)
    }
}


impl std::cmp::PartialEq<str> for ParsedTypeName<'_> {
    #[inline]
    fn eq(
        &self,
        other: &str,
    ) -> bool {
        let refernce_depth_matches = other.bytes().take(self.reference_depth).all(|ch| ch == b'&');
        refernce_depth_matches && &other[self.reference_depth..] == self.name
    }
}

impl std::cmp::PartialEq<&str> for ParsedTypeName<'_> {
    #[inline]
    fn eq(
        &self,
        other: &&str,
    ) -> bool {
        *self == **other
    }
}

/// An scanner over borrowed bytes with basic
/// operations to facilitate basic string parsing
/// from ascii tokens.
struct ByteScanner<'a> {
    // input
    chars: &'a [u8],
    // the absolute character position within the text
    pos:   usize,
}


impl<'a> ByteScanner<'a> {
    /// create a new `Iter<'_>` from the input string starting
    /// at the start of the string
    pub const fn new(source_text: &'a str) -> Self {
        Self {
            pos:   0,
            chars: source_text.as_bytes(),
        }
    }

    /// Get the current position of the parser
    pub fn position(&self) -> usize {
        self.pos
    }

    /// Peek at the next byte but do not consume it
    pub fn peek(&mut self) -> Option<u8> {
        self.byte_at(self.pos)
    }

    /// consume one byte of the source
    pub fn advance(&mut self) -> Option<u8> {
        if let Some(ch) = self.byte_at(self.pos) {
            self.pos += 1;
            Some(ch)
        } else {
            None
        }
    }

    /// Retrieve the byte at `pos`
    fn byte_at(
        &mut self,
        pos: usize,
    ) -> Option<u8> {
        self.chars.get(pos).copied()
    }
}



#[cfg(test)]
mod test {

    mod a {
        pub mod b {
            pub mod c {
                #[derive(Default)]
                pub struct GenericGarbage<T: 'static>(std::marker::PhantomData<&'static T>);
            }
        }
    }

    #[test]
    fn function_macro() {
        assert_eq!(function!(), "function_macro");
        assert_eq!(name_of!(fn), "function_macro");
        assert_eq!(name_of!(crate::macros::test::function_macro), "function_macro");
    }

    #[test]
    fn name_of_macro() {
        let x: a::b::c::GenericGarbage<(i32, bool)> = Default::default();

        assert_eq!(name_of!(x), "GenericGarbage<(i32, bool)>");
        assert_eq!(name_of!(&x), "&GenericGarbage<(i32, bool)>");
        assert_eq!(name_of!(&&&&x), "&&&&GenericGarbage<(i32, bool)>");

        let x: a::b::c::GenericGarbage<(i32, bool, a::b::c::GenericGarbage<()>)> = Default::default();
        // for the sake of performance, the string is just the full type name with the module truncated
        // so we aren't dynamically allocating anything or doing recursive parsing of the typename string
        // so nontrival nested generics can still have long type names
        assert_eq!(
            name_of!(x),
            "GenericGarbage<(i32, bool, cosmos_stdlib_extras::macros::test::a::b::c::GenericGarbage<()>)>"
        );

        #[derive(Default)]
        struct NestedGarbage<T: 'static>(std::marker::PhantomData<&'static T>);

        let x: NestedGarbage<(char, &str)> = Default::default();
        assert_eq!(name_of!(x), "NestedGarbage<(char, &str)>");

        assert_eq!(
            name_of!(type NestedGarbage<(char, &str)>),
            "NestedGarbage<(char, &str)>"
        );

        assert_eq!(name_of!(fn), "name_of_macro");

        assert_eq!(name_of!(mod), "test");

        assert_eq!(name_of!(mod, 2), "macros::test");

        assert_eq!(name_of!(mod, 3), "cosmos_stdlib_extras::macros::test");

        assert_eq!(name_of!(mod, 14), "cosmos_stdlib_extras::macros::test");

        assert_eq!(name_of!(mod, 0), "cosmos_stdlib_extras::macros::test");

        assert_eq!(name_of!(crate), "cosmos_stdlib_extras");

        assert_eq!(name_of!(1u32), "u32");

        assert_eq!(name_of!(&1u32), "&u32");

        assert_eq!(name_of!(&&&1u32), "&&&u32");
    }

    #[test]
    fn parent_module_path_function() {
        use super::parent_module_path;
        assert_eq!(parent_module_path(""), "");
        assert_eq!(parent_module_path("abc"), "");
        assert_eq!(parent_module_path("a::b"), "a");
        assert_eq!(
            parent_module_path("sasquatch::unicorn::Rutabaga"),
            "sasquatch::unicorn"
        );
        assert_eq!(parent_module_path("somecrate::SomeType<T>"), "somecrate");
        assert_eq!(
            parent_module_path("mycrate::types::MyType<mycrate::Value>"),
            "mycrate::types"
        );
    }
}
