pub trait IteratorExt: private::Sealed + Iterator {
    /// Stable version of advice by
    fn advance(
        &mut self,
        n: usize,
    ) -> Result<(), usize> {
        for i in 0..n {
            self.next().ok_or(i)?;
        }
        Ok(())
    }
}

impl<T> IteratorExt for T where T: private::Sealed + Iterator {}


mod private {
    pub trait Sealed {}
    impl<T> Sealed for T where T: Iterator {}
}

#[test]
fn advance_by() {
    let a = [1, 2, 3, 4];
    let mut iter = a.iter();

    assert_eq!(iter.advance(2), Ok(()));
    assert_eq!(iter.next(), Some(&3));
    assert_eq!(iter.advance(100), Err(1)); // only `&4` was skipped
}
