/// Port of the unstable std::any::type_name_of_val() that uses type inference rather than needing
/// to know the type.
#[inline(always)]
pub fn type_name_of_val<T: ?Sized>(_val: &T) -> &'static str {
    ::std::any::type_name::<T>()
}
