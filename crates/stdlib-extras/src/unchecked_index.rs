//! # Unchecked Index - Contiguous Collections
//!
//! Take pointer arithmetic into your own hands and skip the bounds
//! checks when indexing into a slice. This should only be used when
//! absolutely necessary.

pub trait UncheckedIndex<T>: sealed::Sealed {
    type Output: Sized;
    unsafe fn unchecked_index(
        &self,
        n: usize,
    ) -> &Self::Output;
}


pub trait UncheckedIndexMut<T>: sealed::Sealed + UncheckedIndex<T> {
    unsafe fn unchecked_index_mut(
        &mut self,
        n: usize,
    ) -> &mut <Self as UncheckedIndex<T>>::Output;
}

//
//  Implementations
//

impl<'a, T> UncheckedIndex<T> for &'a [T] {
    type Output = T;

    #[inline]
    unsafe fn unchecked_index(
        &self,
        n: usize,
    ) -> &Self::Output {
        &*self.as_ptr().add(n)
    }
}


impl<'a, T> UncheckedIndex<T> for &'a mut [T] {
    type Output = T;

    #[inline]
    unsafe fn unchecked_index(
        &self,
        n: usize,
    ) -> &Self::Output {
        &*self.as_ptr().add(n)
    }
}


impl<'a, T> UncheckedIndexMut<T> for &'a mut [T] {
    #[inline]
    unsafe fn unchecked_index_mut(
        &mut self,
        n: usize,
    ) -> &mut <Self as UncheckedIndex<T>>::Output {
        &mut *self.as_mut_ptr().add(n)
    }
}


impl<T> UncheckedIndex<T> for Vec<T> {
    type Output = T;

    #[inline]
    unsafe fn unchecked_index(
        &self,
        n: usize,
    ) -> &Self::Output {
        &*self.as_ptr().add(n)
    }
}

impl<T> UncheckedIndexMut<T> for Vec<T> {
    #[inline]
    unsafe fn unchecked_index_mut(
        &mut self,
        n: usize,
    ) -> &mut <Self as UncheckedIndex<T>>::Output {
        &mut *self.as_mut_ptr().add(n)
    }
}


impl<'a, T> UncheckedIndex<T> for &'a Vec<T> {
    type Output = T;

    #[inline]
    unsafe fn unchecked_index(
        &self,
        n: usize,
    ) -> &Self::Output {
        &*self.as_ptr().add(n)
    }
}


impl<'a, T> UncheckedIndex<T> for &'a mut Vec<T> {
    type Output = T;

    #[inline]
    unsafe fn unchecked_index(
        &self,
        n: usize,
    ) -> &Self::Output {
        &*self.as_ptr().add(n)
    }
}


impl<'a, T> UncheckedIndexMut<T> for &'a mut Vec<T> {
    #[inline]
    unsafe fn unchecked_index_mut(
        &mut self,
        n: usize,
    ) -> &mut <Self as UncheckedIndex<T>>::Output {
        &mut *self.as_mut_ptr().add(n)
    }
}


impl<T, const N: usize> UncheckedIndex<T> for [T; N] {
    type Output = T;

    #[inline]
    unsafe fn unchecked_index(
        &self,
        n: usize,
    ) -> &Self::Output {
        &*self.as_ptr().add(n)
    }
}


impl<T, const N: usize> UncheckedIndexMut<T> for [T; N] {
    #[inline]
    unsafe fn unchecked_index_mut(
        &mut self,
        n: usize,
    ) -> &mut <Self as UncheckedIndex<T>>::Output {
        &mut *self.as_mut_ptr().add(n)
    }
}


mod sealed {
    pub trait Sealed {}
    impl<T> Sealed for Vec<T> {}
    impl<'a, T> Sealed for &'a Vec<T> {}
    impl<'a, T> Sealed for &'a mut Vec<T> {}
    impl<'a, T> Sealed for &'a [T] {}
    impl<'a, T> Sealed for &'a mut [T] {}
    impl<T, const N: usize> Sealed for [T; N] {}
}
