//! SplitUInt - Split an unsigned integer into upper and lower halves

pub trait SplitUInt: sealed::Sealed {
    type Half;

    /// Split unsigned integer into a tuple of (low bytes, high bytes)
    fn split(self) -> (Self::Half, Self::Half);
}

impl SplitUInt for u16 {
    type Half = u8;

    #[inline(always)]
    fn split(self) -> (Self::Half, Self::Half) {
        let bytes: [u8; 2] = self.to_le_bytes();
        (bytes[0], bytes[1])
    }
}

impl SplitUInt for u32 {
    type Half = u16;

    #[inline(always)]
    fn split(self) -> (Self::Half, Self::Half) {
        let bytes: [u8; 4] = self.to_le_bytes();

        let low = u16::from_le_bytes([bytes[0], bytes[1]]);
        let high = u16::from_le_bytes([bytes[2], bytes[3]]);
        (low, high)
    }
}


impl SplitUInt for u64 {
    type Half = u32;

    #[inline(always)]
    fn split(self) -> (Self::Half, Self::Half) {
        let bytes: [u8; 8] = self.to_le_bytes();

        let low = u32::from_le_bytes([bytes[0], bytes[1], bytes[2], bytes[3]]);
        let high = u32::from_le_bytes([bytes[4], bytes[5], bytes[6], bytes[7]]);
        (low, high)
    }
}


impl SplitUInt for u128 {
    type Half = u64;

    #[inline(always)]
    fn split(self) -> (Self::Half, Self::Half) {
        let bytes: [u8; 16] = self.to_le_bytes();

        let low = u64::from_le_bytes([
            bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7],
        ]);

        let high = u64::from_le_bytes([
            bytes[8], bytes[9], bytes[10], bytes[11], bytes[12], bytes[13], bytes[14], bytes[15],
        ]);

        (low, high)
    }
}

mod sealed {
    pub trait Sealed {}

    impl Sealed for u16 {}

    impl Sealed for u32 {}

    impl Sealed for u64 {}

    impl Sealed for u128 {}
}
