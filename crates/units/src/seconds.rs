//! # Seconds
//!
//! Standard time unit `s`.
use crate::deps::cosmos_crash_macros::crash_on_err;
use crate::Unit;


impl_dimensioned_type_common!(Seconds);


impl Unit for Seconds {
    fn name() -> &'static str {
        "second"
    }

    fn plural_name() -> &'static str {
        "seconds"
    }

    fn suffix() -> &'static str {
        "s"
    }

    fn case_sensitive_suffix() -> bool {
        true
    }

    fn default_prefix_base() -> i32 {
        10
    }

    fn prefixes() -> &'static [&'static crate::Prefix] {
        use crate::si;
        const PREFIXES: &[&crate::Prefix] = &[si::n, si::u, si::u_ascii, si::m, si::K, si::M, si::G, si::T];
        PREFIXES
    }
}

impl_dimensioned_type_tests!(Seconds, { vec![("1 us", "1 us"),] });
