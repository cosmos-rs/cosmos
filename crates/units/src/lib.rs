//! # Cosmos Units - Utilities for working with strings that represent units like '2 GiB'
//!
//! This crate is primarily for helping with (de)serializing configs and adding common logic for
//! humanizing/displaying units. As a result it should make configs a bit easier to visually inspect
//! and parse while not needing to explicitly use non-integer types.
//!
//! The primary way this crate can be used is for fields that represent byte sizes. Annotating any
//! integer fields with `#[serde(with = "cosmos_units::bytes::serde_proxy::int")]` will serialize
//! the annotated fields as iso suffixed byte unit strings:
//!
//! ```
//! #[derive(Serialize, Deserialize)]
//! struct Config {
//!     #[serde(with = "cosmos_units::bytes::serde_proxy::int")]
//!     segment_size: usize,
//! }
//! ```
//!
//! Such that the following instance of `Config`:
//!
//! ```
//! let cfg = Config {
//!     segment_size: 2 << 30,
//! };
//! ```
//!
//! will be serialized as:
//!
//! ```
//! {"segment_size": "2 GiB"}
//! ```
//!
//! But de-serialization will also assume that a naked integer is just functionally a pass-through
//! so both `"2 GiB"` and `2147483648` both will work.
#![warn(rustdoc::missing_crate_level_docs)]
#![warn(missing_debug_implementations)]
#![deny(warnings)]
#![deny(clippy::pedantic)]
mod deps {
    pub use ::anyhow;
    pub use ::derive_more;
    pub use ::num;
    pub use ::num_derive;
    pub use ::serde;
    #[cfg(test)]
    pub use ::serde_json;
    pub use ::thiserror;

    pub use ::cosmos_crash_macros;
    pub use ::cosmos_proc_macros;
}

mod dimensioned;
mod number;
mod prefix;

pub use self::dimensioned::{
    Dimensioned,
    Unit,
};
pub use self::number::Number;
pub use self::prefix::Prefix;


// this must come before the modules that use it
#[macro_use]
mod macros;


pub mod bytes;
pub mod iso;
pub mod seconds;
pub mod si;
