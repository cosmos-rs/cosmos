use crate::deps::derive_more;
use crate::deps::num::rational::Ratio;
use crate::deps::num_derive;
use crate::deps::serde;
pub type Rational = Ratio<i128>;

/// Number is a wrapper around a Ration<i128> which is a pair of i128's representing a numerator and
/// a denominator. It does not offers the largest precision while still allows the type to be copy.
#[repr(transparent)]
#[derive(
    Debug,
    Copy,
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    derive_more::Display,
    num_derive::ToPrimitive,
    num_derive::FromPrimitive,
    num_derive::NumOps,
    num_derive::One,
    num_derive::Zero,
    num_derive::Num,
    serde::Serialize,
    serde::Deserialize,
)]
pub struct Number(Rational);


impl Number {
    pub const ZERO: Number = Number::from_i64(0);

    #[must_use]
    pub const fn inner(&self) -> &Rational {
        &self.0
    }

    #[must_use]
    pub const fn with_base_and_exp(
        base: i64,
        exp: i8,
    ) -> Self {
        let value = base.pow(exp.abs() as u32) as i128;
        if exp.is_positive() {
            Self(Rational::new_raw(value, 1))
        } else {
            Self(Rational::new_raw(1, value))
        }
    }

    #[must_use]
    pub const fn from_usize(n: usize) -> Self {
        Self(Rational::new_raw(n as i128, 1))
    }

    #[must_use]
    pub const fn from_i64(n: i64) -> Self {
        Self(Rational::new_raw(n as i128, 1))
    }

    #[must_use]
    pub const fn from_integer(n: i128) -> Self {
        Self(Rational::new_raw(n, 1))
    }

    #[must_use]
    pub fn to_integer(&self) -> i128 {
        self.0.to_integer()
    }

    #[must_use]
    pub fn is_integer(&self) -> bool {
        self.0.is_integer()
    }

    #[must_use]
    pub fn reduced(&self) -> Self {
        Self(self.0.reduced())
    }

    #[must_use]
    pub const fn numerator(&self) -> i128 {
        *(self.0.numer())
    }
}


impl ::std::str::FromStr for Number {
    type Err = crate::deps::anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.contains('.') || s.contains('e') {
            let float = s.parse::<f64>()?;
            let num = Rational::approximate_float(float).ok_or_else(|| {
                crate::deps::anyhow::anyhow!("{} (f64) could not be represented as a Rational<i128>", float)
            })?;
            Ok(Self(num))
        } else {
            Ok(Self(s.parse::<Rational>()?))
        }
    }
}


impl Default for Number {
    fn default() -> Self {
        Self::ZERO
    }
}


#[cfg(test)]
mod tests {
    use super::Number;
    use crate::deps::cosmos_proc_macros::cosmos;
    use std::str::FromStr;

    #[cosmos(unit_test)]
    fn from_str() {
        let cases = vec!["1", "2.0", "3.14159", "6.02214076e23", "1000/123"];

        for case in cases {
            let num = Number::from_str(case);
            assert!(matches!(num, Ok(_)), "could not parse {:?} as Number", case);
            let num = num.unwrap().reduced();
            debug!(?case, result=%num);
        }
    }
}
