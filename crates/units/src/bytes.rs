#![allow(non_upper_case_globals)]
use crate::deps::cosmos_crash_macros::crash_on_err;
use crate::Unit;

/// Constants
pub mod consts {
    pub const KiB: usize = crate::iso::Ki.magnitude().numerator() as usize;
    pub const MiB: usize = crate::iso::Mi.magnitude().numerator() as usize;
    pub const GiB: usize = crate::iso::Gi.magnitude().numerator() as usize;
    pub const TiB: usize = crate::iso::Ti.magnitude().numerator() as usize;
    pub const EiB: usize = crate::iso::Ei.magnitude().numerator() as usize;
    pub const PiB: usize = crate::iso::Pi.magnitude().numerator() as usize;
}


impl_dimensioned_type_common!(Bytes);


impl Unit for Bytes {
    fn name() -> &'static str {
        "byte"
    }

    fn plural_name() -> &'static str {
        "bytes"
    }

    fn suffix() -> &'static str {
        "B"
    }

    fn case_sensitive_suffix() -> bool {
        true
    }

    fn default_prefix_base() -> i32 {
        2
    }

    fn prefixes() -> &'static [&'static crate::Prefix] {
        const PREFIXES: &[&crate::Prefix] = &crate::iso::PREFIXES;
        PREFIXES
    }
}


impl_dimensioned_type_tests!(Bytes, {
    vec![
        // input    | expected display str
        // standard cases
        ("1234 B", "1234 B"),
        ("1234 kB", "1234 kB"),
        ("1234 KiB", "1234 KiB"),
        ("1234 MB", "1234 MB"),
        ("1234 MiB", "1234 MiB"),
        ("1234 GB", "1234 GB"),
        ("1234 GiB", "1234 GiB"),
        ("1234 TB", "1234 TB"),
        ("1234 TiB", "1234 TiB"),
        ("1234 PB", "1234 PB"),
        ("-1234 PiB", "-1234 PiB"),
        ("-1234 B", "-1234 B"),
        ("-1234 kB", "-1234 kB"),
        ("-1234 KiB", "-1234 KiB"),
        ("-1234 MB", "-1234 MB"),
        ("-1234 MiB", "-1234 MiB"),
        ("-1234 GB", "-1234 GB"),
        ("-1234 GiB", "-1234 GiB"),
        ("-1234 TB", "-1234 TB"),
        ("-1234 TiB", "-1234 TiB"),
        ("-1234 PB", "-1234 PB"),
        ("-1234 PiB", "-1234 PiB"),
        // nonstandard variants with long names
        // and random whitespace and optional pluralization
        ("1B", "1 B"),
        (" 1 B ", "1 B"),
        ("2bytes", "2 B"),
        ("3byte", "3 B"),
        ("4 byte", "4 B"),
        ("5 kilobytes", "5 kB"),
        ("6 kilobyte", "6 kB"),
        ("80 KB", "80 kB"),
        ("1234 GB", "1234 GB"),
        ("1234 GiB", "1234 GiB"),
    ]
});
