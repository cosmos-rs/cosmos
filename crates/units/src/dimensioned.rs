use crate::{
    iso,
    si,
    Number,
    Prefix,
};
use ::core::fmt;

/// This trait should be implemented for types that are Units that can be measured. Like bytes,
/// seconds, meters, etc.
pub trait Unit {
    fn name() -> &'static str;
    fn plural_name() -> &'static str;
    fn suffix() -> &'static str;
    fn case_sensitive_suffix() -> bool;
    fn default_prefix_base() -> i32;
    fn prefixes() -> &'static [&'static crate::Prefix];
}

/// This is the core type that wraps a value and a prefix and is represents a Unit type U.
/// The normal way to use this is to make a newtype wrapper like:
///
/// ```rust
/// #[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd)]
/// pub struct Bytes(Dimensioned<Self>);
/// ```
///
/// This can be done automatically with the `impl_dimensioned_type_common!(<typename>);` macro.
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd)]
pub struct Dimensioned<U, T = Number> {
    pub(crate) prefix: &'static Prefix,
    pub(crate) unit:   std::marker::PhantomData<U>,
    pub(crate) value:  T,
}


impl<U, T> ::std::str::FromStr for Dimensioned<U, T>
where
    U: Unit,
    T: ::std::str::FromStr,
{
    type Err = ParseDimensionedUnitError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let trimmed = s.trim();

        let NumericPrefix { idx, value } = parse_numeric_prefix(trimmed).ok_or_else(Self::Err::new)?;

        let (_value_part, unit_part) = trimmed.split_at(idx);

        let (prefix_part, _) = unit_part
            .rsplit_once(U::suffix())
            .or_else(|| unit_part.rsplit_once(U::name()))
            .or_else(|| unit_part.rsplit_once(U::plural_name()))
            .ok_or_else(Self::Err::new)?;

        let prefix = find_prefix(prefix_part).ok_or_else(Self::Err::new)?;

        Ok(Dimensioned {
            prefix,
            unit: std::marker::PhantomData,
            value,
        })
    }
}


impl<U: Unit> Dimensioned<U, Number> {
    #[must_use]
    pub fn value(&self) -> Number {
        self.value * (self.prefix.magnitude())
    }
}

impl<U, T> fmt::Display for Dimensioned<U, T>
where
    U: Unit,
    T: fmt::Display,
{
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        self.value.fmt(f)?;
        f.write_str(" ")?;
        f.write_str(self.prefix.abbreviation())?;
        f.write_str(U::suffix())
    }
}



/// An error returned when parsing a [`Self`] using [`from_str`] fails
///
/// [`from_str`]: std::str::FromStr::from_str
#[derive(Copy, Clone, PartialEq, Eq)]
pub struct ParseDimensionedUnitError {
    _priv: (),
}


impl ParseDimensionedUnitError {
    const fn new() -> Self {
        ParseDimensionedUnitError { _priv: () }
    }
}

impl ::std::fmt::Debug for ParseDimensionedUnitError {
    fn fmt(
        &self,
        f: &mut ::std::fmt::Formatter<'_>,
    ) -> ::std::fmt::Result {
        ::std::fmt::Display::fmt(self, f)
    }
}

impl ::std::fmt::Display for ParseDimensionedUnitError {
    fn fmt(
        &self,
        f: &mut ::std::fmt::Formatter<'_>,
    ) -> ::std::fmt::Result {
        ("provided string did not represent a byte size in the form `<count> <prefix>?B`").fmt(f)
    }
}


#[derive(Debug)]
pub(crate) struct NumericPrefix<T> {
    pub(crate) idx:   usize,
    pub(crate) value: T,
}


pub(crate) fn parse_numeric_prefix<T>(s: &str) -> Option<NumericPrefix<T>>
where
    T: std::str::FromStr,
{
    s.find(|ch: char| !(ch.is_ascii_digit() || ch == '-'))
        .map(|idx| (idx, s.split_at(idx).0))
        .and_then(|(idx, first)| {
            first
                .parse::<T>()
                .ok()
                .map(|num| NumericPrefix { idx, value: num })
        })
}


pub(crate) fn is_match(
    prefix: &'static Prefix,
    string: &str,
) -> bool {
    let s = string.trim();

    s.eq(prefix.abbreviation())
        || (!prefix.abbreviation_is_cased() && s.eq_ignore_ascii_case(prefix.abbreviation()))
        || s.eq_ignore_ascii_case(prefix.name())
}

pub(crate) fn into_iter<T, const N: usize>(arr: [T; N]) -> std::array::IntoIter<T, N> {
    std::iter::IntoIterator::into_iter(arr)
}

pub(crate) fn all_prefixes() -> impl Iterator<Item = &'static Prefix> {
    into_iter(iso::PREFIXES).chain(into_iter(si::PREFIXES))
}


pub(crate) fn find_prefix(s: &str) -> Option<&'static Prefix> {
    let s = s.trim();
    if s.is_empty() {
        return Some(Prefix::ONE);
    }

    all_prefixes().find(|prefix| is_match(*prefix, s))
}
