//! # Standard SI Prefixes
//!
//! From femto to Exa
use crate::Number;
use crate::Prefix;

pub const PREFIXES: [&Prefix; 16] = [f, p, n, u, u_ascii, m, c, d, da, h, K, M, G, T, P, E];
const SI: &str = "si";

#[allow(non_upper_case_globals)]
pub const f: &Prefix = &Prefix {
    org:                   SI,
    name:                  "femto",
    abbreviation:          "f",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              -15,
    magnitude:             Number::with_base_and_exp(10, -15),
};


#[allow(non_upper_case_globals)]
pub const p: &Prefix = &Prefix {
    org:                   SI,
    name:                  "pico",
    abbreviation:          "p",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              -12,
    magnitude:             Number::with_base_and_exp(10, -12),
};

#[allow(non_upper_case_globals)]
pub const n: &Prefix = &Prefix {
    org:                   SI,
    name:                  "nano",
    abbreviation:          "n",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              -9,
    magnitude:             Number::with_base_and_exp(10, -9),
};

#[allow(non_upper_case_globals)]
pub const u: &Prefix = &Prefix {
    org:                   SI,
    name:                  "micro",
    abbreviation:          "\u{3bc}", // 'μ'
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              -6,
    magnitude:             Number::with_base_and_exp(10, -6),
};

#[allow(non_upper_case_globals)]
pub const u_ascii: &Prefix = &Prefix {
    org:                   SI,
    name:                  "micro",
    abbreviation:          "u",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              -6,
    magnitude:             Number::with_base_and_exp(10, -6),
};

#[allow(non_upper_case_globals)]
pub const m: &Prefix = &Prefix {
    org:                   SI,
    name:                  "milli",
    abbreviation:          "m",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              -3,
    magnitude:             Number::with_base_and_exp(10, -3),
};

#[allow(non_upper_case_globals)]
pub const c: &Prefix = &Prefix {
    org:                   SI,
    name:                  "centi",
    abbreviation:          "c",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              -2,
    magnitude:             Number::with_base_and_exp(10, -2),
};

#[allow(non_upper_case_globals)]
pub const d: &Prefix = &Prefix {
    org:                   SI,
    name:                  "deci",
    abbreviation:          "d",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              -1,
    magnitude:             Number::with_base_and_exp(10, -1),
};

#[allow(non_upper_case_globals)]
pub const da: &Prefix = &Prefix {
    org:                   SI,
    name:                  "deca",
    abbreviation:          "da",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              1,
    magnitude:             Number::with_base_and_exp(10, 1),
};

#[allow(non_upper_case_globals)]
pub const h: &Prefix = &Prefix {
    org:                   SI,
    name:                  "hecto",
    abbreviation:          "h",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              2,
    magnitude:             Number::with_base_and_exp(10, 2),
};

pub const K: &Prefix = &Prefix {
    org:                   SI,
    name:                  "kilo",
    abbreviation:          "k",
    abbreviation_is_cased: false,
    base:                  10,
    exponent:              3,
    magnitude:             Number::with_base_and_exp(10, 3),
};

pub const M: &Prefix = &Prefix {
    org:                   SI,
    name:                  "mega",
    abbreviation:          "M",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              6,
    magnitude:             Number::with_base_and_exp(10, 6),
};

pub const G: &Prefix = &Prefix {
    org:                   SI,
    name:                  "giga",
    abbreviation:          "G",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              9,
    magnitude:             Number::with_base_and_exp(10, 9),
};

pub const T: &Prefix = &Prefix {
    org:                   SI,
    name:                  "tera",
    abbreviation:          "T",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              12,
    magnitude:             Number::with_base_and_exp(10, 12),
};

pub const P: &Prefix = &Prefix {
    org:                   SI,
    name:                  "peta",
    abbreviation:          "P",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              15,
    magnitude:             Number::with_base_and_exp(10, 15),
};

pub const E: &Prefix = &Prefix {
    org:                   SI,
    name:                  "exa",
    abbreviation:          "E",
    abbreviation_is_cased: true,
    base:                  10,
    exponent:              18,
    magnitude:             Number::with_base_and_exp(10, 18),
};
