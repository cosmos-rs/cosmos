use crate::Number;

/// Represents a prefix like "k" or "Gi".
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Prefix {
    pub(crate) org:                   &'static str,
    pub(crate) name:                  &'static str,
    pub(crate) abbreviation:          &'static str,
    pub(crate) abbreviation_is_cased: bool,
    pub(crate) base:                  u8,
    pub(crate) exponent:              i8,
    pub(crate) magnitude:             Number,
}


impl Prefix {
    pub const ONE: &'static Prefix = &Prefix {
        org:                   "none",
        name:                  "one",
        abbreviation:          "",
        abbreviation_is_cased: false,
        base:                  1,
        exponent:              1,
        magnitude:             Number::from_i64(1),
    };

    #[must_use]
    pub const fn org(&self) -> &'static str {
        self.org
    }

    #[must_use]
    pub const fn abbreviation_is_cased(&self) -> bool {
        self.abbreviation_is_cased
    }

    #[must_use]
    pub const fn name(&self) -> &'static str {
        self.name
    }

    #[must_use]
    pub const fn abbreviation(&self) -> &'static str {
        self.abbreviation
    }

    #[must_use]
    pub const fn base(&self) -> u8 {
        self.base
    }

    #[must_use]
    pub const fn exponent(&self) -> i8 {
        self.exponent
    }

    #[must_use]
    pub const fn magnitude(&self) -> Number {
        self.magnitude
    }
}
