macro_rules! impl_dimensioned_type_from_str {
    ($name:ident) => {
        impl ::std::str::FromStr for $name {
            type Err = $crate::dimensioned::ParseDimensionedUnitError;

            fn from_str(s: &str) -> Result<Self, Self::Err> {
                $crate::Dimensioned::<Self>::from_str(s).map($name)
            }
        }
    };
}


macro_rules! impl_dimensioned_type_serialize {
    ($name:ident) => {
        impl $crate::deps::serde::Serialize for $name {
            fn serialize<S>(
                &self,
                serializer: S,
            ) -> Result<
                <S as $crate::deps::serde::Serializer>::Ok,
                <S as $crate::deps::serde::Serializer>::Error,
            >
            where
                S: $crate::deps::serde::Serializer,
            {
                self.to_string().serialize(serializer)
            }
        }
    };
}


macro_rules! impl_dimensioned_type_deserialize {
    ($name:ident) => {
        impl<'de> $crate::deps::serde::Deserialize<'de> for $name {
                fn deserialize<D>(deserializer: D) -> Result<Self, <D as $crate::deps::serde::Deserializer<'de>>::Error>
                    where
                        D: $crate::deps::serde::Deserializer<'de>,
                {
                    use $crate::deps::serde::de::{
                        self,
                        Visitor,
                    };
                    use ::std::marker::PhantomData;
                    use ::std::str::FromStr;
                    // This is a Visitor that forwards string types to $name's `FromStr` impl and
                    // forwards map types to $name's `Deserialize` impl. The `PhantomData` is to
                    // keep the compiler from complaining about$name being an unused generic type
                    // parameter. We need$name in order to know the Value type for the Visitor
                    // impl.
                    struct StringOrStruct(PhantomData<fn() -> $name>);

                    impl<'de> Visitor<'de> for StringOrStruct {
                        type Value = $name;

                        fn expecting(&self, formatter: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
                            formatter.write_str(&format!(
                                "type {} could not be deserialized with either std::str::FromStr serde:Deserialize",
                                ::std::any::type_name::<$name>()
                            ))
                        }

                        fn visit_u64<E>(self, value: u64) -> Result<$name, E>
                            where
                                E: de::Error,
                        {
                            Ok($name($crate::Dimensioned {
                                prefix: $crate::Prefix::ONE,
                                unit: ::std::marker::PhantomData,
                                value: $crate::Number::from_integer(value as i128),
                            })
                                .reduced())
                        }

                        fn visit_i64<E>(self, value: i64) -> Result<$name, E>
                            where
                                E: de::Error,
                        {
                            Ok($name($crate::Dimensioned {
                                prefix: $crate::Prefix::ONE,
                                unit: ::std::marker::PhantomData,
                                value: $crate::Number::from_integer(value as i128),
                            })
                                .reduced())
                        }

                        fn visit_unit<E>(self) -> Result<$name, E>
                            where
                                E: de::Error,
                        {
                            Ok($name($crate::Dimensioned {
                                prefix: $crate::Prefix::ONE,
                                unit: ::std::marker::PhantomData,
                                value: $crate::Number::from_integer(0 as i128),
                            })
                                .reduced())
                        }

                        fn visit_str<E>(self, value: &str) -> Result<$name, E>
                            where
                                E: de::Error,
                        {
                            Ok(FromStr::from_str(value).unwrap())
                        }
                    }

                    deserializer.deserialize_any(StringOrStruct(PhantomData))
                }
            }
    };
}
macro_rules! impl_dimensioned_type_try_into_int {
    ($name:ident, $err:ident, $($dest:ty),+ $(,)?) => {
        $(
          impl ::std::convert::TryInto<$dest> for $name {
              type Error = $err;
              fn try_into(self) -> Result<$dest, Self::Error> {
                  Ok(self.to_number().to_integer().try_into()?)
              }
          }
        )+
    };
    ($name:ident, $err:ident $(,)?) => {
        impl_dimensioned_type_try_into_int!($name, $err, i8, i16, i32, i64, i128, isize, u8, u16, u32, u64, u128, usize);
    };
    ($name:ident $(,)?) => {
        #[derive(Clone, Debug)]
        pub struct TryIntoIntError {
            _priv: ()
        }

        impl ::std::convert::From<::std::num::TryFromIntError> for TryIntoIntError {
            fn from(_x: ::std::num::TryFromIntError) -> Self {
                Self { _priv: () }
            }
        }

        impl ::std::convert::From<::std::convert::Infallible> for TryIntoIntError {
            fn from(_: ::std::convert::Infallible) -> Self {
                Self { _priv: () }
            }
        }

        impl ::std::fmt::Display for TryIntoIntError {
            fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
                f.write_str(concat!("could not convert ", stringify!($name), " into target integer type"))
            }
        }

        impl_dimensioned_type_try_into_int!($name, TryIntoIntError);
    };
}



macro_rules! impl_dimensioned_type_try_from_int {
    ($name:ident, $err:ident, $($dest:ty),+ $(,)?) => {
        $(
          impl ::std::convert::TryFrom<$dest> for $name {
              type Error = $err;
              fn try_from(value: $dest) -> Result<$name, Self::Error> {
                  use ::std::convert::TryInto;
                  Ok($name::from_integer(value.try_into()?))
              }
          }

         impl<'a> ::std::convert::TryFrom<&'a $dest> for $name {
              type Error = $err;
              fn try_from(value: &'a $dest) -> Result<$name, Self::Error> {
                  use ::std::convert::TryInto;
                  Ok($name::from_integer((*value).try_into()?))
              }
          }

        )+
    };
    ($name:ident, $err:ident $(,)?) => {
        impl_dimensioned_type_try_from_int!($name, $err, i8, i16, i32, i64, i128, isize, u8, u16, u32, u64, u128, usize);
    };
    ($name:ident $(,)?) => {
        #[derive(Clone, Debug)]
        pub struct TryFromIntError {
            _priv: ()
        }

        impl ::std::convert::From<::std::num::TryFromIntError> for TryFromIntError {
            fn from(_x: ::std::num::TryFromIntError) -> Self {
                Self { _priv: () }
            }
        }

        impl ::std::convert::From<::std::convert::Infallible> for TryFromIntError {
            fn from(_: ::std::convert::Infallible) -> Self {
                Self { _priv: () }
            }
        }


        impl ::std::fmt::Display for TryFromIntError {
            fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
                f.write_str(concat!("could not convert integer type into ", stringify!($name), " type"))
            }
        }

        impl_dimensioned_type_try_from_int!($name, TryFromIntError);
    };
}


macro_rules! impl_dimensioned_type_tests {
    ($name:ident, $good_cases:expr) => {
        #[cfg(test)]
        mod tests {
            use ::std::str::FromStr;
            use $crate::deps::cosmos_crash_macros::crash_on_err;
            use $crate::deps::cosmos_proc_macros::cosmos;

            use super::$name;

            fn good_cases() -> impl Iterator<Item=(&'static str, &'static str)> {
                ($good_cases).into_iter()
            }

            #[cosmos(unit_test)]
            fn parse_from_string_golden_paths() {

                for (case, expected) in good_cases() {
                    let result = $name::from_str(case);
                    match result {
                        Ok(ok) => {
                            let value = format!("{}", ok);
                            if value == *expected {
                                debug!(?case, value=?value, "success");
                            } else {
                                error!(?case, ?value, ?expected, "{} != {}", value, expected);
                                assert_eq!(
                                    value, *expected,
                                    "formatted display value of {:?} did not mach the expected value",
                                    ok
                                );
                            }
                        }
                        Err(err) => {
                            error!(?case, ?expected, err=%err, "failed");
                            std::panic::panic_any(err);
                        }
                    }
                }
            }

            #[cosmos(unit_test)]
            fn serialization_round_trip() {
                use $crate::deps::serde_json;
                for (case, _) in good_cases() {
                    let dim = $name::from_str(case).unwrap_or_else(crash_on_err!());
                    let s = serde_json::to_string(&dim).unwrap_or_else(crash_on_err!());
                    let value = serde_json::from_str::<$name>(&s).unwrap_or_else(crash_on_err!());
                    debug!(?case, ser=%s, de=?value.to_string());
                }
            }

            #[cosmos(unit_test)]
            fn deserialize_from_naked_int() {
                use $crate::deps::serde_json;

                let cases = vec![1234234, 100, 2, 3000, 44000, 555000, 5_000_000, 1024, 1 << 20, 2 * 1024 * 1024, 37 * (1 << 30)]
                    .into_iter()
                    .map(|i| (i.to_string(), $name::from_integer(i).reduced()))
                    .collect::<Vec<_>>();

                for (case, expected) in cases.into_iter() {
                    let value = serde_json::from_str::<$name>(&case).unwrap_or_else(crash_on_err!());

                    debug!(?case, value=?value.to_string(), expected=?expected.to_string());
                    assert_eq!(value, expected);
                }
            }

        }
    };
}

macro_rules! impl_dimensioned_type_inherent_impl {
    ($name:ident) => {
        #[repr(transparent)]
        #[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, derive_more::Display)]
        pub struct $name(pub(crate) $crate::Dimensioned<Self>);

        impl $name {
            const ZERO: $name = $name::from_i64(0);

            #[must_use]
            pub const fn from_i64(n: i64) -> Self {
                Self($crate::Dimensioned {
                    prefix: $crate::Prefix::ONE,
                    unit:   ::std::marker::PhantomData,
                    value:  $crate::Number::from_i64(n),
                })
            }

            #[must_use]
            pub const fn from_integer(n: i128) -> Self {
                Self($crate::Dimensioned {
                    prefix: $crate::Prefix::ONE,
                    unit:   ::std::marker::PhantomData,
                    value:  $crate::Number::from_integer(n),
                })
            }

            #[must_use]
            pub fn new(n: $crate::Number) -> Self {
                Self($crate::Dimensioned {
                    prefix: $crate::Prefix::ONE,
                    unit:   ::std::marker::PhantomData,
                    value:  n,
                })
            }

            #[must_use]
            pub fn reduced(&self) -> Self {
                use $crate::deps::cosmos_crash_macros::crash;
                let n = self.0.value();
                if n == $crate::Number::ZERO {
                    return Self::ZERO;
                }

                match Self::default_prefix_base() {
                    2 => {
                        use ::std::convert::TryFrom;
                        debug_assert!(self.0.value.inner().is_integer());
                        let n = n.to_integer();
                        let zeros = i8::try_from(n.trailing_zeros()).unwrap_or_else(crash_on_err!());

                        $name::prefixes()
                            .iter()
                            .take_while(|p| p.exponent <= zeros)
                            .last()
                            .map(|prefix| {
                                debug_assert_eq!(prefix.base, 2);

                                Self($crate::Dimensioned {
                                    prefix,
                                    unit: ::std::marker::PhantomData,
                                    value: $crate::Number::from_integer(n >> prefix.exponent),
                                })
                            })
                            .unwrap_or_else(|| *self)
                    }
                    10 => {
                        debug_assert!(self.0.value.inner().is_integer());

                        $name::prefixes()
                            .iter()
                            .filter(|p| p.exponent() >= 0)
                            .take_while(|p| (n / p.magnitude()).is_integer())
                            .last()
                            .map(|prefix| {
                                debug_assert_eq!(prefix.base, 10);

                                Self($crate::Dimensioned {
                                    prefix,
                                    unit: ::std::marker::PhantomData,
                                    value: n / prefix.magnitude(),
                                })
                            })
                            .unwrap_or_else(|| *self)
                    }
                    base => crash!("unknown base {}, expected 2 or 10", base),
                }
            }

            #[must_use]
            pub fn to_number(&self) -> $crate::Number {
                self.0.value()
            }

            #[must_use]
            pub fn prefix(&self) -> &'static $crate::Prefix {
                self.0.prefix
            }
        }
    };
}


macro_rules! impl_dimensioned_typed_serde_proxy {
    ($name:ident) => {
        // Serde's 'with' attribute allows you to define a serializer and a deserializer in a module.
        // You can implement serialize and deserialize as functions in your code, without using modules,
        // and use the `deserialize_with` and `serialize_with` attributes instead.
        pub mod serde_proxy {
            // This is just syntactic sugar, so the attribute looks cool like "from::int".
            // You don't need to embed multiple modules.
            pub mod int {
                use super::super::$name;
                use ::std::convert::TryFrom;
                use ::std::convert::TryInto;

                use crate::deps::serde::{
                    de::Error as _,
                    ser::Error as _,
                    Deserialize,
                    Deserializer,
                    Serialize,
                    Serializer,
                };

                #[doc = r#"
                # Errors
                * the deserialization into the proxy Unit type fails
                * the conversion of the deserialized Unit type value into the target integer type fails
                "#]
                pub fn deserialize<'de, D, T>(deserializer: D) -> Result<T, D::Error>
                where
                    D: Deserializer<'de>,
                    $name: TryInto<T>,
                    <$name as ::std::convert::TryInto<T>>::Error: std::fmt::Display,
                {
                    $name::deserialize(deserializer)?
                        .try_into()
                        .map_err(|e| D::Error::custom(format!("{}", e)))
                }

                #[doc = r#"
                # Errors
                * The conversion from the integer type into the proxy Unit type fails. This
                  should not happen in the normal case.
                * The serailzation of the Unit type to the string format fails.
                "#]
                pub fn serialize<'a, S, T>(value: &'a T, serializer: S) -> Result<S::Ok, S::Error>
                where
                    S: Serializer,
                    T: Copy,
                    $name: ::std::convert::TryFrom<&'a T>,
                    <$name as ::std::convert::TryFrom<&'a T>>::Error: std::fmt::Display,
                {
                    $name::try_from(value)
                        .map_err(|e| S::Error::custom(format!("{}", e)))?
                        .reduced()
                        .serialize(serializer)
                }
            }

            /// Like `int` but do not use reduce or use prefixes
            pub mod int_no_prefix {
                use super::super::$name;
                use ::std::convert::TryFrom;
                use ::std::convert::TryInto;

                use crate::deps::serde::{
                    ser::Error as _,
                    Deserializer,
                    Serialize,
                    Serializer,
                };

                #[doc = r#"
                # Errors
                * the deserialization into the proxy Unit type fails
                * the conversion of the deserialized Unit type value into the target integer type fails
                "#]
                pub fn deserialize<'de, D, T>(deserializer: D) -> Result<T, D::Error>
                where
                    D: Deserializer<'de>,
                    $name: TryInto<T>,
                    <$name as ::std::convert::TryInto<T>>::Error: std::fmt::Display,
                {
                    super::int::deserialize(deserializer)
                }

                #[doc = r#"
                # Errors
                * The conversion from the integer type into the proxy Unit type fails. This
                  should not happen in the normal case.
                * The serailzation of the Unit type to the string format fails.
                "#]
                pub fn serialize<'a, S, T>(value: &'a T, serializer: S) -> Result<S::Ok, S::Error>
                where
                    S: Serializer,
                    T: Copy,
                    $name: ::std::convert::TryFrom<&'a T>,
                    <$name as ::std::convert::TryFrom<&'a T>>::Error: std::fmt::Display,
                {
                    $name::try_from(value)
                        .map_err(|e| S::Error::custom(e.to_string()))?
                        .serialize(serializer)
                }
            }

            // This is just syntactic sugar, so the attribute looks cool like "from::int".
            // You don't need to embed multiple modules.
            pub mod option_int {
                use super::super::$name;
                use ::std::convert::TryFrom;
                use ::std::convert::TryInto;

                use crate::deps::serde::{
                    de::Error as _,
                    ser::Error as _,
                    Deserialize,
                    Deserializer,
                    Serialize,
                    Serializer,
                };

                #[doc = r#"
                # Errors
                * the deserialization into the proxy Unit type fails
                * the conversion of the deserialized Unit type value into the target integer type fails
                "#]
                pub fn deserialize<'de, D, T>(deserializer: D) -> Result<Option<T>, D::Error>
                where
                    D: Deserializer<'de>,
                    $name: TryInto<T>,
                    <$name as ::std::convert::TryInto<T>>::Error: std::fmt::Display,
                {
                   if let Some(value) = Option::<$name>::deserialize(deserializer)? {
                      Some(value.try_into()
                            .map_err(|e| D::Error::custom(format!("{}", e))))
                            .transpose()
                   } else {
                       Ok(None)
                   }
                }

                #[doc = r#"
                # Errors
                * The conversion from the integer type into the proxy Unit type fails. This
                  should not happen in the normal case.
                * The serailzation of the Unit type to the string format fails.
                "#]
                pub fn serialize<'a, S, T>(value: &'a Option<T>, serializer: S) -> Result<S::Ok, S::Error>
                where
                    S: Serializer,
                    T: Copy,
                    $name: ::std::convert::TryFrom<&'a T>,
                    <$name as ::std::convert::TryFrom<&'a T>>::Error: std::fmt::Display,
                {
                    if let Some(value) = value.as_ref() {
                        $name::try_from(value)
                            .map_err(|e| S::Error::custom(format!("{}", e)))?
                            .reduced()
                            .serialize(serializer)
                    } else {
                        let v :Option<()> = None;
                        v.serialize(serializer)
                    }
                }
            }
        }

        #[cfg(test)]
        mod serde_proxy_tests {
            use $crate::Unit;
            use super::$name;
            use $crate::deps::{
                cosmos_crash_macros::{crash, crash_on_err},
                cosmos_proc_macros::cosmos,
                serde,
            };

            #[derive(Debug, serde::Serialize, serde::Deserialize, PartialEq, Eq, Default)]
            struct Example {
                #[serde(with = "super::serde_proxy::int")]
                i8: i8,
                #[serde(with = "super::serde_proxy::int")]
                i16: i16,
                #[serde(with = "super::serde_proxy::int")]
                i32: i32,
                #[serde(with = "super::serde_proxy::int")]
                i64: i64,
                #[serde(with = "super::serde_proxy::int")]
                i128: i128,
                #[serde(with = "super::serde_proxy::int")]
                isize: isize,
                #[serde(with = "super::serde_proxy::int")]
                u8: u8,
                #[serde(with = "super::serde_proxy::int")]
                u16: u16,
                #[serde(with = "super::serde_proxy::int")]
                u32: u32,
                #[serde(with = "super::serde_proxy::int")]
                u64: u64,
                #[serde(with = "super::serde_proxy::int")]
                u128: u128,
                #[serde(with = "super::serde_proxy::int")]
                usize: usize,
            }

            #[cosmos(unit_test)]
            fn serde_round_trip_with_proxy() {
                use crate::deps::serde_json;
                let (example, expected) = match $name::default_prefix_base() {
                    2 => {
                        let expected = format!(r#"{{"i8":"-1 {suffix}","i16":"-2 {suffix}","i32":"-3 {suffix}","i64":"-4 {suffix}","i128":"-5 {suffix}","isize":"-6 {suffix}","u8":"0 {suffix}","u16":"1 Ki{suffix}","u32":"1 Mi{suffix}","u64":"1 Gi{suffix}","u128":"1 Ti{suffix}","usize":"6 {suffix}"}}"#, suffix=$name::suffix());

                        let example = Example {
                            i8: -1,
                            i16: -2,
                            i32: -3,
                            i64: -4,
                            i128: -5,
                            isize: -6,
                            u8: 0,
                            u16: 1024,
                            u32: 1 << 20,
                            u64: 1 << 30,
                            u128: 1 << 40,
                            usize: 6,
                        };
                        (example, expected)
                    },
                    10 => {
                        let expected = format!(r#"{{"i8":"-1 {suffix}","i16":"-2 {suffix}","i32":"-3 {suffix}","i64":"-4 {suffix}","i128":"-5 {suffix}","isize":"-6 {suffix}","u8":"0 {suffix}","u16":"1 k{suffix}","u32":"1 M{suffix}","u64":"1 G{suffix}","u128":"1 T{suffix}","usize":"6 {suffix}"}}"#, suffix=$name::suffix());

                        let example = Example {
                            i8: -1,
                            i16: -2,
                            i32: -3,
                            i64: -4,
                            i128: -5,
                            isize: -6,
                            u8: 0,
                            u16: 1000,
                            u32: 1_000_000,
                            u64: 1_000_000_000,
                            u128: 1_000_000_000_000,
                            usize: 6,
                        };
                        (example, expected)
                    },
                    base => crash!("unknown base {}, expected 2 or 10", base),
                };

                let ser_result = serde_json::to_string(&example);
                assert!(
                    matches!(ser_result, Ok(_)),
                    "could not serialize {:?} into a json string using the serde_proxy::int for integer fields",
                    example
                );
                let value = ser_result.unwrap_or_else(crash_on_err!());
                debug!(?example, %value);
                assert_eq!(value, expected);

                let de_result = serde_json::from_str::<Example>(&value);
                assert!(
                    matches!(de_result, Ok(_)),
                    "could not deserialize Example from the json string {:?} using the serde_proxy::int for integer fields",
                    value
                );

                let reinflated = de_result.unwrap_or_else(crash_on_err!());
                assert_eq!(example, reinflated);
            }

            #[cosmos(unit_test)]
            fn serde_unit_is_0() {
                use crate::deps::serde_json;
                let unit = "null";
                let unit_result = serde_json::from_str::<$name>(unit);
                debug!(value=%unit, result=?unit_result);

                assert!(
                        matches!(unit_result, Ok(_)),
                        "could not serialize {:?} from a json string",
                        unit
                    );
                let value = unit_result.unwrap();
                assert_eq!(value.to_number().to_integer(), 0);
            }
        }


    };
}


macro_rules! impl_dimensioned_type_common {
    ($name:ident) => {
        impl_dimensioned_type_inherent_impl!($name);
        impl_dimensioned_type_from_str!($name);
        impl_dimensioned_type_deserialize!($name);
        impl_dimensioned_type_serialize!($name);
        impl_dimensioned_type_try_into_int!($name,);
        impl_dimensioned_type_try_from_int!($name,);
        impl_dimensioned_typed_serde_proxy!($name);
    };
}
