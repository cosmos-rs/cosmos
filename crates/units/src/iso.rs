#![allow(non_upper_case_globals)]
use crate::{
    Number,
    Prefix,
};

pub const PREFIXES: [&Prefix; 6] = [Ki, Mi, Gi, Ti, Pi, Ei];
const ISO: &str = "iso";

pub const Ki: &Prefix = &Prefix {
    org:                   ISO,
    name:                  "kibi",
    abbreviation:          "Ki",
    abbreviation_is_cased: true,
    base:                  2,
    exponent:              10,
    magnitude:             Number::with_base_and_exp(2, 10),
};

pub const Mi: &Prefix = &Prefix {
    org:                   ISO,
    name:                  "mebi",
    abbreviation:          "Mi",
    abbreviation_is_cased: true,
    base:                  2,
    exponent:              20,
    magnitude:             Number::with_base_and_exp(2, 20),
};

pub const Gi: &Prefix = &Prefix {
    org:                   ISO,
    name:                  "gibi",
    abbreviation:          "Gi",
    abbreviation_is_cased: true,
    base:                  2,
    exponent:              30,
    magnitude:             Number::with_base_and_exp(2, 30),
};

pub const Ti: &Prefix = &Prefix {
    org:                   ISO,
    name:                  "tebi",
    abbreviation:          "Ti",
    abbreviation_is_cased: true,
    base:                  2,
    exponent:              40,
    magnitude:             Number::with_base_and_exp(2, 40),
};

pub const Pi: &Prefix = &Prefix {
    org:                   ISO,
    name:                  "pebi",
    abbreviation:          "Pi",
    abbreviation_is_cased: true,
    base:                  2,
    exponent:              50,
    magnitude:             Number::with_base_and_exp(2, 50),
};

pub const Ei: &Prefix = &Prefix {
    org:                   ISO,
    name:                  "exi",
    abbreviation:          "Ei",
    abbreviation_is_cased: true,
    base:                  2,
    exponent:              60,
    magnitude:             Number::with_base_and_exp(2, 60),
};
