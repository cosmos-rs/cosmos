//! # Enabled Intrinsics
//!
//! ## Branch Prediction Hints
//!
//! Likely ane unlikely branch prediction hints. These should be used with caution. Good rule of
//! thumbs are:
//!
//! 1. Only to use these in code that cares about every iota of single performance in the golden
//!    path at a cost of perf in the alternate path.
//! 2. Code that is called extremely often with a regular access pattern where the expected path is
//!    taken more than 99/100 (>99%) of the time.
//!
//! The backup implementation that works on stable is an implementation that tries
//! to do the same thing as the intrinsics by forcing the compiler to weight the unexpected path
//! higher.
#![cfg_attr(feature = "nightly", feature(core_intrinsics))]
#![deny(missing_docs)]
#![deny(rustdoc::missing_crate_level_docs)]
#![deny(missing_debug_implementations)]

use crate::deps::cfg_if::cfg_if;

#[doc(hidden)]
pub(crate) mod deps {
    pub use ::cfg_if;
}

/// Compiler hint to optimize the branch predictor to indicate that the marked branch
/// is likely to be taken.
#[inline(always)]
pub fn likely(b: bool) -> bool {
    cfg_if! {
        if #[cfg(feautre = "nightly")] {
            ::core::intrinsics::likely(b)
        } else {
            if !b {
                cold()
            }
            b
        }
    }
}


/// Compiler hint to optimize the branch predictor to indicate that the marked branch
/// is unlikely to be taken.
#[inline(always)]
pub fn unlikely(b: bool) -> bool {
    cfg_if! {
        if #[cfg(feautre = "nightly")] {
            ::core::intrinsics::unlikely(b)
        } else {
            if b {
                cold()
            }
            b
        }
    }
}


#[doc(hidden)]
#[inline(never)]
#[cold]
fn cold() {}
