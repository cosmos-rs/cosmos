use crate::deps::{
    derive_more,
    serde,
};
use crate::{
    Error,
    KeySet,
    Slice,
    Slices,
    Span,
};

type Context = ::std::collections::HashMap<String, String>;


/// Render `source` with the key replacements in `substitutions`.
pub fn render_str<S, I, K, V>(
    source: S,
    substitutions: I,
) -> Result<String, Error>
where
    S: AsRef<str>,
    I: IntoIterator<Item = (K, V)>,
    K: AsRef<str>,
    V: AsRef<str>,
{
    crate::parse_str(&source).and_then(move |template| template.render(substitutions))
}


#[repr(transparent)]
#[derive(
    Clone,
    Debug,
    Default,
    PartialEq,
    Eq,
    Ord,
    PartialOrd,
    Hash,
    derive_more::Display,
    serde::Serialize,
    serde::Deserialize,
)]
pub struct TemplateString(String);

impl TemplateString {
    pub const fn new(s: String) -> Self {
        Self(s)
    }

    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}


impl AsRef<str> for TemplateString {
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl<T> From<T> for TemplateString
where
    String: From<T>,
{
    fn from(value: T) -> Self {
        TemplateString(String::from(value))
    }
}



#[repr(transparent)]
#[derive(
    Clone,
    Debug,
    Default,
    PartialEq,
    Eq,
    Ord,
    PartialOrd,
    Hash,
    derive_more::Display,
    serde::Serialize,
    serde::Deserialize,
)]
pub struct TemplateStr<'a>(&'a str);

impl<'a> TemplateStr<'a> {
    pub const fn new(s: &'a str) -> Self {
        Self(s)
    }

    pub fn as_str(&self) -> &str {
        self.0
    }
}

impl<'a> AsRef<str> for TemplateStr<'a> {
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl<'a> From<&'a str> for TemplateStr<'a> {
    fn from(value: &'a str) -> Self {
        Self(value)
    }
}


/// Minimal `Template` which replace all instances of keys in the form `{{KEY}}` with
/// values defined in the render context.
///
/// ```
/// use cosmos_text_interpolation::render_str;
/// let rendered = render_str(
///     "I like to eat {{fruit}}. Yes, only {{fruit}}!",
///     [("fruit", "bananas")],
/// )
/// .expect("render failed");
///
/// assert_eq!(
///     rendered.as_str(),
///     "I like to eat bananas. Yes, only bananas!"
/// );
/// ```
#[derive(Debug, Clone, PartialEq)]
pub struct Template<'a> {
    /// The template text
    pub(crate) text:   &'a str,
    /// The unique set of keys defined within text
    pub(crate) keys:   KeySet<'a>,
    /// The ordered list of Slices (tagged Spans) representing
    /// either a run of `Slice::Text`, which is copied into the rendered string
    /// verbatim, or a `Slice::Key`, which should be replaced from the context
    /// passed to [`Template::render`].
    pub(crate) slices: Slices,
}


impl<'a> Template<'a> {
    /// Render the text template into a [`String`]. The keys within the template are replaced
    /// with the mapping defined in `substitutions`.
    pub fn render<I, K, V>(
        &self,
        substitutions: I,
    ) -> Result<String, Error>
    where
        I: IntoIterator<Item = (K, V)>,
        K: AsRef<str>,
        V: AsRef<str>,
    {
        let context = build_context(substitutions)?;

        let missing_keys = self.find_missing_context_keys(&context);
        if !missing_keys.is_empty() {
            return Err(Error::Runtime {
                err: anyhow::anyhow!("render context missing keys: missing={:?}", missing_keys),
            });
        }

        let parts = self.resolve_all_with_context(&context);

        Ok(parts.join(""))
    }

    pub fn keys(&self) -> impl Iterator<Item = &&'a str> {
        self.keys.iter()
    }

    /// Finds the keys in the template that are not present in the
    /// context map.
    fn find_missing_context_keys<'this, 'b>(
        &'this self,
        context: &'b Context,
    ) -> Vec<&'this &'a str> {
        self.keys
            .iter()
            .filter(move |key| !context.contains_key(**key))
            .collect()
    }

    /// Resolve all of the [`Span`]s using the source text for `Text` slices and substituting
    /// `Key` slices with entries from `context`. The result is a `Vec<&str>` of borrowed parts that
    /// will provide the fully rendered `String` when `Vec::<&str>::join("")` is called.
    ///
    /// This is more efficient than joining a fully owned `Vec<String>` or creating the `String` in
    /// place since the resulting `join` can precalculated the storage needed and just memcpy
    /// the `&str` parts into the resulting buffer resulting in fewer intermediate allocations
    /// and no reallocations when building the final rendered string.
    fn resolve_all_with_context<'b>(
        &'b self,
        context: &'b Context,
    ) -> Vec<&'b str> {
        let mut parts = Vec::with_capacity(self.slices.len());
        for slice in self.slices.iter() {
            match slice {
                Slice::Key(span) => {
                    let key = self.resolve_key(*span);
                    let replacement = &context[key];
                    parts.push(replacement.as_str());
                }
                Slice::Text(span) => {
                    parts.push(self.resolve_text(*span));
                }
            }
        }
        parts
    }

    /// Get the `&str` value of a `Key` represented by the [`Span`] within
    /// the text of the [`Template`]. This function will only return the
    /// name of the key and not the start or end characters for the key`
    ///
    /// `{{ MY_KEY   }}` -> `"MY_KEY"`
    ///
    /// # Panics
    ///
    /// * When [`Span`] represents an out of bounds access of the backing text.
    /// * When the bytes of the [`Span`] are not a proper UTF-8 `str`.
    fn resolve_key(
        &self,
        span: Span,
    ) -> &str {
        crate::normalize_key(self.resolve_text(span))
    }

    /// Get the `&str` for a [`Span`] within the text of the [`Template`]
    ///
    /// # Panics
    ///
    /// * When [`Span`] represents an out of bounds access of the backing text.
    /// * When the bytes of the [`Span`] are not a proper UTF-8 `str`.
    fn resolve_text(
        &self,
        span: Span,
    ) -> &'a str {
        crate::resolve_text_span(self.text, span)
    }
}


/// Build the context map from the iterator of `(key, value)` entries.
///
/// # Errors
///
/// * If `entries` has duplicate key values
fn build_context<I, K, V>(entries: I) -> Result<Context, Error>
where
    I: IntoIterator<Item = (K, V)>,
    K: AsRef<str>,
    V: AsRef<str>,
{
    let mut context = Context::default();
    for (k, v) in entries.into_iter() {
        let key = k.as_ref();
        let replacement = v.as_ref();
        if context.contains_key(key) {
            return Err(Error::Runtime {
                err: anyhow::anyhow!("duplicate context key: {}", key),
            });
        }

        context.insert(key.to_string(), replacement.to_string());
    }


    Ok(context)
}


#[cfg(test)]
mod test {
    use crate::deps::cosmos_proc_macros::cosmos;
    use crate::{
        TemplateOps,
        TemplateStr,
    };

    #[cosmos(unit_test)]
    fn template_string_keys() -> Result<(), Box<dyn std::error::Error>> {
        let template = TemplateStr::new("{{key}}");
        let keys = template.keys()?.into_vec();
        assert_eq!(keys, vec!["key"]);
        Ok(())
    }

    #[cosmos(unit_test)]
    fn template_string_render() -> Result<(), Box<dyn std::error::Error>> {
        let rendered = TemplateStr::new("{{key}}").render([("key", "foo")])?;
        assert_eq!(rendered, "foo");
        Ok(())
    }
}
