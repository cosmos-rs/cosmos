use crate::{
    Error,
    KeySet,
    Template,
    TemplateStr,
    TemplateString,
};

/// Extension trait for `str` like types to add `Template<'_>` operations directly
/// to the type. These require re-parsing the template string on every call so
/// it is less efficient reusing the parsed `Template<'_>` for multiple renders
/// of the same template.
pub trait TemplateOps: sealed::Sealed {
    fn to_template(&self) -> Result<crate::Template<'_>, Error>;

    fn is_template(&self) -> bool {
        self.to_template()
            .map(|tpl| tpl.keys().next().is_some())
            .unwrap_or(false)
    }

    fn render<I, K, V>(
        &self,
        substitutions: I,
    ) -> Result<String, Error>
    where
        Self: Sized,
        I: IntoIterator<Item = (K, V)>,
        K: AsRef<str>,
        V: AsRef<str>,
    {
        self.to_template().and_then(move |tpl| tpl.render(substitutions))
    }

    fn keys(&self) -> Result<KeySet<'_>, Error> {
        self.to_template().map(|tpl| tpl.keys)
    }
}

impl<'a> TemplateOps for &'a str {
    fn to_template(&self) -> Result<Template<'_>, Error> {
        crate::parse_str(self)
    }
}

impl<'a> TemplateOps for &'a String {
    fn to_template(&self) -> Result<Template<'_>, Error> {
        crate::parse_str(self)
    }
}


impl<'a> TemplateOps for ::std::borrow::Cow<'a, str> {
    fn to_template(&self) -> Result<Template<'_>, Error> {
        crate::parse_str(self)
    }
}

impl TemplateOps for String {
    fn to_template(&self) -> Result<Template<'_>, Error> {
        crate::parse_str(self)
    }
}


impl TemplateOps for TemplateString {
    fn to_template(&self) -> Result<Template<'_>, Error> {
        crate::parse_str(self)
    }
}

impl<'a> TemplateOps for TemplateStr<'a> {
    fn to_template(&self) -> Result<Template<'_>, Error> {
        crate::parse_str(self)
    }
}

mod sealed {
    pub trait Sealed {}
    impl<'a> Sealed for &'a str {}
    impl<'a> Sealed for &'a String {}
    impl<'a> Sealed for ::std::borrow::Cow<'a, str> {}
    impl Sealed for String {}
    impl Sealed for crate::TemplateString {}
    impl<'a> Sealed for crate::TemplateStr<'a> {}
}



#[cfg(test)]
mod test {
    use crate::deps::cosmos_proc_macros::cosmos;
    use crate::TemplateOps as _;

    #[cosmos(unit_test)]
    fn template_string_keys() -> Result<(), Box<dyn std::error::Error>> {
        let keys = "{{key}}".keys()?.into_vec();
        assert_eq!(keys, vec!["key"]);
        Ok(())
    }

    #[cosmos(unit_test)]
    fn template_string_render() -> Result<(), Box<dyn std::error::Error>> {
        let rendered = "{{key}}bar".render([("key", "foo")])?;
        assert_eq!(rendered, "foobar");
        Ok(())
    }
}
