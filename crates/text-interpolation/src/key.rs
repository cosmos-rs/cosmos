use crate::deps::indexmap::set::IntoIter;
use crate::deps::indexmap::IndexSet;
use crate::span::Span;

type Value<'a> = &'a str;

/// A key from the source text which includes a reference to the actual text
/// and the byte [`Span`] within the text
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Key<'a> {
    pub name: Value<'a>,
    pub span: Span,
}


/// A set of key values
#[derive(Debug, Clone, Default, PartialEq, derive_more::Deref, derive_more::DerefMut)]
pub struct Set<'a>(IndexSet<Value<'a>>);


impl<'a> Set<'a> {
    pub fn iter(&self) -> impl Iterator<Item = &Value<'a>> {
        self.0.iter()
    }

    pub fn into_iter(self) -> IntoIter<Value<'a>> {
        self.0.into_iter()
    }

    pub fn into_vec(self) -> Vec<Value<'a>> {
        self.into_iter().collect()
    }

    pub fn add(
        &mut self,
        value: Value<'a>,
    ) -> bool {
        self.0.insert(value)
    }
}


impl<'a> ::std::iter::FromIterator<&'a str> for Set<'a> {
    fn from_iter<T: IntoIterator<Item = &'a str>>(iter: T) -> Self {
        Self(IndexSet::from_iter(iter))
    }
}
