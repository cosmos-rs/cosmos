use crate::span::Span;

/// A tagged [`Span`] representing a range of bytes in the source  
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Slice {
    /// The span is a key including the `{{` and `}}` parts
    Key(Span),
    /// just regular, non-key, text to use verbatim in the rendered template
    Text(Span),
}

impl Slice {
    pub fn is_key(&self) -> bool {
        match self {
            Slice::Key(_) => true,
            _ => false,
        }
    }

    #[allow(dead_code)]
    pub fn is_text(&self) -> bool {
        match self {
            Slice::Text(_) => true,
            _ => false,
        }
    }

    pub fn span(&self) -> &Span {
        match self {
            Slice::Key(span) | Slice::Text(span) => span,
        }
    }
}

/// An ordered collection of [`Slice`]s
#[derive(Debug, Clone, Default, PartialEq)]
pub struct Slices(Vec<Slice>);


impl Slices {
    pub fn len(&self) -> usize {
        self.0.len()
    }

    #[allow(dead_code)]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn iter(&self) -> impl Iterator<Item = &Slice> {
        self.0.iter()
    }

    pub fn add_key(
        &mut self,
        key: Span,
    ) {
        self.0.push(Slice::Key(key));
    }

    pub fn add_text(
        &mut self,
        key: Span,
    ) {
        self.0.push(Slice::Text(key));
    }

    pub fn keys(&self) -> impl Iterator<Item = &Slice> {
        self.iter().filter(|slice| slice.is_key())
    }
}
