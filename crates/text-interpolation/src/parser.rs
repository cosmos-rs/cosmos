//! # Template Parser - How it works
//!
//! The template parser works by having by searching for the start `{{` tag of some key. When
//! a key start is found the input is parsed until the key end `}}` is found. The key identifier
//! between `{{` and `}}` must meet some basic rules:
//!
//! * The key ident cannot be blank
//! * The key ident must only contain alphanumeric ascii characters and `_`
//! * The key ident cannot start with a number
//! * The key cannot span multiple lines (no `\n` between start and end tags).
//!
//! The parser keeps some state to track its progress through the text. There is no tokenization
//! of the input since there are only three real tokens we care about `{{`, `}}` and `KEY_IDENT`.
//! The parser consumes the bytes of the input stream in order preserves all of the Text and Key
//! spans in order of the original text. The parsed [`Template`] is made of source spans (just
//! integers representing ranges) and references to the source text, so it is very lightweight.
//! Text is only copied from the source when `render()` is called on the source [`Template`].
use crate::slice::Slices;
use crate::span::Span;
use crate::{
    Error,
    KeySet,
    SourceIter,
    Template,
};

/// Use the default parser implementation to parse the template
// note this makes more sense if the parser becomes more complicated or
// a configuration is added to allow changing the key start/end tokens
pub fn parse_str<S>(s: &S) -> Result<Template, Error>
where
    S: AsRef<str>,
{
    Parser::parse(s.as_ref())
}

/// Parser will convert some `str` input into a Template  
pub struct Parser<'a> {
    source: SourceIter<'a>,
}


impl<'a> Parser<'a> {
    fn new(source: &'a str) -> Self {
        Parser {
            source: SourceIter::new(source),
        }
    }

    pub fn parse(source: &'a str) -> Result<Template<'a>, Error> {
        let mut parser = Parser::new(source);
        let mut slices = Slices::default();
        // keep track of the start of the last Slice::Text
        let mut text_start = 0usize;

        while let Some((ch1, ch2)) = parser.peek2() {
            match (ch1, ch2) {
                // the start tokens for the key
                (b'{', Some(b'{')) => {
                    // mark the end of the current text Slice::Text and add it
                    // to the collections of slices
                    let text_end = parser.pos();
                    let span = Span::from(text_start..text_end);
                    if !span.is_empty() {
                        slices.add_text(span);
                    }
                    // parse the rest of the key and add it to the end of the slices
                    slices.add_key(parser.parse_key()?);

                    // mark the start of the next text slice
                    text_start = parser.pos();
                }
                // anything else just assume it is text to replace verbatim
                // during rendering
                _ => parser.advance(),
            };
        }

        // we've reached the end so mark the end of the last text slice
        // and tag it to the end if it is not empty
        // (which should only happen if the template does not end with a {{ key }}
        let text_end = parser.pos();
        let span = Span::from(text_start..text_end);

        if !span.is_empty() {
            slices.add_text(span);
        }

        // iterate over the slices and find the Key spans and extract the key strings
        // without {{ }} tags from the source text into a set
        let keys = slices
            .keys()
            .map(move |slice| {
                let text = crate::resolve_text_span(source, *slice.span());
                crate::normalize_key(text)
            })
            .collect::<KeySet>();

        Ok(Template {
            text: source,
            keys,
            slices,
        })
    }

    /// get how many bytes of the source text have been consumed
    fn pos(&self) -> usize {
        self.source.current().offset()
    }

    /// consume the next byte of source
    fn advance(&mut self) {
        self.source.advance();
    }

    /// Consume the next two bytes of the source
    fn advance2(&mut self) {
        self.source.advance();
        self.source.advance();
    }

    /// get but do not consume the next two bytes of the source
    fn peek2(&mut self) -> Option<(u8, Option<u8>)> {
        self.source.peek2()
    }

    /// assumes the caller has verified the first two chars are `{{`
    fn parse_key(&mut self) -> Result<Span, Error> {
        let mut seen_first_char = false;
        let mut seen_last_char = false;

        let start = self.pos();
        // eat the assumed {{
        self.advance2();

        // only loop while there is at least one byte of consumable input
        while let Some((ch1, ch2)) = self.peek2() {
            match (ch1, ch2) {
                (b'}', Some(b'}')) => {
                    // the end of the key so consume the }}
                    self.advance2();
                    let end = self.pos();

                    return if seen_first_char {
                        Ok(Span::from(start..end))
                    } else {
                        // if there was only whitespace between the `{{` and `}}` (e.g. `{{    }}`)
                        Err(Error::empty_key(&self.source, Span::from(start..end)))
                    };
                }
                (b'\n', _) => {
                    // do not allow a key to span lines
                    let end = self.pos();
                    return Err(Error::error_unterminated_key(
                        &self.source,
                        Span::from(start..end),
                    ));
                }
                (b' ', _) if !seen_first_char => {
                    // eat all whitespace between {{ and the first ident character
                    self.advance();
                }
                (b, _) if !seen_first_char && is_ident_start(b) => {
                    // begin the key identifier and signal the finding the first character
                    self.advance();
                    seen_first_char = true;
                }
                (b, _) if !seen_last_char && is_ident(b) => {
                    // eat all valid ident characters as long as we haven't seen a space
                    self.advance();
                }
                (b, _) if b == b' ' => {
                    // we've seen a space but
                    seen_last_char = true;
                    self.advance();
                }
                _ => {
                    let end = self.pos();
                    return Err(Error::invalid_key_ident_char(
                        &self.source,
                        Span::from(start..end),
                        ch1,
                    ));
                }
            };
        }

        let end = self.pos();
        Err(Error::error_unterminated_key(
            &self.source,
            Span::from(start..end),
        ))
    }
}

// a key ident must start with an ascii letter or an underscore, like most programming
// language identifiers
fn is_ident_start(b: u8) -> bool {
    b.is_ascii_alphabetic() || b == b'_'
}

// after the start character allow both ascii numbers and letters
fn is_ident(b: u8) -> bool {
    b.is_ascii_alphanumeric() || b == b'_'
}



#[cfg(test)]
mod test {
    use crate::deps::cosmos_proc_macros::cosmos;
    use crate::parser::Parser;
    use crate::Error;

    #[cosmos(unit_test)]
    fn fail_on_empty_key() {
        let result = Parser::parse(r#"{{ }}"#);
        debug!(?result, failed = result.is_err(), "should fail, empty key");
        assert!(matches!(result, Err(Error::EmptyKey { .. })))
    }

    #[cosmos(unit_test)]
    fn fail_on_bad_key_chars() {
        let result = Parser::parse(r#"{{ a+b }}"#);
        debug!(
            ?result,
            failed = result.is_err(),
            "should fail, + is not a valid key character"
        );
        assert!(matches!(result, Err(Error::BadKeyCharacter { .. })))
    }

    #[cosmos(unit_test)]
    fn fail_on_unterminated_key() {
        let result = Parser::parse(
            r#"
        {{ this_is_bad
                      }}"#,
        );
        debug!(
            ?result,
            failed = result.is_err(),
            "should fail, key escapes cannot span lines"
        );
        assert!(matches!(result, Err(Error::UnterminatedKey { .. })))
    }

    #[cosmos(unit_test)]
    fn single_key() {
        let result = Parser::parse(
            r#"
        {{ single_key }}"#,
        );

        assert!(matches!(result, Ok(_)));

        let parsed_text = result.unwrap();
        let keys = parsed_text.keys().map(|s| *s).collect::<Vec<_>>();
        assert_eq!(keys, vec!["single_key"]);
    }

    #[cosmos(unit_test)]
    fn single_key_repeated() {
        let result = Parser::parse(
            r#"
       {{ single_key }}{{ single_key }}{{ single_key }}{{ single_key }}{{ single_key }}
       {{ single_key }}{{ single_key }}{{ single_key }}{{ single_key }}{{ single_key }}
       {{ single_key }}{{ single_key }}{{ single_key }}{{ single_key }}{{ single_key }}
       {{ single_key }}{{ single_key }}{{ single_key }}{{ single_key }}{{ single_key }}
       {{ single_key }}{{ single_key }}{{ single_key }}{{ single_key }}{{ single_key }}
       "#,
        );

        assert!(matches!(result, Ok(_)));

        let parsed_text = result.unwrap();
        let keys = parsed_text.keys().map(|s| *s).collect::<Vec<_>>();
        assert_eq!(keys, vec!["single_key"]);
        let key_slices = parsed_text.slices.keys().count();
        // this is the number of times the single_key is repeated
        assert_eq!(key_slices, 25);
    }


    #[cosmos(unit_test)]
    fn multiple_keys() {
        let result = Parser::parse(
            r#"
       {{ A }}{{ B }}{{ C }}{{ D }}{{ E }}
       F G H I J K
       {{L}} M {{N}} O {{P}}
       "#,
        );

        assert!(matches!(result, Ok(_)));

        let parsed_text = result.unwrap();
        let keys = parsed_text.keys().map(|s| *s).collect::<Vec<_>>();
        assert_eq!(keys, vec!["A", "B", "C", "D", "E", "L", "N", "P"]);
        let key_slices = parsed_text.slices.keys().count();
        // this is the number of times the single_key is repeated
        assert_eq!(key_slices, 8);
    }
}
