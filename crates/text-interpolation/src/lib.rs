//! # Cosmos Text Interpolation
//!
//! Minimal text templates for replacing instance of keys in the form `{{key_name}}`
//! with runtime defined "Context" values. Why not use X? Most templates libraries are
//! geared towards structured output like html. The minimal libraries like tiny-template
//! do not allow customization of template escapes in our case `{{` and `}}` and all add
//! a lot of features we do not need.
//!
//! ```
//! use cosmos_text_interpolation::render_str;
//! let rendered = render_str(
//!     "I like to eat {{fruit}}. Yes, only {{fruit}}!",
//!     [("fruit", "bananas")],
//! )
//! .expect("render failed");
//!
//! assert_eq!(
//!     rendered.as_str(),
//!     "I like to eat bananas. Yes, only bananas!"
//! );
//! ```
#![warn(rustdoc::missing_crate_level_docs)]
#![warn(missing_debug_implementations)]
#![deny(warnings)]
mod deps {
    pub use ::cosmos_crash_macros;
    pub use ::cosmos_log_macros;
    pub use ::cosmos_proc_macros;

    pub use ::anyhow;
    pub use ::derive_more;
    pub use ::indexmap;
    pub use ::serde;
    pub use ::thiserror;
}

mod error;
mod ext;
mod key;
mod parser;
mod slice;
mod source;
mod span;
mod template;

pub const KEY_START: &str = "{{";
pub const KEY_END: &str = "}}";

pub use crate::error::Error;
pub use crate::ext::TemplateOps;
use crate::key::Set as KeySet;
pub use crate::parser::parse_str;
use crate::slice::{
    Slice,
    Slices,
};
use crate::source::Iter as SourceIter;
use crate::span::Span;
pub use crate::template::{
    render_str,
    Template,
    TemplateStr,
    TemplateString,
};


/// Normalize a `"{{ KEY }}"` into a string `"KEY"` by stripping the key prefix and suffix and
/// trimming any resulting whitespace.
#[inline]
pub(crate) fn normalize_key(key: &str) -> &str {
    key.trim_start_matches(KEY_START).trim_end_matches(KEY_END).trim()
}

/// Get the `&str` for a [`Span`] within the text of the [`Template`]
///
/// # Panics
///
/// * When [`Span`] represents an out of bounds access of the backing text.
/// * When the bytes of the [`Span`] are not a proper UTF-8 `str`.
#[inline]
pub(crate) fn resolve_text_span(
    text: &str,
    span: Span,
) -> &str {
    use crate::deps::cosmos_crash_macros::crash_on_err;

    let range: ::std::ops::Range<usize> = span.into();
    let slice: &[u8] = &text.as_bytes()[range];
    std::str::from_utf8(slice).unwrap_or_else(crash_on_err!(
        "text for  span {:?} was not a valid utf-8 string",
        span
    ))
}

/// Get the `Cow<'_, str>` for a [`Span`] within the text of the [`Template`]
///
/// Unlike [`resolve_text_span`] this function will never panic.
#[inline]
pub(crate) fn resolve_byte_span_lossy(
    text: &[u8],
    span: Span,
) -> ::std::borrow::Cow<'_, str> {
    let range: ::std::ops::Range<usize> = span.into();
    let slice: &[u8] = &text[range];
    std::str::from_utf8(slice)
        .map(::std::borrow::Cow::Borrowed)
        .unwrap_or_else(|_| String::from_utf8_lossy(slice))
}
