use crate::span::Span;
use ::core::fmt;
use std::borrow::Cow;


/// Iterator over some source text that tracks the position of and detects end of input
#[derive(Copy, Clone)]
pub struct Iter<'a> {
    // input
    chars:    &'a [u8],
    // the current line
    lineno:   usize,
    // the relative position within the current line
    line_pos: usize,
    // the absolute character position within the text
    pos:      usize,
}


impl<'a> Iter<'a> {
    /// create a new `Iter<'_>` from the input string starting
    /// at the start of the string  
    pub fn new(source_text: &'a str) -> Self {
        Self {
            lineno:   1,
            line_pos: 0,
            pos:      0,
            chars:    source_text.as_bytes(),
        }
    }

    /// Peek at the next byte but do not consume it
    pub fn peek(&mut self) -> Option<u8> {
        self.byte_at(self.pos)
    }

    /// Peek at the next two bytes but not consume them
    pub fn peek2(&mut self) -> Option<(u8, Option<u8>)> {
        match self.byte_at(self.pos) {
            Some(ch1) => Some((ch1, self.byte_at(self.pos + 1))),
            None => None,
        }
    }

    /// consume one byte of the source
    pub fn advance(&mut self) -> Option<u8> {
        if let Some(ch) = self.byte_at(self.pos) {
            if ch == b'\n' {
                self.lineno += 1;
                self.line_pos = 0;
            }
            self.line_pos += 1;
            self.pos += 1;
            Some(ch)
        } else {
            None
        }
    }

    pub fn eof(&self) -> bool {
        self.pos == self.chars.len()
    }

    /// Get the current byte position
    pub const fn current(&self) -> Location {
        Location { source: self }
    }

    /// Retrieve the source text corresponding to the byte [`Span`]
    pub(crate) fn source_text(
        &self,
        span: Span,
    ) -> Cow<'_, str> {
        crate::resolve_byte_span_lossy(self.chars, span)
    }

    /// Retrieve the byte at `pos`
    fn byte_at(
        &mut self,
        pos: usize,
    ) -> Option<u8> {
        self.chars.get(pos).copied()
    }
}


impl<'a> fmt::Debug for Iter<'a> {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        let current = self.current();
        f.debug_struct("Iter").field("current", &current).finish()
    }
}



#[derive(Copy, Clone)]
pub struct Location<'a, 'b> {
    source: &'a Iter<'b>,
}


impl<'a, 'b> Location<'a, 'b> {
    pub const fn offset(&self) -> usize {
        self.source.pos
    }

    pub const fn line_number(&self) -> usize {
        self.source.lineno
    }

    pub const fn line_offset(&self) -> usize {
        self.source.line_pos
    }

    pub fn line(&self) -> Cow<'_, str> {
        let start = (self.offset() - self.line_offset()) + 1;
        let slice = &self.source.chars[start..];
        let end = start
            + slice
                .iter()
                .copied()
                .enumerate()
                .find(|(_, ch)| *ch == b'\n')
                .map(|(idx, _)| idx)
                .unwrap_or_else(|| slice.len());


        self.source.source_text(Span::from(start..end))
    }
}

impl<'a, 'b> fmt::Debug for Location<'a, 'b> {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        let line = self.line();
        f.debug_struct("Location")
            .field("offset", &self.offset())
            .field("line", &self.line_number())
            .field("column", &self.line_offset())
            .field("line_text", &line)
            .finish()
    }
}
