use crate::deps::thiserror;
use crate::span::Span;
use crate::SourceIter;

/// Errors that can occur during parsing or usage of text templates
#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("{message}")]
    BadKeyCharacter { message: String },

    #[error("{message}")]
    UnterminatedKey { message: String },

    #[error("{message}")]
    EmptyKey { message: String },

    #[error("utf8 error: {err}")]
    Utf8 {
        #[from]
        err: ::std::str::Utf8Error,
    },

    /// Catch all for errors that can occur, not handled, and for which extra type information
    /// and details would not benefit.
    #[error("runtime error: {err}")]
    Runtime {
        /// the source error for the `Runtime` error
        #[from]
        err: crate::deps::anyhow::Error,
    },
}


impl Error {
    pub fn invalid_key_ident_char(
        source: &SourceIter,
        span: Span,
        ch: u8,
    ) -> Error {
        Error::BadKeyCharacter {
            message: format!(
                "invalid character '{}' at {} when parsing key identifier: loc={:?}; key_span={:?}; key_text={:?}",
                char::from(ch),
                span.end(),
                source.current(),
                span,
                source.source_text(span),
            )
        }
    }

    pub fn error_unterminated_key(
        source: &SourceIter,
        span: Span,
    ) -> Error {
        Error::UnterminatedKey {
            message: format!(
                "unexpected end of key at {}: loc={:?}; key_span={:?}; key_text={:?}",
                span.end(),
                source.current(),
                span,
                source.source_text(span),
            ),
        }
    }

    pub fn empty_key(
        source: &SourceIter,
        span: Span,
    ) -> Error {
        Error::EmptyKey {
            message: format!(
                "empty key at {}: loc={:?}; key_span={:?}; key_text={:?}",
                span.end(),
                source.current(),
                span,
                source.source_text(span),
            ),
        }
    }
}
