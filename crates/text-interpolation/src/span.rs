use crate::deps::cosmos_crash_macros::crash_on_err;

type ByteIndex = u32;

/// The span in bytes from some source text
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Span {
    start: ByteIndex,
    end:   ByteIndex,
}


#[allow(unused)]
impl Span {
    /// Create a new span from a start position and an end position
    #[must_use]
    pub const fn new(
        start: ByteIndex,
        end: ByteIndex,
    ) -> Self {
        Span { start, end }
    }

    /// The absolute start position in unicode chars of the span within the source
    #[must_use]
    pub const fn start(&self) -> ByteIndex {
        self.start
    }

    /// The exclusive end in unicode chars of the span within the source
    #[must_use]
    pub const fn end(&self) -> ByteIndex {
        self.end
    }

    pub const fn is_empty(&self) -> bool {
        self.end <= self.start
    }
}

impl From<Span> for ::std::ops::Range<usize> {
    fn from(s: Span) -> Self {
        (s.start as usize)..(s.end as usize)
    }
}

impl From<::std::ops::Range<usize>> for Span {
    fn from(range: ::std::ops::Range<usize>) -> Self {
        let start = ByteIndex::try_from(range.start)
            .unwrap_or_else(crash_on_err!("range.start would overflow ByteIndex (u32)"));
        let end = ByteIndex::try_from(range.end)
            .unwrap_or_else(crash_on_err!("range.end would overflow ByteIndex (u32)"));
        Span { start, end }
    }
}

impl From<::std::ops::RangeInclusive<usize>> for Span {
    fn from(range: ::std::ops::RangeInclusive<usize>) -> Self {
        let start = ByteIndex::try_from(*range.start())
            .unwrap_or_else(crash_on_err!("range.start would overflow ByteIndex (u32)"));
        let end = ByteIndex::try_from(*range.end())
            .unwrap_or_else(crash_on_err!("range.end would overflow ByteIndex (u32)"));
        Span { start, end: end + 1 }
    }
}
