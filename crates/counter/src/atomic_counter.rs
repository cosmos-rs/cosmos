#[cfg(feature = "serde")]
use crate::deps::serde::{
    Deserialize,
    Serialize,
};

use crate::AtomicInt;



#[derive(Default)]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
pub struct AtomicCounter<T>(T);

impl<T> AtomicCounter<T> {
    pub const fn const_new(value: T) -> Self {
        AtomicCounter(value)
    }
}

/// A thread safe counter
impl<T> AtomicCounter<T>
where
    T: AtomicInt,
{
    #[inline(always)]
    pub fn new() -> Self {
        AtomicCounter(T::default())
    }

    #[inline(always)]
    pub fn with_start(start: <T as AtomicInt>::Int) -> Self {
        AtomicCounter(T::from(start))
    }

    #[inline(always)]
    pub fn add(
        &self,
        n: <T as AtomicInt>::Int,
    ) -> <T as AtomicInt>::Int {
        self.0.add(n)
    }

    #[inline(always)]
    pub fn inc(&self) -> <T as AtomicInt>::Int {
        self.0.inc()
    }

    #[inline(always)]
    pub fn sub(
        &self,
        value: <T as AtomicInt>::Int,
    ) -> <T as AtomicInt>::Int {
        self.0.sub(value)
    }

    #[inline(always)]
    pub fn dec(&self) -> <T as AtomicInt>::Int {
        self.0.dec()
    }

    #[inline(always)]
    pub fn value(&self) -> <T as AtomicInt>::Int {
        self.0.value()
    }

    #[inline(always)]
    pub fn set(
        &self,
        value: <T as AtomicInt>::Int,
    ) -> <T as AtomicInt>::Int {
        self.0.set(value)
    }

    #[inline(always)]
    pub fn reset(&self) -> <T as AtomicInt>::Int {
        self.0.reset()
    }
}


impl<T> std::fmt::Debug for AtomicCounter<T>
where
    T: AtomicInt,
{
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter,
    ) -> std::fmt::Result {
        f.debug_tuple("Counter").field(&self.value()).finish()
    }
}

impl<T> std::fmt::Display for AtomicCounter<T>
where
    T: AtomicInt,
{
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter,
    ) -> std::fmt::Result {
        self.value().fmt(f)
    }
}
