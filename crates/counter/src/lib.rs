#[doc(hidden)]
pub(crate) mod deps {
    pub use ::cosmos_atomic_int;

    pub use ::num_traits;
    #[cfg(feature = "serde")]
    pub use ::serde;
}


mod atomic_counter;

pub use self::atomic_counter::AtomicCounter;
use crate::deps::cosmos_atomic_int::AtomicInt;

pub type Counter = AtomicCounter<::std::sync::atomic::AtomicUsize>;
pub type CounterU8 = AtomicCounter<::std::sync::atomic::AtomicU8>;
pub type CounterU16 = AtomicCounter<::std::sync::atomic::AtomicU16>;
pub type CounterU32 = AtomicCounter<::std::sync::atomic::AtomicU32>;
pub type CounterU64 = AtomicCounter<::std::sync::atomic::AtomicU64>;
pub type CounterUsize = AtomicCounter<::std::sync::atomic::AtomicUsize>;
pub type CounterI8 = AtomicCounter<::std::sync::atomic::AtomicI8>;
pub type CounterI16 = AtomicCounter<::std::sync::atomic::AtomicI16>;
pub type CounterI32 = AtomicCounter<::std::sync::atomic::AtomicI32>;
pub type CounterI64 = AtomicCounter<::std::sync::atomic::AtomicI64>;
pub type CounterIsize = AtomicCounter<::std::sync::atomic::AtomicIsize>;
