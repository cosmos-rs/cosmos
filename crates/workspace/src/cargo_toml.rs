use crate::deps::hashbrown::HashMap;
use crate::deps::serde::{
    Deserialize,
    Serialize,
};
use crate::deps::serde_json::Value as JsonValue;
use crate::UnknownFields;


#[derive(Clone, Debug, Serialize, Deserialize, Default)]
#[non_exhaustive]
pub struct WorkspaceRoot {
    pub workspace:  Workspace,
    #[doc(hidden)]
    #[serde(flatten)]
    #[serde(default)]
    unknown_fields: UnknownFields,
}

#[derive(Clone, Debug, Serialize, Deserialize, Default)]
#[non_exhaustive]
pub struct Workspace {
    #[serde(default)]
    pub resolver:   String,
    #[serde(default)]
    pub members:    Vec<String>,
    #[doc(hidden)]
    #[serde(flatten)]
    #[serde(default)]
    unknown_fields: UnknownFields,
}


#[derive(Clone, Debug, Serialize, Deserialize, Default)]
#[non_exhaustive]
pub struct Crate {
    pub package:      Package,
    #[serde(default)]
    pub bin:          Vec<Bin>,
    #[serde(flatten)]
    pub dependencies: CrateDependencies,
    #[doc(hidden)]
    #[serde(flatten)]
    #[serde(default)]
    unknown_fields:   UnknownFields,
}

#[derive(Clone, Debug, Serialize, Deserialize, Default)]
#[non_exhaustive]
pub struct CrateDependencies {
    #[serde(rename = "build-dependencies")]
    #[serde(default)]
    pub build_dependencies: Dependencies,
    #[serde(rename = "dependencies")]
    #[serde(default)]
    pub dependencies:       Dependencies,
    #[serde(rename = "dev-dependencies")]
    #[serde(default)]
    pub dev_dependencies:   Dependencies,
    #[doc(hidden)]
    #[serde(flatten)]
    #[serde(default)]
    unknown_fields:         UnknownFields,
}


impl CrateDependencies {
    pub fn iter(&self) -> impl '_ + Iterator<Item = Dependency<'_>> {
        self.build_dependencies
            .crates
            .iter()
            .chain(self.dev_dependencies.iter())
            .chain(self.dependencies.iter())
            .map(|item| Dependency { item })
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Dependency<'a> {
    item: (&'a String, &'a JsonValue),
}

impl<'a> Dependency<'a> {
    pub fn name(&self) -> &str {
        self.item.0.as_str()
    }

    pub fn path(&self) -> Option<&str> {
        self.item.1.get("path").and_then(|path| path.as_str())
    }
}


#[derive(Clone, Debug, Serialize, Deserialize, Default)]
#[non_exhaustive]
pub struct Package {
    pub name:       String,
    #[doc(hidden)]
    #[serde(flatten)]
    #[serde(default)]
    unknown_fields: UnknownFields,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Dependencies {
    #[serde(flatten)]
    #[serde(default)]
    pub crates:     HashMap<String, JsonValue>,
    #[doc(hidden)]
    #[serde(flatten)]
    #[serde(default)]
    unknown_fields: UnknownFields,
}

impl Dependencies {
    pub fn iter(&self) -> impl Iterator<Item = (&String, &JsonValue)> {
        self.crates.iter()
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, Default)]
#[non_exhaustive]
pub struct Bin {
    pub name:       String,
    #[doc(hidden)]
    #[serde(flatten)]
    #[serde(default)]
    unknown_fields: UnknownFields,
}
