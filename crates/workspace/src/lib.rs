//! A half-replacement for Cargo Metadata to generate build time workspace manifests
#![warn(rustdoc::missing_crate_level_docs)]
#![warn(missing_debug_implementations)]
#![deny(warnings)]
#![deny(clippy::pedantic)]

#[doc(hidden)]
mod deps {
    pub use ::hashbrown;
    pub use ::quote;
    pub use ::serde;
    pub use ::serde_json;
    pub use ::toml;
}

use crate::deps::quote::quote;
use crate::deps::serde::{
    Deserialize,
    Serialize,
};
use crate::deps::serde_json::{
    Map,
    Value as JsonValue,
};
use crate::deps::toml;
use std::collections::{
    HashMap,
    VecDeque,
};
use std::iter::FromIterator;
use std::path::Path;

mod cargo_toml;


/// As part of a build.rs script, generate an include-able `<outdir>/cosmos_workspace_metadata.rs`
/// from the workspace `Cargo.toml` pointed to by `manifest_path`.
pub fn generate_metadata_file<I, O>(
    manifest: I,
    outdir: O,
) where
    I: AsRef<Path>,
    O: AsRef<Path>,
{
    let metadata = load_metadata(manifest);

    let metadata_json = serde_json::to_string(&metadata).unwrap();

    let hack = quote! {
        pub(crate) fn workspace_metadata() -> ::cosmos_workspace::Metadata {
            try_workspace_metadata().unwrap()
        }

        fn try_workspace_metadata() -> Result<::cosmos_workspace::Metadata, &'static str> {
            const BUILD_TIME_WORKSPACE_METADATA_JSON: &'static str = #metadata_json;

            ::serde_json::from_str::<::cosmos_workspace::Metadata>(BUILD_TIME_WORKSPACE_METADATA_JSON)
                .map_err(|_| "BUILD_TIME_WORKSPACE_METADATA_JSON could not be deserialized into a \
                cosmos_workspace::Metadata struct")
        }

        #[test]
        fn deserialize_compile_time_workspace_metadata() {
            assert!(try_workspace_metadata().is_ok());
        }
    }
    .to_string();

    let outpath = outdir.as_ref().join("cosmos_workspace_metadata.rs");
    std::fs::write(&outpath, &hack).unwrap();
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[non_exhaustive]
pub struct Metadata {
    pub packages:   Vec<Package>,
    #[doc(hidden)]
    #[serde(flatten)]
    #[serde(default)]
    unknown_fields: UnknownFields,
}


#[derive(Debug, Clone, Serialize, Deserialize)]
#[non_exhaustive]
pub struct Package {
    pub name:       String,
    pub binaries:   Vec<String>,
    #[doc(hidden)]
    #[serde(flatten)]
    #[serde(default)]
    unknown_fields: UnknownFields,
}

type JsonObject = Map<String, JsonValue>;

#[doc(hidden)]
#[repr(transparent)]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct UnknownFields {
    #[serde(flatten)]
    inner: JsonObject,
}

fn load_metadata<P>(path: P) -> Metadata
where
    P: AsRef<Path>,
{
    let mut crates = HashMap::new();

    let workspace_dir = path.as_ref().parent().unwrap().to_path_buf();
    let cargo_toml::WorkspaceRoot { workspace, .. } =
        toml::from_str::<cargo_toml::WorkspaceRoot>(&read_cargo_toml(&path)).unwrap();

    let mut queue = VecDeque::from_iter(workspace.members);
    while let Some(member) = queue.pop_front() {
        let cargo_toml_path = workspace_dir.join(&member).join("Cargo.toml");
        eprintln!("{}", cargo_toml_path.display());
        let cargo_toml::Crate {
            package,
            bin,
            dependencies,
            ..
        } = toml::from_str(&read_cargo_toml(&cargo_toml_path)).unwrap();
        if crates.contains_key(&package.name) {
            continue;
        }

        crates.insert(
            package.name.clone(),
            Package {
                name:           package.name,
                binaries:       bin.into_iter().map(|b| b.name).collect(),
                unknown_fields: Default::default(),
            },
        );

        for dep in dependencies.iter().filter(|d| !crates.contains_key(d.name())) {
            if let Some(path) = dep.path() {
                queue.push_back(Path::new(&member).join(path).display().to_string());
            }
        }
    }

    Metadata {
        packages:       crates.into_values().collect(),
        unknown_fields: Default::default(),
    }
}


fn read_cargo_toml<P>(path: P) -> String
where
    P: AsRef<Path>,
{
    ::std::fs::read_to_string(path).unwrap()
}
