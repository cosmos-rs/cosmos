use crate::deps::serde;
use crate::Error;
use std::fmt;

/// Database connection configuration parameters
///
/// Notes:
///
///   * If using a aws secretsmanager, match the json schema of the database configuration stored in
///     the aws secretsmanager.
#[derive(Clone, serde::Deserialize)]
pub struct Connection {
    username:   String,
    password:   String,
    engine:     String,
    host:       String,
    port:       u16,
    #[serde(rename = "dbname")]
    db_name:    String,
    #[serde(rename = "dbClusterIdentifier")]
    db_cluster: String,
}

impl Connection {
    pub fn username(&self) -> &str {
        &self.username
    }

    pub fn password(&self) -> &str {
        &self.password
    }

    pub fn engine(&self) -> &str {
        &self.engine
    }

    pub fn host(&self) -> &str {
        &self.host
    }

    pub fn port(&self) -> u16 {
        self.port
    }

    pub fn db_name(&self) -> &str {
        &self.db_name
    }

    pub fn db_cluster(&self) -> &str {
        &self.db_cluster
    }
}


impl Connection {
    pub fn to_connection_url(&self) -> Result<String, Error> {
        let protocol = self.protocol()?;
        Ok(format!(
            "{protocol}://{username}:{password}@{host}/{db_name}",
            protocol = protocol,
            username = self.username,
            password = self.password,
            host = self.host,
            db_name = self.db_name
        ))
    }

    pub fn set_db_name(
        &mut self,
        name: &str,
    ) {
        self.db_name = name.to_string();
    }

    fn protocol(&self) -> Result<&str, Error> {
        match self.engine.as_str() {
            "postgres" => Ok("postgresql"),
            other => Err(Error::msg(format!("unsupported engine: `{}`", other))),
        }
    }
}

impl fmt::Debug for Connection {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        f.debug_struct("Connection")
            .field("username", &self.username)
            .field("password", &"************")
            .field("engine", &self.host)
            .field("port", &self.port)
            .field("db_name", &self.db_name)
            .field("db_cluster", &self.db_cluster)
            .finish()
    }
}
