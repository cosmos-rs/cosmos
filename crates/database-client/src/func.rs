use crate::ty;

pub fn now() -> ty::TimestampZ {
    crate::deps::chrono::Utc::now()
}
