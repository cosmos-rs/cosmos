//! Names to map SQL types to Rust types  
pub type Integer = i32;
pub type BigInt = i64;
pub type Text = String;
pub type JsonB = crate::deps::serde_json::Value;
pub type TimestampZ = crate::deps::chrono::DateTime<crate::deps::chrono::Utc>;
pub type DoublePrecision = f64;
