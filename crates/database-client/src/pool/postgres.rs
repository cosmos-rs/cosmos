use std::future::Future;
use std::pin::Pin;
use crate::deps::anyhow::Context;
use crate::deps::cosmos_crash_macros::crash_on_err;
use crate::deps::tracing::{
    info,
    warn,
};
use crate::deps::native_tls::{
    Certificate,
    TlsConnector,
};
use crate::deps::postgres_native_tls::MakeTlsConnector;
use crate::deps::reqwest;
use crate::deps::tokio::sync::OnceCell;
use crate::deps::tokio_postgres::GenericClient;
use crate::deps::tokio_postgres::Statement;
use crate::pool::{
    Connection,
    ConnectionManager,
    ManagedConnection,
    Transaction,
};
use crate::{Error, MessageStream};
use crate::deps::tokio::sync::mpsc;
use std::sync::{Arc};
use std::task::ready;
use tokio_postgres::{AsyncMessage, Socket};
use tokio_postgres::tls::NoTlsStream;
use tracing::error;
use tokio::io::{AsyncRead, AsyncWrite};

static AWS_RDS_SSL_CERT: OnceCell<Arc<RdsSslCertificate>> = OnceCell::const_new();


const CERT_URL: &str = "https://s3.amazonaws.com/rds-downloads/rds-ca-2019-root.pem";


struct RdsSslCertificate {
    body: Vec<u8>,
}

impl AsRef<[u8]> for RdsSslCertificate {
    fn as_ref(&self) -> &[u8] {
        &self.body[..]
    }
}


impl RdsSslCertificate {
    pub async fn instance() -> &'static RdsSslCertificate {
        let cert = AWS_RDS_SSL_CERT
            .get_or_init(|| {
                async {
                    let client = reqwest::Client::new();
                    let resp = client
                        .get(CERT_URL)
                        .send()
                        .await
                        .context("failed to get RDS ssl cert")
                        .unwrap_or_else(crash_on_err!());
                    let certificate_pem = resp
                        .bytes()
                        .await
                        .context("failed to get RDS ssl cert body")
                        .unwrap_or_else(crash_on_err!())
                        .to_vec();

                    Arc::new(RdsSslCertificate {
                        body: certificate_pem,
                    })
                }
            })
            .await;

        &*cert
    }
}

pub struct Postgres(String);

impl Postgres {
    pub fn new(url: String) -> Self {
        Postgres(url)
    }
}

pub struct ConnectionX<S, T> {
    connection: tokio_postgres::Connection<S, T>,
    listener: Option<mpsc::UnboundedSender<AsyncMessage>>
}


impl<S, T> Future for ConnectionX<S, T>
    where
        S: AsyncRead + AsyncWrite + Unpin,
        T: AsyncRead + AsyncWrite + Unpin,
{
    type Output = Result<(), Error>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> std::task::Poll<Result<(), Error>> {
        while let Some(message) = ready!(self.connection.poll_message(cx)?) {
            match self.listener.as_ref() {
                None => {
                    if let AsyncMessage::Notice(notice) = message {
                        info!("{}: {}", notice.severity(), notice.message());
                    }
                }
                Some(listener) => {
                    if let Err(e) = listener.send(message) {
                        error!("could not forward message to listener {:?}", e)
                    }
                }
            }
        }
        std::task::Poll::Ready(Ok(()))
    }
}
async fn make_client(db_url: &str, listen: bool) -> Result<ClientConnection, Error> {

    let (listener, rx) = if listen {
        let (tx, rx) = mpsc::unbounded_channel();
        (Some(tx), Some(rx))
    } else {
        (None, None)
    };

    if db_url.contains("rds.amazonaws.com") {
        let cert = RdsSslCertificate::instance().await;
        let cert = Certificate::from_pem(cert.as_ref()).context("made certificate")?;
        let connector = TlsConnector::builder()
            .add_root_certificate(cert)
            .build()
            .context("built TlsConnector")?;
        let connector = MakeTlsConnector::new(connector);

        let (db_client,  connection) = match tokio_postgres::connect(&db_url, connector).await {
            Ok(v) => v,
            Err(e) => {
                anyhow::bail!("failed to connect to DB: {}", e);
            }
        };


        tokio::spawn(async move {
            let connx = ConnectionX {connection, listener};
            if let Err(e) = connx.await {
                error!("database connection error: {}", e);
            }
        });

        Ok(ClientConnection { client: db_client , rx})
    } else {
        warn!("Warning: Non-TLS connection to non-RDS DB");
        let (db_client, connection) = match tokio_postgres::connect(&db_url, tokio_postgres::NoTls).await {
            Ok(v) => v,
            Err(e) => {
                anyhow::bail!("failed to connect to DB: {}", e);
            }
        };

        tokio::spawn(async move {
            let connx = ConnectionX {connection, listener};
            if let Err(e) = connx.await {
                error!("database connection error: {}", e);
            }
        });

        Ok(ClientConnection { client: db_client,  rx })
    }
}


#[async_trait::async_trait]
impl ConnectionManager for Postgres {
    type Connection = PostgresConnection;

    /// Open the connection
    async fn open(&self) -> Result<Self::Connection, Error> {
        let client = make_client(&self.0, false).await.unwrap();

        // (&mut client.connection).await?;
        Ok(PostgresConnection::new(client).await)
    }

    async fn open_with_listener(&self, tx: tokio::sync::mpsc::UnboundedSender<AsyncMessage>) -> Result<Self::Connection, Error> {
        let mut client = make_client(&self.0, true).await.unwrap();
        let mut rx = client.rx.take().unwrap();

        tokio::spawn( async move {
            loop {
                match rx.try_recv() {
                    Ok(message) => {
                        tx.send(message).unwrap_or_default()
                    },
                    Err(tokio::sync::mpsc::error::TryRecvError::Empty) => tokio::task::yield_now().await,
                    Err(err) => {
                        warn!("{}", err);
                        break;
                    }
                }
            }
        });

        Ok(PostgresConnection::new(client).await)
    }

    async fn is_valid(
        &self,
        conn: &Self::Connection,
    ) -> bool {
        !conn.client_conn.client.is_closed()
    }
}


#[async_trait::async_trait]
impl<'a> Transaction for PostgresTransaction<'a> {
    async fn commit(self: Box<Self>) -> Result<(), anyhow::Error> {
        Ok(self.conn.commit().await?)
    }

    async fn finish(self: Box<Self>) -> Result<(), anyhow::Error> {
        Ok(self.conn.rollback().await?)
    }

    fn conn(&mut self) -> &mut dyn Connection {
        self
    }

    fn conn_ref(&self) -> &dyn Connection {
        self
    }
}

#[derive(Clone, Default)]
pub struct CachedStatements {
    statements: crate::deps::im::HashMap<&'static str, Statement, ahash::RandomState>,
}

impl CachedStatements {
    pub fn get(
        &self,
        text: &'static str,
    ) -> Option<&Statement> {
        self.statements.get(text)
    }

    pub fn insert(
        &mut self,
        text: &'static str,
        statement: Statement,
    ) {
        self.statements = self.statements.update(text, statement);
    }
}

pub struct PostgresTransaction<'a> {
    statements: CachedStatements,
    conn: tokio_postgres::Transaction<'a>,

}


pub enum RawConnection {
    Tls(tokio_postgres::Connection<Socket, postgres_native_tls::TlsStream<Socket>>),
    NoTls(tokio_postgres::Connection<Socket, NoTlsStream>),
}

impl RawConnection {
    pub fn poll_message(
        &mut self,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Result<AsyncMessage, tokio_postgres::Error>>> {
        match self {
            RawConnection::Tls(conn) => conn.poll_message(cx),
            RawConnection::NoTls(conn) => conn.poll_message(cx),
        }
    }
}

unsafe impl Sync for RawConnection {}

impl std::future::Future for RawConnection
{
    type Output = Result<(), tokio_postgres::Error>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> std::task::Poll<Result<(), tokio_postgres::Error>> {
        while let Some(message) = ready!(self.poll_message(cx)?) {
            if let AsyncMessage::Notice(notice) = message {
                info!("{}: {}", notice.severity(), notice.message());
            }
        }
        std::task::Poll::Ready(Ok(()))
    }
}


pub struct ClientConnection {
    client: tokio_postgres::Client,
    rx: Option<mpsc::UnboundedReceiver<AsyncMessage>>,
}

pub struct PostgresConnection {
    statements: CachedStatements,
    client_conn: ClientConnection,
}

impl Into<tokio_postgres::Client> for PostgresConnection {
    fn into(self) -> tokio_postgres::Client {
        self.client_conn.client
    }
}

impl Into<ClientConnection> for PostgresConnection {
    fn into(self) -> ClientConnection {
        self.client_conn
    }
}


pub trait PClient {
    type Client: Send + Sync + tokio_postgres::GenericClient;
    fn conn(&self) -> &Self::Client;
    fn conn_mut(&mut self) -> &mut Self::Client;
    fn statements(&self) -> &CachedStatements;
    fn statements_mut(&mut self) -> &mut CachedStatements;
}

pub trait PMessageQueue {
    fn try_next_message(&mut self) -> Option<AsyncMessage>;
    fn take_receiver(&mut self) -> Option<MessageStream>;
}

impl<'a> PClient for PostgresTransaction<'a> {
    type Client = tokio_postgres::Transaction<'a>;

    fn conn(&self) -> &Self::Client {
        &self.conn
    }

    fn conn_mut(&mut self) -> &mut Self::Client {
        &mut self.conn
    }

    fn statements(&self) -> &CachedStatements {
        &self.statements
    }

    fn statements_mut(&mut self) -> &mut CachedStatements {
        &mut self.statements
    }
}

impl PClient for ManagedConnection<PostgresConnection> {
    type Client = tokio_postgres::Client;

    fn conn(&self) -> &Self::Client {
        &(&**self).client_conn.client
    }

    fn conn_mut(&mut self) -> &mut Self::Client {
        &mut (&mut **self).client_conn.client
    }

    fn statements(&self) -> &CachedStatements {
        &self.statements
    }

    fn statements_mut(&mut self) -> &mut CachedStatements {
        &mut self.statements
    }
}

impl PMessageQueue for ManagedConnection<PostgresConnection> {
    fn try_next_message(&mut self) -> Option<AsyncMessage> {
        if let Some(rx) = &mut self.client_conn.rx {
            rx.try_recv().ok()
        }
        else {
            None
        }
    }

    fn take_receiver(&mut self) -> Option<MessageStream> {
        self.client_conn.rx.take()
    }
}

impl PostgresConnection {
    pub async fn new(conn: ClientConnection) -> Self {
        PostgresConnection {
            statements: CachedStatements::default(),
            client_conn: conn,
        }
    }
}

#[async_trait::async_trait]
impl<P> Connection for P
    where
        P: Send + Sync + PClient,
{
    async fn transaction(&mut self) -> Box<dyn Transaction + '_> {
        let statements = self.statements().clone();
        let tx = self.conn_mut().transaction().await.unwrap();
        Box::new(PostgresTransaction { statements, conn: tx })
    }
}
