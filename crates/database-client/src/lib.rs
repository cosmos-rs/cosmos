#[doc(hidden)]
mod deps {
    pub(crate) use ::cosmos_crash_macros;

    #[allow(unused)]
    pub(crate) use ::ahash;
    pub(crate) use ::anyhow;
    pub(crate) use ::chrono;
    pub(crate) use ::im;
    pub(crate) use ::native_tls;
    pub(crate) use ::postgres_native_tls;
    pub(crate) use ::reqwest;
    pub(crate) use ::serde;
    pub(crate) use ::serde_json;
    pub(crate) use ::tokio;
    pub(crate) use ::tokio_postgres;
    pub(crate) use ::tracing;
}



pub mod config;
pub mod func;
pub mod pool;
pub mod ty;

pub use self::pool::{
    Connection,
    Pool,
};

pub type MessageStream = tokio::sync::mpsc::UnboundedReceiver<tokio_postgres::AsyncMessage>;

pub type Error = crate::deps::anyhow::Error;
