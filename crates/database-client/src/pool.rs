use crate::deps::tokio::sync::{
    OwnedSemaphorePermit,
    Semaphore,
};
use crate::deps::tracing::info;
use crate::{Error, MessageStream};
use std::sync::{
    Arc,
    Mutex,
};
use tokio_postgres::AsyncMessage;

pub mod postgres;


#[async_trait::async_trait]
pub trait Connection: Send + Sync {
    async fn transaction(&mut self) -> Box<dyn Transaction + '_>;
}


#[async_trait::async_trait]
pub trait Transaction: Send + Sync {
    fn conn(&mut self) -> &mut dyn Connection;
    fn conn_ref(&self) -> &dyn Connection;

    async fn commit(self: Box<Self>) -> Result<(), crate::Error>;
    async fn finish(self: Box<Self>) -> Result<(), crate::Error>;
}

#[async_trait::async_trait]
pub trait ConnectionManager {
    type Connection;
    async fn open(&self) -> Result<Self::Connection, Error>;
    async fn open_with_listener(&self, tx: tokio::sync::mpsc::UnboundedSender<AsyncMessage>) -> Result<Self::Connection, Error>;
    async fn is_valid(
        &self,
        c: &Self::Connection,
    ) -> bool;
}


pub struct ConnectionPool<M: ConnectionManager> {
    connections: Arc<Mutex<Vec<M::Connection>>>,
    // The maximum number of outstanding connections
    permits:     Arc<Semaphore>,
    manager:     M,
}

pub struct ManagedConnection<T> {
    conn:        Option<T>,
    connections: Arc<Mutex<Vec<T>>>,
    #[allow(unused)]
    permit:      OwnedSemaphorePermit,
}

impl<T> std::ops::Deref for ManagedConnection<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.conn.as_ref().unwrap()
    }
}

impl<T> std::ops::DerefMut for ManagedConnection<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.conn.as_mut().unwrap()
    }
}

impl<T> Drop for ManagedConnection<T> {
    fn drop(&mut self) {
        let conn = self.conn.take().unwrap();
        self.connections
            .lock()
            .unwrap_or_else(|e| e.into_inner())
            .push(conn);
    }
}

impl<T, M> ConnectionPool<M>
where
    T: Send,
    M: ConnectionManager<Connection = T>,
{
    pub fn new(manager: M) -> Self {
        ConnectionPool {
            connections: Arc::new(Mutex::new(Vec::with_capacity(32))),
            permits: Arc::new(Semaphore::new(32)),
            manager,
        }
    }

    pub fn raw(&mut self) -> &mut M {
        &mut self.manager
    }

    pub async fn get(&self) -> Result<ManagedConnection<T>, Error> {
        let permit = self.permits.clone().acquire_owned().await?;
        let conn = {
            let mut slots = self.connections.lock().unwrap_or_else(|e| e.into_inner());
            slots.pop()
        };
        if let Some(mut c) = conn {
            if self.manager.is_valid(&mut c).await {
                return Ok(ManagedConnection {
                    conn: Some(c),
                    permit,
                    connections: self.connections.clone(),
                });
            }
        }

        let conn = self.manager.open().await?;
        Ok(ManagedConnection {
            conn: Some(conn),
            connections: self.connections.clone(),
            permit,
        })
    }
    pub async fn get_with_listener(&self) -> Result<(ManagedConnection<T>, MessageStream), Error> {
        let permit = self.permits.clone().acquire_owned().await?;
        let (tx, rx) = tokio::sync::mpsc::unbounded_channel();
        let conn = self.manager.open_with_listener(tx).await?;
        Ok((ManagedConnection {
            conn: Some(conn),
            connections: self.connections.clone(),
            permit,
        }, rx))
    }

}

pub enum PooledConnection {
    Postgres(ManagedConnection<postgres::PostgresConnection>),
    //Sqlite(ConnectionPool<sqlite::Sqlite>),
}

pub enum Pool {
    Postgres(ConnectionPool<postgres::Postgres>),
    //Sqlite(ConnectionPool<sqlite::Sqlite>),
}

impl Pool {
    pub async fn connection(&self) -> Result<PooledConnection, Error> {
        match self {
            Pool::Postgres(p) => Ok(PooledConnection::Postgres(p.get().await?))
        }
    }

    pub async fn connection_with_listener(&self) -> Result<(PooledConnection, MessageStream), Error> {
        match self {
            Pool::Postgres(p) => {
                let (conn, listener) = p.get_with_listener().await?;

                Ok((PooledConnection::Postgres(conn), listener))
            }
        }
    }


    #[tracing::instrument]
    pub fn open(uri: &str) -> Pool {
        match uri.split_once("://") {
            Some(("postgresql", _)) => {
                info!(engine = "postgres", "initializing pool");
                Pool::Postgres(ConnectionPool::new(postgres::Postgres::new(uri.into())))
            }
            Some((unsupported, _)) => {
                unimplemented!("unsupported engine {:?}", unsupported)
            }
            None => {
                panic!(
                    "{:?} does not look like a valid database connection uri (missing protocol string)",
                    uri
                )
            }
        }
    }
}
