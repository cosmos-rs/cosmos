//! # Cosmos Hierarchical Structured Text
//!
//! This crate is for loading materializing chained hierarchical text documents into a single merged
//! config object. The chaining is accomplished by embedding a `inherits` key to the relative path
//! to the parent in the text file.
//!
//! ```toml
//! # MyConfig.toml
//! inherits = "./BaseConfig.json"
//! ```
//!
//! Inheritance chains with files of potentially different serialization formats is supported.
//! The formats the ones supported by `cosmos-structured-text-transcoder` but you should probably
//! stick to `toml` since it is the flattest but there might be good reasons to use json because it
//! can be easier to generate. See `cosmos-structured-text-transcoder` for all supported formats.
//!
//! Internally, the normalization format will always be `serde_json::Value`. That is an
//! implementation detail that may change.
#![warn(rustdoc::missing_crate_level_docs)]
#![warn(missing_debug_implementations)]
#![deny(warnings)]
mod deps {
    pub(crate) use ::cosmos_crash_macros;
    #[cfg(test)]
    pub(crate) use ::cosmos_proc_macros;
    pub(crate) use ::cosmos_structured_text_transcoder;

    pub(crate) use ::anyhow;
    pub(crate) use ::indexmap;
    pub(crate) use ::thiserror;
    pub(crate) use ::tracing;
}


mod error;
pub use crate::error::Error;


use crate::deps::cosmos_crash_macros::crash_on_none;
use crate::deps::cosmos_structured_text_transcoder::backend::json::{
    json,
    Value,
};
use crate::deps::cosmos_structured_text_transcoder::{
    JsonValue,
    TextTranscoder,
};
use crate::deps::tracing::debug;
use std::path::{
    Path,
    PathBuf,
};


/// implementation key that should not be used as a field name in configs
pub const INHERITANCE_CHAIN_KEY: &str = "$inheritance_chain$";


pub fn load_all<P>(leaf_path: P) -> Result<ChainedConfigDescriptors, crate::Error>
where
    P: AsRef<Path>,
{
    let mut configs = crate::deps::indexmap::IndexMap::new();

    let mut filepath = Some(leaf_path.as_ref().to_owned());

    while let Some(path) = filepath.take() {
        debug!(path=%path.display(), "loading config");

        if let Some(_) = configs.get(path.as_path()) {
            return Err(Error::FileCycle {
                path:  path.clone(),
                chain: ChainedConfigDescriptors {
                    configs: configs
                        .clone()
                        .into_iter()
                        .map(|(_, v): (PathBuf, ConfigDescriptor)| v.clone())
                        .collect(),
                },
            });
        }

        let transcoder = TextTranscoder::from_path(&path).map_err(|err| {
            Error::FileLoad {
                err:     err.into(),
                path:    path.clone(),
                chain:   ChainedConfigDescriptors {
                    configs: configs
                        .clone()
                        .into_iter()
                        .map(|(_, v): (PathBuf, ConfigDescriptor)| v.clone())
                        .collect(),
                },
                message: "unable to open file for reading".into(),
            }
        })?;

        let config: JsonValue = transcoder.transcode_json()?;
        match config.get("inherits") {
            Some(Value::String(next_path)) if !next_path.is_empty() => {
                let parent = path.parent().unwrap_or_else(|| Path::new(""));
                let path = parent.join(next_path);
                let path = path.canonicalize().unwrap_or_else(|_| path.clone());

                filepath = Some(path);
            }
            _ => {}
        }

        configs.insert(path.clone(), ConfigDescriptor { path, json: config });
    }

    Ok(ChainedConfigDescriptors {
        configs: configs.into_iter().map(|(_, v)| v).collect(),
    })
}


/// Contains the ordered list of `ConfigDescriptor` instances which
/// chain loaded from following the paths in the `inherits` keys in the config
/// files.
#[derive(Clone, Debug, Default, serde::Serialize, serde::Deserialize)]
pub struct ChainedConfigDescriptors {
    configs: Vec<ConfigDescriptor>,
}


impl ChainedConfigDescriptors {
    /// Flatten the individual Config descriptors into a singular value.
    pub fn flattened(&self) -> Result<Flattened<'_>, Error> {
        let mut value = Value::Object(Default::default());

        let mut inherits = vec![];
        // reverse walk the config descriptors which will go from eldest ancestor to
        // the config leaf merging "json objects". Keys that do not
        // exist are inserted into the result while ones that do exist are
        // replaced with the value that occurs later in the inheritance chain.
        for descriptor in self.configs.iter().rev() {
            merge(&mut value, descriptor.json(), |key| key != "inherits")?;
            inherits.push(descriptor.into());
        }

        let obj = value.as_object_mut().unwrap_or_else(crash_on_none!());
        obj.insert(INHERITANCE_CHAIN_KEY.into(), Value::Array(inherits));
        Ok(Flattened { source: self, value })
    }
}

/// Keeps track of the specific loaded configuration value (i.e. a link in the chain) and the file
/// path from which the value was loaded.
#[derive(Clone, Debug, serde::Serialize, serde::Deserialize)]
struct ConfigDescriptor {
    path: PathBuf,
    json: JsonValue,
}


impl ConfigDescriptor {
    #[must_use]
    pub const fn json(&self) -> &JsonValue {
        &self.json
    }
}

impl From<&ConfigDescriptor> for JsonValue {
    fn from(descriptor: &ConfigDescriptor) -> JsonValue {
        json!({
            "path": descriptor.path.to_string_lossy().to_string(),
            "json": descriptor.json.clone(),
        })
    }
}



/// The flattened Json object with a reference back to the chained descriptors from which
/// it was formed.
#[derive(Debug, Clone)]
pub struct Flattened<'a> {
    source: &'a ChainedConfigDescriptors,
    value:  JsonValue,
}


impl<'a> Flattened<'a> {
    /// Get the source descriptor chain
    #[must_use]
    pub fn source_descriptors(&self) -> &'a ChainedConfigDescriptors {
        self.source
    }

    /// Get the reference to the flattened json object
    #[must_use]
    pub fn json(&self) -> &JsonValue {
        &self.value
    }

    /// Cast the flattened json object into `T`. This is accomplished by
    /// stringifying the flattened json and using `serde_json` to reinflate
    /// an instance of `T` from that string.
    ///
    /// # Errors
    ///
    /// * When the flattened json object string does not represent an instance of `T`
    pub fn cast<T>(&self) -> Result<T, Error>
    where
        T: for<'de> serde::Deserialize<'de>,
    {
        let mut json_value = self.value.clone();
        json_value
            .as_object_mut()
            .map(|o| o.remove(INHERITANCE_CHAIN_KEY));

        let value = cosmos_structured_text_transcoder::backend::json::to_string(&json_value)
            .and_then(|s| cosmos_structured_text_transcoder::backend::json::from_str(&s))
            .map_err(|err| {
                Error::BadCast {
                    r#type: ::std::any::type_name::<T>(),
                    value: self.value.to_string(),
                    err,
                }
            })?;
        Ok(value)
    }
}



/// This merge is just a key replacement which is a reasonable default policy
/// for merging objects otherwise things get complicated when merging arrays.
pub fn merge<F>(
    obj: &mut JsonValue,
    patch: &JsonValue,
    filter: F,
) -> Result<(), Error>
where
    F: (FnOnce(&str) -> bool) + Copy,
{
    if !patch.is_object() {
        *obj = patch.clone();
        return Ok(());
    }

    if !obj.is_object() {
        *obj = Value::Object(Default::default());
    }

    let object_map = obj.as_object_mut().unwrap_or_else(crash_on_none!());
    let patch_object_map = patch.as_object().unwrap_or_else(crash_on_none!());

    for (key, value) in patch_object_map {
        if !(filter)(key.as_str()) {
            continue;
        }

        // remove keys explicitly set to null
        match value {
            Value::Null => {
                object_map.remove(key.as_str());
            }
            _ => {
                merge(
                    object_map.entry(key.as_str()).or_insert(Value::Null),
                    value,
                    filter,
                )?;
            }
        }
    }
    Ok(())
}


#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use crate::deps::cosmos_proc_macros::cosmos;
    use crate::deps::cosmos_structured_text_transcoder::backend::json::{
        json,
        Value,
    };

    use super::{
        ChainedConfigDescriptors,
        ConfigDescriptor,
        INHERITANCE_CHAIN_KEY,
    };

    #[cosmos(unit_test)]
    fn flattening_chained() -> anyhow::Result<()> {
        #[derive(Clone, Debug, PartialEq, serde::Serialize, serde::Deserialize)]
        pub struct Person {
            pub phones:   Vec<String>,
            pub name:     String,
            pub age:      usize,
            pub birthday: String,
            pub friends:  ::std::collections::BTreeMap<String, bool>,
            #[serde(flatten)]
            #[serde(skip_serializing)]
            pub other:    std::collections::BTreeMap<String, Value>,
        }

        // what you might get after calling load_all()?;
        let chained = ChainedConfigDescriptors {
            configs: vec![
                ConfigDescriptor {
                    path: PathBuf::from("json.config.json"),
                    json: json!({
                        "inherits": "./person.config.json",
                        "name": "John Doe",
                        "age": 43,
                        "friends": {
                            "bar": false
                        },
                        "phones": [
                            "+44 1234567",
                            "+44 2345678"
                        ]
                    }),
                },
                ConfigDescriptor {
                    path: PathBuf::from("person.config.json"),
                    json: json!({
                        "inherits": null,
                        "birthday": "1900-01-01",
                        "age": 121,
                        "friends": {
                            "foo": true
                        },
                        "phones": [
                            "+1 206 555 5555"
                        ]
                    }),
                },
            ],
        };
        debug!("chained config wrapper:\n{:#?}", chained);

        // 1-  can it be flattened into a single value?
        let flattened = chained.flattened()?;
        debug!("\n\nflattened json:\n{:#?}", flattened.json());

        let mut js = flattened.json().clone();
        js.as_object_mut().map(|o| o.remove(INHERITANCE_CHAIN_KEY));

        // 2 - does the flattened value match the expected value
        // excluding the INHERITANCE_CHAIN_KEY.
        assert_eq!(
            js,
            json!({
                "name": "John Doe",
                "birthday": "1900-01-01",
                "age": 43,
                "friends": {
                    "foo": true,
                    "bar": false
                },
                "phones": [
                    "+44 1234567",
                    "+44 2345678"
                ]
            })
        );

        // 3 - is the flattened value castable to Person?
        let person: Person = flattened.cast()?;
        debug!("\n\nreinflated-strongly-typed:\n{:#?}", person);

        // 4 - does the casted value match the expected value
        assert_eq!(
            person,
            Person {
                phones:   vec!["+44 1234567".to_string(), "+44 2345678".to_string(),],
                name:     "John Doe".to_string(),
                age:      43,
                birthday: "1900-01-01".to_string(),
                friends:  {
                    let mut m = std::collections::BTreeMap::new();
                    m.insert("bar".into(), false);
                    m.insert("foo".into(), true);
                    m
                },
                other:    Default::default(),
            }
        );

        Ok(())
    }
}
