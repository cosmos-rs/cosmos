use crate::deps::thiserror;
use std::borrow::Cow;
use std::path::PathBuf;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("io error - {err}")]
    Io {
        #[from]
        err: std::io::Error,
    },
    #[error("file load error - could not load config file: path={path:?}; message={message:?}; error={err}; chain={chain:?};")]
    FileLoad {
        err:     crate::deps::anyhow::Error,
        path:    PathBuf,
        chain:   crate::ChainedConfigDescriptors,
        message: Cow<'static, str>,
    },
    #[error("file cycle error - file was previously loaded and would cause an infinite cycle: path={path:?}; chain={chain:?};")]
    FileCycle {
        path:  PathBuf,
        chain: crate::ChainedConfigDescriptors,
    },
    #[error("json error - file was not valid json: path={path:?}; error={err}")]
    FileNotJson {
        err:  crate::deps::cosmos_structured_text_transcoder::backend::json::Error,
        path: PathBuf,
    },
    #[error("transcode error: {err}")]
    Transcode {
        #[from]
        err: crate::deps::cosmos_structured_text_transcoder::Error,
    },
    #[error("bad cast - could not cast json into value: type={r#type}; err={err}; value={value}")]
    BadCast {
        r#type: &'static str,
        value:  String,
        err:    crate::deps::cosmos_structured_text_transcoder::backend::json::Error,
    },
}
