//! Traits for cosmos_proc_macro `#[derive(Trait)]`
#![warn(rustdoc::missing_crate_level_docs)]
#![warn(missing_debug_implementations)]
#![warn(missing_docs)]
#![deny(warnings)]
#![deny(clippy::pedantic)]


#[allow(unused_imports)]
#[macro_use]
extern crate cosmos_proc_macros;

/// Derives for enums
pub mod derive_enum {
    pub use ::cosmos_proc_macros::NamedVariants;
    pub use ::cosmos_proc_macros::VariantCount;

    /// Derivable on enums to get the name of a variant
    pub trait NamedVariants {
        /// return the name of the variant
        fn variant_name(&self) -> &'static str;
    }

    /// Derivable on enums to get the total number of variants
    pub trait VariantCount {
        /// return the name of the variant
        fn variant_count() -> usize;
    }
}
