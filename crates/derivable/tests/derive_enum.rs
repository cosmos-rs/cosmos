#![allow(unused)]
use cosmos_derivable::derive_enum::{
    NamedVariants,
    VariantCount,
};
use cosmos_proc_macros::cosmos;



#[cosmos(unit_test)]
fn name_eq_variant_name() {
    #[derive(NamedVariants)]
    enum Bird {
        Sparrow,
    }

    debug!(variant_name=%Bird::Sparrow.variant_name());
    assert_eq!(Bird::Sparrow.variant_name(), "Sparrow");

    #[derive(NamedVariants)]
    enum Reptile {
        Snake { poisonous: bool },
    }

    debug!(variant_name=%Reptile::Snake { poisonous: true }.variant_name());
    assert_eq!(Reptile::Snake { poisonous: true }.variant_name(), "Snake");
}


#[cosmos(unit_test)]
fn variant_count_expected() {
    #[derive(VariantCount)]
    enum Empty {}

    assert_eq!(Empty::variant_count(), 0);


    #[derive(VariantCount)]
    enum Bird {
        Sparrow,
    }

    debug!(variant_name=%Bird::variant_count());
    assert_eq!(Bird::variant_count(), 1);


    #[derive(VariantCount)]
    enum Fish {
        Red,
        Blue,
        Green,
        Yellow,
    }

    assert_eq!(Fish::variant_count(), 4);


    #[derive(VariantCount)]
    enum Path {
        Road { dirt: bool },
        Sidewalk(u32),
        NatureTrail(Bird),
        Middle,
        Highway,
        Wormhole,
    }

    // usable as a trait and const context
    const PATH_COUNT: usize = Path::variant_count();
    assert_eq!(PATH_COUNT, 6);
}
