use proc_macro2::{
    Span,
    TokenStream,
};
use quote::{
    quote,
    ToTokens,
};
use syn::{
    spanned::Spanned as _,
    Meta,
};

use crate::internals::{
    CALL_ONCE,
    UNIT_TEST,
};


#[derive(Debug, Default)]
pub(crate) struct Attrs {
    /// `#[cosmos(call_once)]`
    pub(crate) call_once: Option<Span>,

    /// `#[cosmos(unit_test)]`
    pub(crate) unit_test: Option<Span>,
}


pub(crate) struct Context {
    pub(crate) errors:         Vec<syn::Error>,
    pub(crate) named_variants: TokenStream,
    pub(crate) variant_count:  TokenStream,
}


impl Context {
    /// Construct a new context.
    pub(crate) fn new() -> Self {
        Self::with_module(&quote!(crate))
    }

    /// Construct a new context.
    pub(crate) fn with_module<M>(module: M) -> Self
    where
        M: Copy + ToTokens,
    {
        Self {
            errors:         Vec::new(),
            named_variants: quote!(#module::NamedVariants),
            variant_count:  quote!(#module::VariantCount),
        }
    }

    /// Parse the `meta` of a `#[cosmos(<meta>)]` attribute
    pub(crate) fn parse_meta(
        &mut self,
        meta: &syn::Meta,
    ) -> Option<Attrs> {
        let mut attrs = Attrs::default();

        match &meta {
            // Parse `#[cosmos(call_once)]`
            Meta::Path(word) if *word == CALL_ONCE => {
                attrs.call_once = Some(meta.span());
            }
            // Parse `#[cosmos(unit_test)]`
            Meta::Path(word) if *word == UNIT_TEST => {
                attrs.unit_test = Some(meta.span());
            }
            meta => {
                self.errors
                    .push(syn::Error::new_spanned(meta, format!("unsupported attribute")));

                return None;
            }
        }

        Some(attrs)
    }
}
