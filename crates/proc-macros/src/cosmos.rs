use proc_macro2::TokenStream;
use syn::parse::{
    Parse,
    Parser,
};

use crate::context::Context;

pub(crate) struct CosmosAttribute {
    input: syn::Meta,
}

impl syn::parse::Parse for CosmosAttribute {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        Ok(Self {
            input: input.parse()?,
        })
    }
}


impl CosmosAttribute {
    pub(crate) fn expand(
        self,
        input: proc_macro::TokenStream,
    ) -> Result<TokenStream, Vec<syn::Error>> {
        let CosmosAttribute { input: attr } = self;

        let mut ctx = Context::new();
        let attrs = ctx.parse_meta(&attr).unwrap_or_default();

        // #[cosmos(call_once)]
        let stream = if let Some(_) = attrs.call_once.as_ref() {
            expand_call_once(ctx, input)?

        // #[cosmos(unit_test)]
        } else if let Some(_) = attrs.unit_test.as_ref() {
            expand_unit_test(ctx, input)?
        } else {
            ctx.errors.push(syn::Error::new_spanned(
                attr,
                format!("unsupported attribute {:?}", attrs),
            ));
            return Err(ctx.errors);
        };

        Ok(stream)
    }
}


fn expand_call_once(
    mut ctx: Context,
    input: proc_macro::TokenStream,
) -> Result<TokenStream, Vec<syn::Error>> {
    use crate::call_once::CallOnce;
    let expander = match CallOnce::parse.parse(input) {
        Ok(call_once) => call_once,
        Err(err) => {
            ctx.errors.push(err);
            return Err(ctx.errors);
        }
    };

    expander.expand(ctx)
}


fn expand_unit_test(
    mut ctx: Context,
    input: proc_macro::TokenStream,
) -> Result<TokenStream, Vec<syn::Error>> {
    use crate::unit_test::UnitTest;
    let expander = match UnitTest::parse.parse(input) {
        Ok(call_once) => call_once,
        Err(err) => {
            ctx.errors.push(err);
            return Err(ctx.errors);
        }
    };

    expander.expand(ctx)
}

