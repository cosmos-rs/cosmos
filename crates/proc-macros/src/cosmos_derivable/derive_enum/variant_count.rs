use crate::context::Context;
use proc_macro2::TokenStream;
use quote::{
    quote,
    quote_spanned,
};
use syn::spanned::Spanned as _;

/// Derive implementation of Enum Variant names
pub struct Derive {
    input: syn::DeriveInput,
}

impl syn::parse::Parse for Derive {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        Ok(Self {
            input: input.parse()?,
        })
    }
}

impl Derive {
    pub(crate) fn expand(self) -> Result<TokenStream, Vec<syn::Error>> {
        let mut expander = Expander {
            ctx: Context::with_module(&quote!(::cosmos_derivable::derive_enum)),
        };

        match &self.input.data {
            syn::Data::Enum(en) => {
                if let Some(stream) = expander.expand_enum(&self.input, en) {
                    return Ok(stream);
                }
            }
            syn::Data::Struct(st) => {
                expander.ctx.errors.push(syn::Error::new_spanned(
                    st.struct_token,
                    "not supported on structs, only enums",
                ));
            }
            syn::Data::Union(un) => {
                expander.ctx.errors.push(syn::Error::new_spanned(
                    un.union_token,
                    "not supported on unions, only enums",
                ));
            }
        }

        Err(expander.ctx.errors)
    }
}

struct Expander {
    ctx: Context,
}



impl Expander {
    /// Expand on a struct.
    fn expand_enum(
        &mut self,
        input: &syn::DeriveInput,
        st: &syn::DataEnum,
    ) -> Option<TokenStream> {
        let count = st.variants.len().to_string();
        let count_token = syn::LitInt::new(&count, st.variants.span());

        let ident = &input.ident;
        let named_variants = &self.ctx.variant_count;


        Some(quote_spanned! { input.span() =>
            impl #ident {
                pub const fn variant_count() -> usize {
                    #count_token
                }
            }

            impl #named_variants for #ident {
                fn variant_count() -> usize {
                    #ident::variant_count()
                }
            }
        })
    }
}
