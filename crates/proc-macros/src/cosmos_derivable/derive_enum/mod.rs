pub mod named_variants;
pub mod variant_count;

pub use self::named_variants::Derive as NamedVariants;
pub use self::variant_count::Derive as VariantCount;
