use crate::context::Context;
use proc_macro2::TokenStream;
use quote::{
    quote,
    quote_spanned,
};
use syn::spanned::Spanned as _;

/// Derive implementation of Enum Variant names
pub struct Derive {
    pub(crate) input: syn::DeriveInput,
}

impl syn::parse::Parse for Derive {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        Ok(Self {
            input: input.parse()?,
        })
    }
}

impl Derive {
    pub(crate) fn expand(self) -> Result<TokenStream, Vec<syn::Error>> {
        expand(&self.input)
    }
}

pub fn expand(input: &syn::DeriveInput) -> Result<TokenStream, Vec<syn::Error>> {
    let mut expander = Expander {
        ctx: Context::with_module(&quote!(::cosmos_derivable::derive_enum)),
    };

    match &input.data {
        syn::Data::Enum(en) => {
            if let Some(stream) = expander.expand_enum(&input, en) {
                return Ok(stream);
            }
        }
        syn::Data::Struct(st) => {
            expander.ctx.errors.push(syn::Error::new_spanned(
                st.struct_token,
                "not supported on structs, only enums",
            ));
        }
        syn::Data::Union(un) => {
            expander.ctx.errors.push(syn::Error::new_spanned(
                un.union_token,
                "not supported on unions, only enums",
            ));
        }
    }

    Err(expander.ctx.errors)
}

struct Expander {
    ctx: Context,
}



impl Expander {
    /// Expand on a struct.
    fn expand_enum(
        &mut self,
        input: &syn::DeriveInput,
        st: &syn::DataEnum,
    ) -> Option<TokenStream> {
        let mut impl_enum_variant_name = Vec::new();

        for variant in &st.variants {
            let expanded = self.expand_variant_fields(variant, &variant.fields)?;
            impl_enum_variant_name.push(expanded);
        }

        let ident = &input.ident;
        let named_variants = &self.ctx.named_variants;


        Some(quote_spanned! { input.span() =>
            impl #named_variants for #ident {
                fn variant_name(&self) -> &'static str {
                    match self {
                        #(#impl_enum_variant_name,)*
                        #[allow(unused)]
                        _ => "<Variant>"
                    }
                }
            }
        })
    }

    /// map variant to name.
    fn expand_variant_fields(
        &mut self,
        variant: &syn::Variant,
        fields: &syn::Fields,
    ) -> Option<TokenStream> {
        let ident = &variant.ident;
        let lit: syn::Lit = syn::Lit::Str(syn::LitStr::new(&variant.ident.to_string(), variant.ident.span()));

        let expanded_variant = match &fields {
            syn::Fields::Named(named) => {
                quote_spanned! { named.span() => Self::#ident { .. } => { #lit } }
            }
            syn::Fields::Unnamed(unnamed) => {
                quote_spanned! { unnamed.span() => Self::#ident(..) => { #lit }}
            }
            syn::Fields::Unit => {
                quote_spanned! { ident.span() => Self::#ident  => { #lit } }
            }
        };

        Some(expanded_variant)
    }
}
