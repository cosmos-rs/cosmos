use proc_macro2::TokenStream;
use quote::quote_spanned;
use syn::spanned::Spanned as _;

use crate::context::Context;


pub struct UnitTest {
    input: syn::Item,
}

impl syn::parse::Parse for UnitTest {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        Ok(Self {
            input: input.parse()?,
        })
    }
}

impl UnitTest {
    pub(super) fn expand(
        self,
        ctx: Context,
    ) -> Result<TokenStream, Vec<syn::Error>> {
        let mut expander = Expander { ctx };

        expander
            .expand_item(&self.input)
            .ok_or_else(|| expander.ctx.errors)
    }
}

struct Expander {
    ctx: Context,
}

impl Expander {
    fn expand_item(
        &mut self,
        item: &syn::Item,
    ) -> Option<TokenStream> {
        match item {
            syn::Item::Fn(item_fn) => self.expand_function(item_fn),
            _ => {
                self.ctx
                    .errors
                    .push(syn::Error::new_spanned(item, "only function are supported"));
                None
            }
        }
    }

    fn expand_function(
        &mut self,
        input: &syn::ItemFn,
    ) -> Option<TokenStream> {
        let vis = &input.vis;
        let sig = &input.sig;
        let block = &input.block;
        let name_lit: syn::Lit = syn::Lit::Str(syn::LitStr::new(
            &input.sig.ident.to_string(),
            input.sig.ident.span(),
        ));

        let unit_test_impl = quote_spanned! { input.span() =>
            #[test]
            #vis #sig {
                #[allow(unused_imports)]
                use ::tracing::{info, debug, warn, error, trace};
                ::cosmos_unit_test_logger::init();
                info!(test=%#name_lit, "running");
                #block
            }
        };

        Some(quote_spanned! { input.span() => #unit_test_impl})
    }
}
