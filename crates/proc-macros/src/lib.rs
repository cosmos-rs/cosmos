extern crate proc_macro;


mod call_once;
mod context;
mod cosmos;
mod cosmos_derivable;
mod internals;
mod unit_test;


/// Derive to implement `cosmos_derivable::derive_enum::NamedVariant`.
#[proc_macro_derive(NamedVariants, attributes(cosmos))]
#[doc(hidden)]
pub fn derive_enum_variant_name(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let derive = syn::parse_macro_input!(input as self::cosmos_derivable::derive_enum::NamedVariants);
    derive.expand().unwrap_or_else(to_compile_errors).into()
}


/// Derive to implement `cosmos_derivable::derive_enum::VariantCount`.
#[proc_macro_derive(VariantCount, attributes(cosmos))]
#[doc(hidden)]
pub fn derive_enum_variant_count(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let derive = syn::parse_macro_input!(input as self::cosmos_derivable::derive_enum::VariantCount);
    derive.expand().unwrap_or_else(to_compile_errors).into()
}


/// entrypoint for non-derive `#[cosmos(...)]` macros
#[proc_macro_attribute]
#[doc(hidden)]
pub fn cosmos(
    attr: proc_macro::TokenStream,
    input: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let attr = syn::parse_macro_input! { attr as cosmos::CosmosAttribute };
    attr.expand(input).unwrap_or_else(to_compile_errors).into()
}


fn to_compile_errors(errors: Vec<syn::Error>) -> proc_macro2::TokenStream {
    let compile_errors = errors.iter().map(syn::Error::to_compile_error);
    ::quote::quote!(#(#compile_errors)*)
}
