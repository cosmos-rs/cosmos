use crate::deps::cosmos_workspace::Metadata;
pub use crate::deps::cosmos_workspace::Package;


mod generated {
    include! {concat!(env!("OUT_DIR"), "/cosmos_workspace_metadata.rs")}
}


pub fn workspace_packages() -> Result<Vec<Package>, String> {
    let Metadata { mut packages, .. } = generated::workspace_metadata();
    packages.sort_unstable_by(|left, right| left.name.cmp(&right.name));
    Ok(packages)
}
