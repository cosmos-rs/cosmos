//! # cosmos-build-extras
//!
//!
//! Utilities to reduce copy pasta in build scripts
#![deny(warnings)]
#![deny(clippy::pedantic)]
#![allow(clippy::needless_doctest_main)]

#[doc(hidden)]
pub(crate) mod deps {
    pub use ::chrono;
    pub use ::cosmos_workspace;
}


pub mod cargo;

#[macro_use]
pub mod meta;
