//! # build-meta
//!
//! Capture and use build metadata within a main compilation. The main use is to produce
//! a rich build info string in the form:
//!
//! ```text
//! lib:  <crate>                 version: v<major>.<minor>.<patch>   build:  <profile>.<date>.<build-id>.[<user>]?   source: <commit-hash> <commit-date> [(dirty)]?
//! lib:  cosmos_wormhole         version: v4.0.0                     build: release.2021-04-13.6044012680.dillon   source: ab61df28 2021-04-12
//! ```
//!
//! To enable this crate you will need to:
//!
//! * Add build-meta as a build dependency
//! * Add build-meta as a regular dependency
//! * Add a line to your build.rs to add the required environment variables to the rustc environment
//! * Use the `build_version_info!()` in your crate to get an instance of the [`VersionInfo`] struct
//!
//! ```toml
//! # Cargo.toml
//! name = "xyz"
//! version = "1.0.0"
//! authors = ["Dillon Hicks <project+comsosrs@dillonhicks.io>"]
//! edition = "2021"
//! publish = false
//! description = "abc's that xyz"
//!
//! [build-dependencies]
//! build-meta = { version = "1.0.0", path = "../build-meta" }
//!
//!
//! [dependencies]
//! build-meta = { version = "1.0.0", path = "../build-meta" }
//! ```
//!
//! **build.rs**
//!
//! ```rust
//! fn main() {
//!     println!("{}", cosmos_build_meta::cargo_rustc_env());
//! }
//! ```
//!
//! **main.rs**
//!
//! ```rust
//! fn main() {
//!     println!("{}", cosmos_build_meta::build_version_info!());
//! }
//! ```
//!
//! ## Important Notes
//!
//! * This implementation depends on the generation of a source-info.txt file.
#![deny(warnings)]
#![deny(clippy::pedantic)]
#![allow(clippy::needless_doctest_main)]

pub const COLUMN_SEP: &str = "      ";

#[link(name = "c")]
extern "C" {
    fn getuid() -> u32;
}


#[macro_export]
macro_rules! build_version_info {
    () => {{
        let major = env!("CARGO_PKG_VERSION_MAJOR").parse::<u8>().unwrap();
        let minor = env!("CARGO_PKG_VERSION_MINOR").parse::<u8>().unwrap();
        let patch = env!("CARGO_PKG_VERSION_PATCH").parse::<u16>().unwrap();
        let crate_name = env!("CARGO_PKG_NAME");

        let host_compiler = option_env!("COSMOS_BUILD_META_RUSTC_VERSION");
        let commit_hash = option_env!("COSMOS_BUILD_META_GIT_HASH");
        let commit_date = option_env!("COSMOS_BUILD_META_COMMIT_DATE");
        let event_id = option_env!("COSMOS_BUILD_META_EVENT_ID");
        let profile = option_env!("COSMOS_BUILD_META_PROFILE");
        let user = option_env!("COSMOS_BUILD_META_USER");
        let dirty_workspace = option_env!("COSMOS_BUILD_META_WORKSPACE_DIRTY")
            .unwrap_or_default()
            .parse::<usize>()
            .unwrap_or_default();
        let date = option_env!("COSMOS_BUILD_META_DATE");

        $crate::meta::VersionInfo {
            major,
            minor,
            patch,
            host_compiler,
            commit_hash,
            commit_date,
            crate_name,
            user,
            profile,
            event_id,
            date,
            dirty_workspace: dirty_workspace > 0,
        }
    }};
}


#[must_use]
pub fn cargo_rustc_env() -> String {
    let SourceInfo {
        commit_hash: hash,
        commit_date: cdate,
    } = SourceInfo::load().unwrap_or_default();

    format!(
        r#"
cargo:rustc-env=COSMOS_BUILD_META_USER={}
cargo:rustc-env=COSMOS_BUILD_META_PROFILE={}
cargo:rustc-env=COSMOS_BUILD_META_RUSTC_VERSION={}
cargo:rustc-env=COSMOS_BUILD_META_GIT_HASH={}
cargo:rustc-env=COSMOS_BUILD_META_COMMIT_DATE={}
cargo:rustc-env=COSMOS_BUILD_META_EVENT_ID={}
cargo:rustc-env=COSMOS_BUILD_META_WORKSPACE_DIRTY={}
cargo:rustc-env=COSMOS_BUILD_META_DATE={}
"#,
        user().unwrap_or_default(),
        std::env::var("COSMOS_CARGO_PROFILE")
            .or_else(|_| std::env::var("PROFILE"))
            .unwrap_or_default(),
        rustc_version().unwrap_or_default(),
        commit_hash().unwrap_or(hash),
        commit_date().unwrap_or(cdate),
        build_event_id().unwrap_or_default(),
        git_status().unwrap_or_default().lines().count(),
        build_date()
    )
}



/// The source-info.txt file is generated by the  ./build-tools/cargo.sh
#[derive(Debug, Default)]
pub struct SourceInfo {
    pub commit_hash: String,
    pub commit_date: String,
}


impl SourceInfo {
    /// # Errors
    ///
    /// * When reading from the `source-info.txt` file fails
    pub fn load() -> Result<Self, Box<dyn std::error::Error>> {
        let path = std::path::PathBuf::from(source_root())
            .join("build")
            .join("private")
            .join("source-info.txt");

        let text = std::fs::read_to_string(path)?;
        let mut lines = text.lines();

        Ok(Self {
            commit_hash: lines.next().unwrap_or_default().to_string(),
            commit_date: lines.next().unwrap_or_default().to_string(),
        })
    }
}

// some code taken and adapted from RLS/cargo/clippy
#[derive(Debug)]
pub struct VersionInfo<'a> {
    pub major:           u8,
    pub minor:           u8,
    pub patch:           u16,
    pub host_compiler:   Option<&'a str>,
    pub commit_hash:     Option<&'a str>,
    pub commit_date:     Option<&'a str>,
    pub crate_name:      &'a str,
    pub event_id:        Option<&'a str>,
    pub profile:         Option<&'a str>,
    pub user:            Option<&'a str>,
    pub date:            Option<&'a str>,
    pub dirty_workspace: bool,
}

impl<'a> VersionInfo<'a> {
    /// String in the form `build_profile|build_date|commit_sha` ->
    /// `release|2022-05-23|abcdef123`
    pub fn short(&self) -> String {
        let date_raw = self.date.unwrap_or_default();
        let date = date_raw.trim();

        let profile_raw = self.profile.unwrap_or_default();
        let profile = profile_raw.trim();

        let hash_raw = self.commit_hash.unwrap_or_default();
        let hash = hash_raw.trim();

        [profile, date, hash].join(".")
    }

    pub fn build_id(&self) -> String {
        let event_id_raw = self.event_id.unwrap_or_default();
        let event_id = event_id_raw.trim();

        let date_raw = self.date.unwrap_or_default();
        let date = date_raw.trim();

        let profile_raw = self.profile.unwrap_or_default();
        let profile = profile_raw.trim();

        let user_raw = self.user.unwrap_or_default();
        let user = user_raw.trim();

        [profile, event_id, date, user]
            .iter()
            .filter(|s| !s.is_empty())
            .copied()
            .collect::<Vec<_>>()
            .join(".")
    }
}

impl<'a> VersionInfo<'a> {
    pub fn non_root_user(&self) -> Option<&'a str> {
        self.user
            .map(str::trim)
            .filter(|user| !(*user == "root" || user.is_empty()))
    }
}

impl std::fmt::Display for VersionInfo<'_> {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> std::fmt::Result {
        let bid = self.build_id();

        let hash_raw = self.commit_hash.unwrap_or_default();
        let hash = hash_raw.trim();

        let commit_date_raw = self.commit_date.unwrap_or_default();
        let commit_date = commit_date_raw.trim();

        let git_info = if self.dirty_workspace {
            vec![hash, commit_date, "(dirty)"]
        } else {
            vec![hash, commit_date]
        };

        let git_info = git_info
            .into_iter()
            .filter(|s| !s.is_empty())
            .collect::<Vec<_>>()
            .join(" ");

        write!(
            f,
            "lib: {}{pad}version: v{}.{}.{}{pad}build: {}{pad}source: {}",
            self.crate_name,
            self.major,
            self.minor,
            self.patch,
            bid,
            git_info,
            pad = COLUMN_SEP
        )?;



        Ok(())
    }
}


#[must_use]
pub fn user() -> Option<String> {
    let user = std::env::var("USER").ok().unwrap_or_default();
    let uid = unsafe { getuid() };
    if !(user.is_empty() || uid == 0) {
        return Some(user);
    }

    let srcroot = source_root();
    let build_dir = srcroot + "/build";

    let user = std::process::Command::new("readlink")
        .args(&["-f", build_dir.as_str()])
        .output()
        .ok()
        .and_then(|r| String::from_utf8(r.stdout).ok().map(|s| s.trim().to_string()))
        .filter(|dir| dir.starts_with("/home/") || dir.starts_with("/local/home"))
        .iter()
        .flat_map(|dir| dir.split('/'))
        .filter(|s| !s.is_empty())
        .filter(|s| !["home", "local"].contains(s))
        .map(ToString::to_string)
        .next();

    user
}

#[must_use]
pub fn commit_hash() -> Option<String> {
    std::process::Command::new("git")
        .args(&["rev-parse", "--short", "HEAD"])
        .output()
        .ok()
        .filter(|r| r.status.success())
        .and_then(|r| String::from_utf8(r.stdout).ok())
}


#[must_use]
pub fn git_status() -> Option<String> {
    std::process::Command::new("git")
        .args(&["status", "--porcelain", "--untracked-files=no"])
        .output()
        .ok()
        .filter(|r| r.status.success())
        .and_then(|r| String::from_utf8(r.stdout).ok())
}


#[must_use]
pub fn commit_date() -> Option<String> {
    std::process::Command::new("git")
        .args(&["log", "-1", "--date=short", "--pretty=format:%cd"])
        .output()
        .ok()
        .filter(|r| r.status.success())
        .and_then(|r| String::from_utf8(r.stdout).ok())
}


#[allow(clippy::unnecessary_wraps)]
#[must_use]
pub fn rustc_version() -> Option<String> {
    std::process::Command::new("rustc")
        .arg("-V")
        .output()
        .ok()
        .filter(|r| r.status.success())
        .and_then(|r| String::from_utf8(r.stdout).ok().map(|s| s.trim().to_string()))
}


#[must_use]
pub fn build_date() -> String {
    format!("{}", chrono::Utc::now().format("%Y-%m-%d"))
}


#[must_use]
pub fn build_event_id() -> Option<String> {
    std::env::var("COSMOS_BUILD_EVENT_ID").ok()
}


#[must_use]
pub fn source_root() -> String {
    std::env::var("COSMOS_SRCROOT")
        .or_else(|_| std::env::var("PWD"))
        .ok()
        .unwrap_or_else(|| String::from("."))
}



#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_load_source_info() {
        println!("{:?}", SourceInfo::load());
    }
}
