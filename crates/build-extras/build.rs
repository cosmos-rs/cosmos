#![deny(warnings)]
use std::path::Path;

fn main() {
    let workspace_root = option_env!("COSMOS_WORKSPACE_ROOT")
        .map(Path::new)
        .unwrap_or(Path::new("../../"));
    
    let cargo_toml = workspace_root.join("Cargo.toml");

    println!("cargo:rerun-if-changed={}", cargo_toml.display());
    cosmos_workspace::generate_metadata_file(cargo_toml, std::env::var("OUT_DIR").unwrap());
}
