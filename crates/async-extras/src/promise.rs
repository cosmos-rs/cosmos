//! This is meant to be used to bridge the gap from sync -> async code. Async code can provide
//! promises as a interface to sync code, while hiding any async runtime specific behavior.
use std::future::Future;
use std::pin::Pin;

use crate::deps::futures::task::{
    noop_waker,
    Context,
    Poll,
};

pub trait Promise {
    type Output;

    fn resolve(&mut self) -> <Self as Promise>::Output;
    fn try_resolve(&mut self) -> Option<<Self as Promise>::Output>;
}


pub struct TokioPromise<F: Future> {
    future:    Pin<Box<F>>,
    rt_handle: crate::deps::tokio::runtime::Handle,
}


impl<F: Future> TokioPromise<F> {
    pub fn with_current_runtime(fut: F) -> Option<Self> {
        crate::deps::tokio::runtime::Handle::try_current()
            .ok()
            .map(|handle| Self::with_handle(handle, fut))
    }

    pub fn with_handle(
        rt_handle: crate::deps::tokio::runtime::Handle,
        fut: F,
    ) -> Self {
        Self {
            future: std::boxed::Box::pin(fut),
            rt_handle,
        }
    }

    pub fn into_boxed(self) -> Box<Self> {
        Box::new(self)
    }
}

impl<F: Future> Promise for TokioPromise<F> {
    type Output = <F as Future>::Output;

    fn resolve(&mut self) -> <Self as Promise>::Output {
        const ITER_BEFORE_SLEEP: usize = 5;

        let waker = noop_waker();
        let mut ctx = Context::from_waker(&waker);

        let mut fut = self.future.as_mut();

        let _guard = self.rt_handle.enter();
        let mut i = ::std::num::Wrapping(0usize);
        loop {
            let polled = fut.as_mut().poll(&mut ctx);

            match polled {
                Poll::Ready(output) => {
                    return output;
                }
                Poll::Pending => {
                    // do nothing
                }
            }

            if i.0 < ITER_BEFORE_SLEEP {
                ::core::hint::spin_loop();
            } else {
                ::std::thread::yield_now();
            }
            i += ::std::num::Wrapping(1);
        }
    }

    fn try_resolve(&mut self) -> Option<<F as Future>::Output> {
        let waker = noop_waker();
        let mut ctx = Context::from_waker(&waker);

        let fut = self.future.as_mut();

        let _guard = self.rt_handle.enter();

        let polled = fut.poll(&mut ctx);
        match polled {
            Poll::Ready(output) => return Some(output),
            Poll::Pending => None,
        }
    }
}
