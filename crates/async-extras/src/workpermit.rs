use crate::deps::tokio::sync::{
    OwnedSemaphorePermit,
    Semaphore,
};
use std::marker::PhantomData;
use std::num::NonZeroUsize;
use std::sync::Arc;


/// The holder of a [`WorkPermit`] is authorized to do work specific to that permit kind. The permit
/// is released back to the issuing [`WorkPermitBroker`] when the permit is dropped.
#[derive(Debug)]
pub struct WorkPermit<T>
where
    T: 'static,
{
    #[allow(dead_code)]
    permit: OwnedSemaphorePermit,
    _p:     PhantomData<&'static T>,
}

impl<T> WorkPermit<T>
where
    T: 'static,
{
    #[inline]
    fn new(permit: OwnedSemaphorePermit) -> Self {
        Self {
            permit,
            _p: PhantomData,
        }
    }
}

/// The holder of a [`WorkPermitBroker`] lends out a [`WorkPermit`] if it there are permits
/// available.
///
/// The `T` parameter should be unit structs that represent the unit of work.
///
/// ```rust,no_run
/// struct Download;
///
/// // only allow up to 5 download work tokens to be vended at any given time
/// let mut work_permits = WorkPermitBroker::<Download>::new(5);
/// ```
#[derive(Clone, Debug)]
pub struct WorkPermitBroker<T: 'static> {
    semaphore: Arc<Semaphore>,
    _p:        PhantomData<&'static T>,
}


impl<T> WorkPermitBroker<T> {
    #[inline]
    pub fn new(max_tokens: usize) -> Self {
        let max_tokens = NonZeroUsize::new(max_tokens).map(NonZeroUsize::get).unwrap_or(1);

        Self {
            semaphore: Arc::new(Semaphore::new(max_tokens)),
            _p:        PhantomData,
        }
    }

    pub async fn acquire(&self) -> WorkPermit<T> {
        Semaphore::acquire_owned(self.semaphore.clone())
            .await
            .map(WorkPermit::new)
            .expect(
                "implementation should never fail because the semaphore is \
                owned by the broker and will not be dropped early",
            )
    }
}
