//! A mpsc channel wrapper that never errors, only warns, where the consumer is also an AsyncStream
use crate::deps::tokio::sync::mpsc;
use crate::deps::tokio_stream::Stream;

use crate::deps::tokio_stream::wrappers::UnboundedReceiverStream;
use crate::deps::tracing::warn;
use std::pin::Pin;
use std::task::{
    Context,
    Poll,
};


#[derive(Debug, Clone)]
pub struct Sender<T> {
    tx: mpsc::UnboundedSender<T>,
}


impl<T> Sender<T> {
    pub fn send(
        &self,
        message: T,
    ) {
        self.tx
            .send(message)
            .unwrap_or_else(|_err| warn!("publication channel closed, dropping message"));
    }
}


#[derive(Debug)]
pub struct Receiver<T> {
    rx: UnboundedReceiverStream<T>,
}

impl<T> Stream for Receiver<T> {
    type Item = T;

    fn poll_next(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
    ) -> Poll<Option<Self::Item>> {
        Pin::new(&mut self.rx).poll_next(cx)
    }
}


pub fn streaming_channel<T: Unpin>() -> (Sender<T>, Receiver<T>) {
    let (tx, rx) = mpsc::unbounded_channel::<T>();

    (
        Sender { tx },
        Receiver {
            rx: UnboundedReceiverStream::new(rx),
        },
    )
}
