//! A trait alias for async safe trait objects

pub trait AsyncSafe: 'static + ::std::marker::Send + ::std::marker::Sync {}
impl<T> AsyncSafe for T where T: 'static + ::std::marker::Send + ::std::marker::Sync {}
