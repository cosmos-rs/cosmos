use crate::deps::cosmos_counter::CounterU64;
use crate::deps::derive_more;
use crate::deps::log::{
    error,
    error_span,
    info,
    trace,
};
use crate::deps::once_cell::sync::OnceCell;

use std::borrow::Cow;
use std::future::Future;

#[derive(Debug, Copy, Clone, derive_more::Display, derive_more::From, derive_more::Into)]
struct TaskId(u64);

static TASKID_COUNTER: OnceCell<CounterU64> = OnceCell::new();

impl TaskId {
    fn new() -> Self {
        let id = TASKID_COUNTER.get_or_init(|| CounterU64::with_start(1)).inc();

        TaskId(id)
    }
}

/// create a tracing span from a task that is more readable than the default fmt::Debug derivation
/// ```text
/// // with fmt::Debug
/// Task{id=TaskId(1), name=None}
/// Task{id=TaskId(2), name=Some("BigDeal")}
///
/// // with task_span()
/// task{id=1}
/// task(id=2, name="BigDeal"}
/// ```
fn task_span(task: &Decorator) -> crate::deps::tracing::Span {
    match task.name.as_ref() {
        Some(n) => error_span!("task", id=%task.id, named_task=%n),
        None => error_span!("task", id=%task.id),
    }
}


fn timed<T>(fut: impl Future<Output = T>) -> impl Future<Output = T> {
    use crate::deps::cosmos_chrono::task;
    async move {
        task::timed::with_nanos(fut)
            .await
            .record_elapsed(|nanos| info!(elapsed_time = %nanos))
    }
}


fn timeout<T>(
    fut: impl Future<Output = T>,
    duration: std::time::Duration,
) -> impl Future<Output = Option<T>> {
    use crate::deps::futures::{
        future::select,
        pin_mut,
    };

    async move {
        let timeout_fut = crate::deps::tokio::time::sleep(duration);

        pin_mut!(fut);
        pin_mut!(timeout_fut);

        match select(fut, timeout_fut).await {
            futures::future::Either::Left((result, _)) => Some(result),
            futures::future::Either::Right(_) => None,
        }
    }
}


fn log_start_stop<T>(fut: impl Future<Output = T>) -> impl Future<Output = T> {
    async move {
        trace!("starting...");
        let ret = fut.await;
        trace!("stopping");
        ret
    }
}

/// Same as `run` but also wraps the inner future such that panics will be handled gracefully
/// (caught and unwound). If the future completes successfully then it will return
/// `Some(FInner::Output)` and if it panics it will return `None`.
fn catch_unwind<T>(fut: impl Future<Output = T>) -> impl Future<Output = Option<T>> {
    use crate::deps::futures::future::FutureExt;

    async move {
        let result = std::panic::AssertUnwindSafe(fut).catch_unwind().await;

        let ret = match result {
            Ok(value) => {
                trace!("success!");
                Some(value)
            }
            Err(err) => {
                let message = match err.downcast_ref::<&'static str>() {
                    Some(s) => s,
                    None => {
                        match err.downcast_ref::<String>() {
                            Some(s) => &s,
                            None => "Box<Any>",
                        }
                    }
                };
                error!("failed due to a panic: message={}", message);
                None
            }
        };

        ret
    }
}


/// Provides a set of factory functions for reducing common boiler plate with Futures.
/// All "decorated" futures will inject a tracing `info_span!(task)` into the resulting
/// future that will contain a unique id and optional name for of the future in the
/// tracing context.
pub struct Decorator {
    id:          TaskId,
    name:        Option<Cow<'static, str>>,
    timed:       bool,
    timeout:     Option<std::time::Duration>,
    catch_panic: bool,
}

impl Clone for Decorator {
    fn clone(&self) -> Self {
        Self {
            id:          TaskId::new(),
            name:        self.name.clone(),
            timed:       self.timed,
            timeout:     self.timeout.clone(),
            catch_panic: self.catch_panic,
        }
    }
}

impl Decorator {
    pub fn new() -> Decorator {
        Decorator {
            id:          TaskId::new(),
            name:        None,
            timed:       false,
            timeout:     None,
            catch_panic: false,
        }
    }

    pub fn with_name<S>(name: S) -> Decorator
    where
        S: Into<Cow<'static, str>>,
    {
        let mut task = Decorator::new();
        task.name = Some(name.into());
        task
    }

    pub fn timed(mut self) -> Decorator {
        self.timed = true;
        self
    }

    pub fn with_timeout<T>(
        mut self,
        duration: T,
    ) -> Decorator
    where
        std::time::Duration: From<T>,
    {
        self.timeout = Some(duration.into());
        self
    }

    pub fn catch_unwind(mut self) -> Decorator {
        self.catch_panic = true;
        self
    }

    /// Create a decorated future that will, at minimum, have a task id to identify all
    /// logs generated from a specific future invocation and may be output elapsed
    /// time on exit.
    pub fn wrap<T>(
        self,
        fut: impl Future<Output = T>,
    ) -> impl Future<Output = Option<T>> {
        use crate::deps::tracing_futures::{
            Instrument,
            WithSubscriber,
        };

        let span = task_span(&self);

        // create the wrapper that lets you know when the task has started/done
        let wrapped = async move {
            let is_timed = self.timed;
            let catch_panic = self.catch_panic;
            let fut = async move {
                match (is_timed, catch_panic) {
                    (true, true) => timed(log_start_stop(catch_unwind(fut))).await,
                    (true, false) => {
                        let ret = timed(log_start_stop(fut)).await;
                        Some(ret)
                    }
                    (false, true) => log_start_stop(catch_unwind(fut)).await,
                    (false, false) => {
                        let ret = log_start_stop(fut).await;
                        Some(ret)
                    }
                }
            };

            match self.timeout {
                Some(duration) => {
                    // peel one layer of option
                    timeout(fut, duration).await.unwrap_or(None)
                }
                None => fut.await,
            }
        };

        // generated by any functions within this future will
        // contain the task identifiers.
        wrapped.instrument(span).with_current_subscriber()
    }
}

#[cfg(test)]
mod tests {
    use crate::decorator::Decorator;
    pub use ::cosmos_crash_macros::{
        crash_if_not,
        crash_if_not_equal,
    };
    use ::tokio;

    async fn panic_unconditionally() -> i32 {
        panic!("🔥 this is an expected panic 🔥")
    }

    async fn never_panic() -> i32 {
        42
    }

    #[tokio::test]
    async fn decorator_catch_unwind_with_panic() {
        let result = Decorator::with_name("catch-unwind-test")
            .timed()
            .catch_unwind()
            .wrap(panic_unconditionally())
            .await;

        crash_if_not!(result.is_none(), "the task did not panic or catch a panic");
    }


    #[tokio::test]
    async fn decorator_catch_unwind_no_panic() {
        let result = Decorator::with_name("catch-unwind-test")
            .timed()
            .catch_unwind()
            .wrap(never_panic())
            .await;

        crash_if_not_equal!(
            result,
            Some(42i32),
            "the task should not have panicked and gave back a result of 42"
        );
    }
}
