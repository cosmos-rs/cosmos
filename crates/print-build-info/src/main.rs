//! # cosmos-print-build-info
//!
//! Packages exists to ensure that the cosmos-build-meta crate functions
fn main() {
    println!("{}", cosmos_build_extras::build_version_info!());
}


#[test]
fn build_version_info() {
    let version = cosmos_build_extras::build_version_info!().to_string();
    let parts = version
        .split(cosmos_build_extras::meta::COLUMN_SEP)
        .map(|s| s.to_string())
        .collect::<Vec<_>>();
    match &parts[..] {
        [lib, version, build, source] => {
            assert_eq!(lib, "lib: cosmos-print-build-info");
            assert_eq!(version, "version: v1.0.0");

            let build_parts = build.split(": ").collect::<Vec<_>>();
            assert_eq!(build_parts.len(), 2);
            assert_ne!(build_parts[1].trim(), "");

            let source_parts = source.split(": ").collect::<Vec<_>>();
            assert_eq!(source_parts.len(), 2);
            assert_ne!(build_parts[1].trim(), "");
        }
        x => panic!("version info did not contain the 4 sections: {:?}", x),
    }
}
