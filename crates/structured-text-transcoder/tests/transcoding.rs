use cosmos_proc_macros::cosmos;
use cosmos_structured_text_transcoder::{
    Format,
    TextTranscoder,
};

/// Given the specific input string of a given format, attempt to convert that
/// string against every kind of format including the same format as the input.
fn transcode_to_every_format(
    in_format: Format,
    input_string: &'static str,
) {
    use tracing::{
        debug,
        error,
    };

    let supported_formats_iter = std::iter::IntoIterator::into_iter(Format::all());

    for out_format in supported_formats_iter {
        debug!(r#in=?in_format, out=?out_format, "specific test case");

        let transcoder = TextTranscoder::new(
            in_format,
            Box::new(std::io::Cursor::new(input_string.to_string().into_bytes())),
        );

        let result = transcoder.transcode(out_format);
        match &result {
            Ok(value) => debug!(r#in=?in_format, out=?out_format, value=%format!("\n{}", value), "success"),
            Err(err) => error!(r#in=?in_format, out=?out_format, %err, "failed"),
        }

        assert!(
            matches!(result, Ok(_)),
            "transcoding: {:?} -> {:?} failed",
            in_format,
            out_format
        );
    }
}


#[cosmos(unit_test)]
fn transcode_from_json() {
    transcode_to_every_format(
        Format::Json,
        r#" {"x": {
  "fullscreen": false,
  "mouse_sensitivity": 1.4,
  "window_size": [
    800,
    600
  ],
  "window_title": "PAC-MAN",
    "difficulty_options": {
    "adaptive": false
  }
} }
"#,
    );
}


#[cosmos(unit_test)]
fn transcode_from_toml() {
    transcode_to_every_format(
        Format::Toml,
        r#"
[package]
name = "toml"
version = "0.4.2"
authors = ["Alex Crichton <alex@alexcrichton.com>"]

[dependencies]
serde = "1.0"

[dependencies.potato]
thing = 1
"#,
    );
}


#[cosmos(unit_test)]
fn transcode_from_yaml() {
    transcode_to_every_format(
        Format::Yaml,
        r#"
version: '3'
services:
    php-apache:
        image: php:7.2.1-apache
        ports:
            - 80:80
        volumes:
            - ./DocumentRoot:/var/www/html:z
        links:
            - 'mariadb'

    mariadb:
        image: mariadb:10.1
        volumes:
            - mariadb:/var/lib/mysql
        environment:
            TZ: "Europe/Rome"
            MYSQL_ALLOW_EMPTY_PASSWORD: "no"
            MYSQL_ROOT_PASSWORD: "rootpwd"
            MYSQL_USER: 'testuser'
            MYSQL_PASSWORD: 'testpassword'
            MYSQL_DATABASE: 'testdb'

volumes:
    mariadb
"#,
    );
}

#[cosmos(unit_test)]
fn transcode_from_json5() {
    transcode_to_every_format(
        Format::Json5,
        r#"
        {
          // A traditional message.
          message: 'hello world',

          // A number for some reason.
          n: 42,
    }
"#,
    );
}

#[cfg(feature = "hjson")]
#[cosmos(unit_test)]
fn transcode_from_hjson() {
    transcode_to_every_format(
        Format::Hjson,
        r#"{
  # specify rate in requests/second (because comments are helpful!)
  rate: 1000

  // prefer c-style comments?
  /* feeling old fashioned? */

  # did you notice that rate doesn't need quotes?
  hey: look ma, no quotes for strings either!

  # best of all
  notice: []
  anything: ?

  # yes, commas are optional!
}"#,
    );
}


#[cfg(feature = "ron")]
#[cosmos(unit_test)]
fn transcode_from_ron() {
    transcode_to_every_format(
        Format::Ron,
        r#"
GameConfig( // optional struct name
    window_size: (800, 600),
    window_title: "PAC-MAN",
    fullscreen: false,
    mouse_sensitivity: 1.4,
    key_bindings: {
        "up": Up,
        "down": Down,
        "left": Left,
        "right": Right,

        // Uncomment to enable WASD controls
        /*
        "W": Up,
        "A": Down,
        "S": Left,
        "D": Right,
        */
    },

    difficulty_options: (
        start_difficulty: Easy,
        adaptive: false,
    ),
)

"#,
    );
}
