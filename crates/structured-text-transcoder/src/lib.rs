//! # Cosmos Serde Text Transcoding
//!
//! Convert between different serde supported text formats. The primary use is to provide
//! a way to normalization of and transform between configuration files with different text
//! serialization formats (yaml, json, json5, toml, etc).
//!
//! ```
//! use cosmos_structured_text_transcoder::{
//!     Format,
//!     TextTranscoder,
//!     Value,
//! };
//! use std::io::Cursor;
//!
//! let transcoder = TextTranscoder::new(
//!     Format::Json,
//!     Box::new(Cursor::new(br#"{"a": {"b": 123}}"#.to_vec())),
//! );
//!
//! let value = transcoder
//!     .transcode(Format::Toml)
//!     .expect("could not transcode example")
//!     .to_toml()
//!     .expect("transcoded value was not valid toml");
//!
//! assert_eq!(
//!     value.to_string(),
//!     r#"[a]
//! b = 123
//! "#
//! );
//! ```
#![warn(rustdoc::missing_crate_level_docs)]
#![warn(missing_debug_implementations)]
#![deny(warnings)]
mod deps {
    pub(crate) use ::cosmos_crash_macros;
    pub(crate) use ::cosmos_derivable;
    #[cfg(test)]
    pub(crate) use ::cosmos_proc_macros;

    #[cfg(feature = "hjson")]
    pub(crate) use ::serde_hjson;
    pub(crate) use ::serde_json;
    pub(crate) use ::serde_json5;
    #[cfg(feature = "ron")]
    pub(crate) use ::serde_ron;
    pub(crate) use ::serde_toml;
    pub(crate) use ::serde_transcode;
    pub(crate) use ::serde_yaml;

    pub(crate) use ::thiserror;
}


mod error;
mod format;
mod imp;
mod transcoder;

pub mod backend {
    #[cfg(feature = "hjson")]
    pub use ::serde_hjson as hjson;
    pub use ::serde_json as json;
    pub use ::serde_json5 as json5;
    #[cfg(feature = "ron")]
    pub use ::serde_ron as ron;
    pub use ::serde_toml as toml;
    pub use ::serde_yaml as yaml;
}


pub use crate::deps::serde_json::Value as JsonValue;
pub use crate::error::Error;
pub use crate::format::Format;
pub use crate::imp::Value;
pub use crate::transcoder::TextTranscoder;
