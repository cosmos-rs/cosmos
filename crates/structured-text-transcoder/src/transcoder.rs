use crate::deps::cosmos_crash_macros::crash_on_none;
use crate::{
    imp,
    Error,
    Format,
};
use ::core::fmt;
use ::std::path::Path;
use ::std::str::FromStr;
use std::ffi::OsStr;

type BufferedFileReader = std::io::BufReader<std::fs::File>;


pub struct TextTranscoder {
    input:  Format,
    reader: Box<dyn std::io::Read>,
}


impl TextTranscoder {
    pub fn new(
        input: Format,
        reader: Box<dyn std::io::Read>,
    ) -> Self {
        Self { input, reader }
    }

    pub fn from_path<P>(path: P) -> Result<Self, Error>
    where
        P: AsRef<OsStr>,
    {
        let path = Path::new(&path);
        let path_ext = path.extension();
        let ext_string = path_ext.and_then(|p| p.to_str()).unwrap_or("");
        Ok(Self {
            input:  Format::from_str(ext_string)?,
            reader: Box::new(open_file(path)?),
        })
    }

    pub fn input_format(&self) -> Format {
        self.input
    }

    pub fn transcode(
        self,
        output: Format,
    ) -> Result<imp::Value, Error> {
        let Self { input, reader } = self;

        match output {
            Format::Json => imp::json(input, reader),
            Format::Json5 => imp::json5(input, reader),
            Format::Yaml => imp::yaml(input, reader),
            Format::Toml => imp::toml(input, reader),
            #[cfg(feature = "hjson")]
            Format::Hjson => imp::hjson(input, reader),
            #[cfg(feature = "ron")]
            Format::Ron => imp::ron(input, reader),
        }
    }

    pub fn transcode_json(self) -> Result<serde_json::Value, Error> {
        Ok(self
            .transcode(Format::Json)?
            .to_json()
            .unwrap_or_else(crash_on_none!("impossible")))
    }

    pub fn transcode_json5(self) -> Result<serde_json::Value, Error> {
        Ok(self
            .transcode(Format::Json5)?
            .to_json5()
            .unwrap_or_else(crash_on_none!("impossible")))
    }

    pub fn transcode_yaml(self) -> Result<serde_yaml::Value, Error> {
        Ok(self
            .transcode(Format::Yaml)?
            .to_yaml()
            .unwrap_or_else(crash_on_none!("impossible")))
    }

    pub fn transcode_toml(self) -> Result<serde_toml::Value, Error> {
        Ok(self
            .transcode(Format::Toml)?
            .to_toml()
            .unwrap_or_else(crash_on_none!("impossible")))
    }

    #[cfg(feature = "ron")]
    pub fn transcode_ron(self) -> Result<serde_ron::Value, Error> {
        Ok(self
            .transcode(Format::Ron)?
            .to_ron()
            .unwrap_or_else(crash_on_none!("impossible")))
    }

    #[cfg(feature = "hjson")]
    pub fn transcode_hjson(self) -> Result<serde_hjson::Value, Error> {
        Ok(self
            .transcode(Format::Hjson)?
            .to_hjson()
            .unwrap_or_else(crash_on_none!("impossible")))
    }
}


impl fmt::Debug for TextTranscoder {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        f.debug_struct("JsonTranscoder")
            .field("input", &self.input)
            .finish()
    }
}



fn open_file<P>(path: P) -> Result<BufferedFileReader, Error>
where
    P: AsRef<Path>,
{
    Ok(std::io::BufReader::new(
        std::fs::OpenOptions::new()
            .append(false)
            .write(false)
            .read(true)
            .open(path)?,
    ))
}
