use crate::deps::cosmos_derivable::derive_enum;
use crate::Error;
use ::core::fmt;
use std::str::FromStr;

type FormatValues = [Format; Format::variant_count()];

/// Represents one of
#[derive(Copy, Clone, Debug, PartialEq, Eq, derive_enum::VariantCount)]
pub enum Format {
    Json,
    Json5,
    Yaml,
    Toml,
    #[cfg(feature = "hjson")]
    Hjson,
    #[cfg(feature = "ron")]
    Ron,
}

impl Format {
    const ALLOWED_EXTENSIONS: &'static [(&'static str, Self)] = &[
        ("json", Self::Json),
        ("json5", Self::Json5),
        ("yaml", Self::Yaml),
        ("yml", Self::Yaml),
        ("toml", Self::Toml),
        #[cfg(feature = "hjson")]
        ("hjson", Self::Hjson),
        #[cfg(feature = "ron")]
        ("ron", Self::Ron),
    ];

    pub const fn all() -> FormatValues {
        [
            Self::Json,
            Self::Json5,
            Self::Yaml,
            Self::Toml,
            #[cfg(feature = "hjson")]
            Self::Hjson,
            #[cfg(feature = "ron")]
            Self::Ron,
        ]
    }

    pub fn from_extension<S>(ext: S) -> Option<Self>
    where
        S: AsRef<str>,
    {
        let s = ext.as_ref();
        let extension = s.trim();
        Self::ALLOWED_EXTENSIONS
            .iter()
            .find(|(ext, _kind)| ext.eq_ignore_ascii_case(extension))
            .map(|(_, kind)| *kind)
    }

    pub const fn as_str(&self) -> &str {
        match self {
            Self::Json => "json",
            Self::Json5 => "json5",
            Self::Yaml => "yaml",
            Self::Toml => "toml",
            #[cfg(feature = "hjson")]
            Self::Hjson => "hjson",
            #[cfg(feature = "ron")]
            Self::Ron => "ron",
        }
    }
}

impl FromStr for Format {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::from_extension(s).ok_or_else(|| {
            Error::BadFileExtension {
                extension: s.to_string().into(),
                allowed:   Self::ALLOWED_EXTENSIONS.iter().map(|(ext, _)| *ext).collect(),
            }
        })
    }
}


impl fmt::Display for Format {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        f.write_str(self.as_str())
    }
}


#[cfg(test)]
mod tests {
    use super::Format;
    use crate::deps::cosmos_proc_macros::cosmos;

    #[cosmos(unit_test)]
    fn detect_format_from_extension_string() {
        let mut cases = vec![];

        cases.extend(vec![
            ("json", Some(Format::Json)),
            ("JSON", Some(Format::Json)),
            ("JsOn", Some(Format::Json)),
            ("jsn", None),
            ("json5", Some(Format::Json5)),
            ("JSON5", Some(Format::Json5)),
            ("Json5", Some(Format::Json5)),
            ("js0n5", None),
            ("yaml", Some(Format::Yaml)),
            ("YAML", Some(Format::Yaml)),
            ("Yaml", Some(Format::Yaml)),
            ("yml", Some(Format::Yaml)),
            ("YML", Some(Format::Yaml)),
            ("Yml", Some(Format::Yaml)),
            ("ym", None),
            ("yam", None),
            ("toml", Some(Format::Toml)),
            ("TOML", Some(Format::Toml)),
            ("Toml", Some(Format::Toml)),
            ("tom", None),
        ]);

        #[cfg(feature = "hjson")]
        cases.extend(vec![
            ("hjson", Some(Format::Hjson)),
            ("HJSON", Some(Format::Hjson)),
            ("hJsOn", Some(Format::Hjson)),
            ("h-json", None),
        ]);

        #[cfg(feature = "ron")]
        cases.extend(vec![
            ("ron", Some(Format::Ron)),
            ("RON", Some(Format::Ron)),
            ("ron", Some(Format::Ron)),
            ("rony", None),
            ("rn", None),
        ]);

        for (ext, expected) in cases.into_iter() {
            let actual = Format::from_extension(ext);
            if actual == expected {
                info!(extension=?ext, ?expected, ?actual);
            } else {
                error!(extension=?ext, ?expected, ?actual, "expected and actual do not match");
            }
            assert_eq!(
                expected, actual,
                "expected and actual do not match for file extension: {:?}",
                ext
            );
        }
    }
}
