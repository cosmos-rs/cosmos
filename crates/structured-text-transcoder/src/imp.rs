//! Specific format conversion implementations

use crate::deps::cosmos_crash_macros::crash_on_err;
use crate::deps::{
    serde_json,
    serde_json5,
    serde_toml,
    serde_transcode,
    serde_yaml,
};

use crate::deps::cosmos_derivable::derive_enum;

#[cfg(feature = "hjson")]
use crate::deps::serde_hjson;

#[cfg(feature = "ron")]
use crate::deps::serde_ron;

use ::core::fmt;

use crate::{
    Error,
    Format,
};


pub fn json(
    input: Format,
    mut reader: Box<dyn std::io::Read>,
) -> Result<Value, Error> {
    let json_string = match input {
        Format::Json => {
            let json_string = read_to_string(reader)?;
            let value = serde_json::from_str::<'_, serde_json::Value>(&json_string)?;
            serde_json::to_string_pretty(&value)?
        }
        Format::Json5 => {
            let json5_string = read_to_string(reader)?;
            let value = serde_json5::from_str::<'_, serde_json::Value>(&json5_string)?;
            serde_json::to_string_pretty(&value)?
        }
        Format::Yaml => {
            let mut writer = std::io::Cursor::new(Vec::<u8>::new());
            let mut ser = serde_json::Serializer::pretty(&mut writer);
            let de = serde_yaml::Deserializer::from_reader(&mut reader);
            serde_transcode::transcode(de, &mut ser)?;
            String::from_utf8(writer.into_inner()).unwrap_or_else(crash_on_err!())
        }
        Format::Toml => {
            let mut writer = std::io::Cursor::new(Vec::<u8>::new());
            let mut ser = serde_json::Serializer::pretty(&mut writer);
            let string = read_to_string(reader)?;
            let mut de = serde_toml::de::Deserializer::new(&string);
            serde_transcode::transcode(&mut de, &mut ser)?;
            String::from_utf8(writer.into_inner()).unwrap_or_else(crash_on_err!())
        }
        #[cfg(feature = "hjson")]
        Format::Hjson => {
            serde_json::to_string_pretty(&serde_hjson::from_reader::<_, serde_hjson::Value>(&mut reader)?)?
        }
        #[cfg(feature = "ron")]
        Format::Ron => {
            let mut writer = std::io::Cursor::new(Vec::<u8>::new());
            let mut ser = serde_json::Serializer::pretty(&mut writer);
            let string = read_to_string(reader)?;
            let mut de = serde_ron::de::Deserializer::from_str(&string)?;
            serde_transcode::transcode(&mut de, &mut ser)?;
            String::from_utf8(writer.into_inner()).unwrap_or_else(crash_on_err!())
        }
    };

    Ok(Value {
        typed_text: TypedText::Json(json_string),
    })
}


pub fn json5(
    input: Format,
    reader: Box<dyn std::io::Read>,
) -> Result<Value, Error> {
    match input {
        Format::Json5 | Format::Json => {
            let json5_string = read_to_string(reader)?;
            let value = serde_json5::from_str::<'_, serde_json::Value>(&json5_string)?;
            Ok(Value {
                typed_text: TypedText::Json5(serde_json::to_string_pretty(&value)?),
            })
        }
        Format::Yaml | Format::Toml => json(input, reader),
        #[cfg(feature = "hjson")]
        Format::Hjson => json(input, reader),
        #[cfg(feature = "ron")]
        Format::Ron => json(input, reader),
    }
}


pub fn toml(
    input: Format,
    reader: Box<dyn std::io::Read>,
) -> Result<Value, Error> {
    let toml_string = match input {
        Format::Json => {
            let mut writer = String::new();
            let mut ser = serde_toml::Serializer::pretty(&mut writer);
            let string = read_to_string(reader)?;
            let mut de = serde_json::Deserializer::from_str(&string);
            serde_transcode::transcode(&mut de, &mut ser)?;
            writer
        }
        Format::Json5 => {
            serde_toml::to_string_pretty(&serde_json5::from_str::<'_, serde_json::Value>(&read_to_string(
                reader,
            )?)?)?
        }
        Format::Yaml => {
            let value = serde_yaml::from_str::<serde_toml::Value>(&read_to_string(reader)?)?;
            serde_toml::to_string_pretty(&value)?
        }
        Format::Toml => read_to_string(reader)?,
        #[cfg(feature = "hjson")]
        Format::Hjson => {
            serde_toml::to_string_pretty(&serde_hjson::from_reader::<_, serde_hjson::Value>(reader)?)?
        }
        #[cfg(feature = "ron")]
        Format::Ron => {
            let mut writer = String::new();
            let mut ser = serde_toml::Serializer::pretty(&mut writer);
            let string = read_to_string(reader)?;
            let mut de = serde_ron::Deserializer::from_str(&string)?;
            serde_transcode::transcode(&mut de, &mut ser)?;
            writer
        }
    };

    Ok(Value {
        typed_text: TypedText::Toml(toml_string),
    })
}

#[cfg(feature = "ron")]
pub fn ron(
    input: Format,
    reader: Box<dyn std::io::Read>,
) -> Result<Value, Error> {
    let config = Some(
        serde_ron::ser::PrettyConfig::default()
            .with_decimal_floats(true)
            .with_enumerate_arrays(true),
    );

    let ron_string = match input {
        Format::Ron => read_to_string(reader)?,
        Format::Yaml => {
            let mut writer = std::io::Cursor::new(Vec::<u8>::new());
            let mut ser = serde_ron::Serializer::new(&mut writer, config, true)?;
            let de = serde_yaml::Deserializer::from_reader(reader);
            serde_transcode::transcode(de, &mut ser)?;
            String::from_utf8(writer.into_inner()).unwrap_or_else(crash_on_err!())
        }
        Format::Json5 => {
            let value: serde_json::Value = serde_json5::from_str(&read_to_string(reader)?)?;
            serde_ron::to_string(&value)?
        }
        Format::Json => {
            let mut writer = std::io::Cursor::new(Vec::<u8>::new());
            let mut ser = serde_ron::Serializer::new(&mut writer, config, true)?;
            let mut de = serde_json::Deserializer::from_reader(reader);
            serde_transcode::transcode(&mut de, &mut ser)?;
            String::from_utf8(writer.into_inner()).unwrap_or_else(crash_on_err!())
        }
        #[cfg(feature = "hjson")]
        Format::Hjson => {
            let value = serde_hjson::from_reader::<_, serde_hjson::Value>(&mut reader)?;
            serde_ron::to_string(&value)?
        }
        Format::Toml => {
            let mut writer = std::io::Cursor::new(Vec::<u8>::new());
            let mut ser = serde_ron::Serializer::new(&mut writer, config, true)?;
            let string = read_to_string(reader)?;
            let mut de = serde_toml::de::Deserializer::new(&string);
            serde_transcode::transcode(&mut de, &mut ser)?;
            String::from_utf8(writer.into_inner()).unwrap_or_else(crash_on_err!())
        }
    };

    Ok(Value {
        typed_text: TypedText::Ron(ron_string),
    })
}


#[cfg(feature = "hjson")]
pub fn hjson(
    input: Format,
    reader: Box<dyn std::io::Read>,
) -> Result<Value, Error> {
    let hjson_string = match input {
        Format::Hjson => {
            let value = serde_hjson::from_reader::<_, serde_hjson::Value>(reader)?;
            serde_hjson::to_string(&value)?
        }
        Format::Yaml => {
            let yaml_value = serde_yaml::from_reader::<_, serde_yaml::Value>(reader)?;
            serde_hjson::to_string(&yaml_value)?
        }
        Format::Json5 => {
            let json5_value = serde_json5::from_str::<serde_json::Value>(&read_to_string(reader)?)?;
            serde_hjson::to_string(&json5_value)?
        }
        Format::Json => {
            let json_value = serde_json::from_reader::<_, serde_json::Value>(reader)?;
            serde_hjson::to_string(&json_value)?
        }
        Format::Toml => {
            let string = read_to_string(reader)?;
            let toml_value = serde_toml::from_str::<serde_toml::Value>(&string)?;
            serde_hjson::to_string(&toml_value)?
        }
        #[cfg(feature = "ron")]
        Format::Ron => {
            let ron_value = serde_ron::from_str::<serde_ron::Value>(&read_to_string(reader)?)?;
            serde_hjson::to_string(&ron_value)?
        }
    };

    Ok(Value {
        typed_text: TypedText::Hjson(hjson_string),
    })
}


pub fn yaml(
    input: Format,
    reader: Box<dyn std::io::Read>,
) -> Result<Value, Error> {
    let yaml_string = match input {
        Format::Yaml => {
            let value: serde_yaml::Value = serde_yaml::from_reader(reader)?;
            serde_yaml::to_string(&value)?
        }
        Format::Json5 => {
            let value: serde_json::Value = serde_json5::from_str(&read_to_string(reader)?)?;
            serde_yaml::to_string(&value)?
        }
        Format::Json => {
            let value: serde_json::Value = serde_json::from_reader(reader)?;
            serde_yaml::to_string(&value)?
        }
        Format::Toml => {
            let mut writer = std::io::Cursor::new(Vec::<u8>::new());
            let mut ser = serde_yaml::Serializer::new(&mut writer);
            let string = read_to_string(reader)?;
            let mut de = serde_toml::de::Deserializer::new(&string);
            serde_transcode::transcode(&mut de, &mut ser)?;
            String::from_utf8(writer.into_inner()).unwrap_or_else(crash_on_err!())
        }
        #[cfg(feature = "hjson")]
        Format::Hjson => {
            let value = serde_hjson::from_reader::<_, serde_hjson::Value>(reader)?;
            serde_yaml::to_string(&value)?
        }
        #[cfg(feature = "ron")]
        Format::Ron => {
            let mut writer = std::io::Cursor::new(Vec::<u8>::new());
            let mut ser = serde_yaml::Serializer::new(&mut writer);
            let string = read_to_string(reader)?;
            let mut de = serde_ron::de::Deserializer::from_str(&string)?;
            serde_transcode::transcode(&mut de, &mut ser)?;
            String::from_utf8(writer.into_inner()).unwrap_or_else(crash_on_err!())
        }
    };

    Ok(Value {
        typed_text: TypedText::Yaml(yaml_string),
    })
}


/// [`Value`] can be one of any of the transcoding formats as the format specific strongly typed
/// `Value`.
#[derive(Clone)]
pub struct Value {
    // hide the enum of possible Value types since variants are pub
    // and it is an implementation detail
    typed_text: TypedText,
}

impl Value {
    pub fn format(&self) -> Format {
        match &self.typed_text {
            TypedText::Yaml(_) => Format::Yaml,
            TypedText::Json(_) => Format::Json,
            TypedText::Json5(_) => Format::Json5,
            TypedText::Toml(_) => Format::Toml,

            #[cfg(feature = "hjson")]
            TypedText::Hjson(_) => Format::Hjson,

            #[cfg(feature = "ron")]
            TypedText::Ron(_) => Format::Ron,
        }
    }

    pub fn as_str(&self) -> &str {
        match &self.typed_text {
            TypedText::Yaml(s) | TypedText::Json(s) | TypedText::Json5(s) | TypedText::Toml(s) => s.as_str(),

            #[cfg(feature = "hjson")]
            TypedText::Hjson(s) => s.as_str(),

            #[cfg(feature = "ron")]
            TypedText::Ron(s) => s.as_str(),
        }
    }

    pub fn to_yaml(&self) -> Option<serde_yaml::Value> {
        match &self.typed_text {
            TypedText::Yaml(value) => {
                Some(
                    serde_yaml::from_str(value.as_str())
                        .unwrap_or_else(crash_on_err!("yaml string was not deserializable")),
                )
            }
            _ => None,
        }
    }

    pub fn to_json(&self) -> Option<serde_json::Value> {
        match &self.typed_text {
            TypedText::Json5(value) | TypedText::Json(value) => {
                Some(
                    serde_json::from_str(value.as_str())
                        .unwrap_or_else(crash_on_err!("json string was not deserializable")),
                )
            }
            _ => None,
        }
    }

    pub fn to_json5(&self) -> Option<serde_json::Value> {
        self.to_json()
    }

    pub fn to_toml(&self) -> Option<serde_toml::Value> {
        match &self.typed_text {
            TypedText::Toml(value) => {
                Some(
                    serde_toml::from_str(value.as_str())
                        .unwrap_or_else(crash_on_err!("toml string was not deserializable")),
                )
            }
            _ => None,
        }
    }

    #[cfg(feature = "hjson")]
    pub fn to_hjson(&self) -> Option<serde_hjson::Value> {
        match &self.typed_text {
            TypedText::Hjson(value) => {
                Some(
                    serde_hjson::from_str(value.as_str())
                        .unwrap_or_else(crash_on_err!("hjson string was not deserializable")),
                )
            }
            _ => None,
        }
    }

    #[cfg(feature = "ron")]
    pub fn to_ron(&self) -> Option<serde_ron::Value> {
        match &self.typed_text {
            TypedText::Ron(value) => {
                Some(
                    serde_ron::from_str(value.as_str())
                        .unwrap_or_else(crash_on_err!("ron string was not deserializable")),
                )
            }
            _ => None,
        }
    }
}


impl fmt::Debug for Value {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        f.debug_tuple("Value").field(&self.typed_text).finish()
    }
}

impl fmt::Display for Value {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        self.typed_text.fmt(f)
    }
}



#[derive(Clone, Debug, derive_enum::VariantCount)]
enum TypedText {
    Yaml(String),
    Json(String),
    Json5(String),
    Toml(String),
    #[cfg(feature = "hjson")]
    Hjson(String),
    #[cfg(feature = "ron")]
    Ron(String),
}


impl fmt::Display for TypedText {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        match self {
            TypedText::Yaml(v) | TypedText::Json(v) | TypedText::Json5(v) | TypedText::Toml(v) => v.fmt(f),
            #[cfg(feature = "hjson")]
            TypedText::Hjson(v) => v.fmt(f),

            #[cfg(feature = "ron")]
            TypedText::Ron(v) => v.fmt(f),
        }
    }
}


fn read_to_string<R: std::io::Read>(mut reader: R) -> Result<String, Error> {
    let mut string = String::new();
    reader.read_to_string(&mut string)?;
    Ok(string)
}



#[test]
fn value_inner_handles_all_known_format_variants() {
    assert_eq!(
        TypedText::variant_count(),
        Format::variant_count(),
        "known formats vs handled formats variant mismatch"
    )
}
