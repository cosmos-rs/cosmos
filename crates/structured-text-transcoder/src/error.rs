#[cfg(feature = "hjson")]
use crate::deps::serde_hjson;
#[cfg(feature = "ron")]
use crate::deps::serde_ron;

use crate::deps::{
    serde_json,
    serde_json5,
    serde_toml,
    serde_yaml,
    thiserror,
};
use std::borrow::Cow;

/// One of the possible errors that can occur during text transcoding
#[derive(Debug, thiserror::Error)]
#[allow(missing_docs)]
pub enum Error {
    /// generic system io error such as unable to read from from a file
    #[error("io error - {err}")]
    Io {
        #[from]
        err: std::io::Error,
    },
    #[error("unknown file extension error - could not determine the configuration file input format from unknown extension: extension={extension:?}; allowed={allowed:?}")]
    BadFileExtension {
        extension: Cow<'static, str>,
        allowed:   Vec<&'static str>,
    },
    #[error("json format error: {err}")]
    SerdeJson {
        #[from]
        err: serde_json::Error,
    },

    #[error("json5 format error: {err}")]
    SerdeJson5 {
        #[from]
        err: serde_json5::Error,
    },

    #[error("yaml format error: {err}")]
    SerdeYaml {
        #[from]
        err: serde_yaml::Error,
    },
    #[error("toml format error: {err}")]
    SerdeToml {
        #[from]
        err: serde_toml::de::Error,
    },

    #[error("toml format error: {err}")]
    SerdeTomlSer {
        #[from]
        err: serde_toml::ser::Error,
    },
    #[cfg(feature = "hjson")]
    #[error("hjson format error: {err}")]
    SerdeHjson {
        #[from]
        err: serde_hjson::Error,
    },
    #[cfg(feature = "ron")]
    #[error("ron format error: {err}")]
    SerdeRon {
        #[from]
        err: serde_ron::Error,
    },
}
