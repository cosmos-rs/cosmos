//! Functions to time the execution of a scope. For the async implementations of these functions see
//! [`crate::task`].
//!
//! ```
//! use cosmos_chrono::timed;
//! use std::num::Wrapping;
//!
//! let time = timed::with_nanos(|| {
//!     let mut value = Wrapping(1);
//!     for x in 1..1000 {
//!         value *= Wrapping(x);
//!     }
//! });
//!
//! println!("scope took {}", time.elapsed);
//! ```
use std::fmt;

use crate::{
    Micros,
    Millis,
    Nanos,
};
use crate::{
    Stopwatch,
    Watch,
};

/// Measure the execution time of the scope defined by the closure `f` and return a
/// [`Timed<std::time::Duration, R>`]
#[inline(always)]
pub fn with_duration<F, R>(f: F) -> Timed<std::time::Duration, R>
where
    F: (FnOnce() -> R),
{
    let stopwatch = Stopwatch::<std::time::Instant>::start();
    let value = f();
    let elapsed = stopwatch.into_elapsed();
    Timed { elapsed, value }
}

/// Measure the execution time of the scope defined by the closure `f` and return a
/// [`Timed<Nanos, R>`]
#[inline(always)]
pub fn with_nanos<F, R>(f: F) -> Timed<Nanos, R>
where
    F: (FnOnce() -> R),
{
    let stopwatch = Stopwatch::<Nanos>::start();
    let value = f();
    let elapsed = stopwatch.into_elapsed();
    Timed { elapsed, value }
}

/// Measure the execution time of the scope defined by the closure `f` and return a
/// [`Timed<Micros, R>`]
#[inline(always)]
pub fn with_micros<F, R>(f: F) -> Timed<Micros, R>
where
    F: (FnOnce() -> R),
{
    with_nanos(f).into_micros()
}

/// Measure the execution time of the scope defined by the closure `f` and return a
/// [`Timed<Millis, R>`]
#[inline(always)]
pub fn with_millis<F, R>(f: F) -> Timed<Millis, R>
where
    F: (FnOnce() -> R),
{
    with_micros(f).into_millis()
}


/// The combined elapsed_time and returnvalue of a timed scope.
pub struct Timed<T, V> {
    /// The amount of time that elapsed during the action that was timed
    pub elapsed: T,
    /// The value produced by the action
    pub value:   V,
}


impl<T, V> Timed<T, V> {
    /// Consume the time quanta of the [`Timed<T, V>`] with the given closure and
    /// return the original value of the timed scope.
    /// ```
    /// use cosmos_chrono::timed;
    ///
    /// let value = timed::with_nanos(|| {
    ///     std::thread::sleep(::std::time::Duration::from_micros(1));
    ///     Some("yup this works")
    /// })
    /// .into_micros()
    /// .record_elapsed(|micros| {
    ///     println!("scope took {}", micros);
    /// });
    ///
    /// assert_eq!(Some("yup this works"), value);
    /// ```
    #[inline]
    pub fn record_elapsed<F>(
        self,
        recorder: F,
    ) -> V
    where
        F: (FnOnce(T)),
    {
        let Timed { elapsed, value } = self;
        recorder(elapsed);
        value
    }
}


impl<T, V, E> Timed<T, std::result::Result<V, E>> {
    ///  Convert a [`Timed<T, std::result::Result<V, E>>`] into [`Result<(T, V), E>`]
    /// by pushing the time quanta into the result's success value.
    #[inline]
    pub fn into_result(self) -> std::result::Result<(T, V), E> {
        let Timed { elapsed, value } = self;
        value.map(|value| (elapsed, value))
    }
}

impl<V> Timed<Nanos, V> {
    /// convenience function to get the nanos as micros
    pub fn into_micros(self) -> Timed<Micros, V> {
        Timed {
            elapsed: self.elapsed.into_micros(),
            value:   self.value,
        }
    }
}


impl<V> Timed<Micros, V> {
    /// convenience function to get the micros as millis
    pub fn into_millis(self) -> Timed<Millis, V> {
        Timed {
            elapsed: self.elapsed.into_millis(),
            value:   self.value,
        }
    }
}

impl<T, V> fmt::Debug for Timed<T, V>
where
    T: fmt::Debug,
    V: fmt::Debug,
{
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        f.debug_struct("Timed")
            .field("elapsed", &self.elapsed)
            .field("value", &self.value)
            .finish()
    }
}
