pub mod timed {
    use crate::timed::Timed;
    use crate::{
        Micros,
        Millis,
        Nanos,
    };
    use crate::{
        Stopwatch,
        Watch,
    };
    use std::future::Future;

    /// Wrap a future to time the execution time of the future and return a [`Timed<T>`] that
    /// includes the execution time as a [`std::time::Duration`] along with the original
    /// value of the future.
    #[inline]
    pub fn with_duration<T>(
        f: impl Future<Output = T>
    ) -> impl Future<Output = Timed<std::time::Duration, T>> {
        async move {
            let stopwatch = Stopwatch::<std::time::Instant>::start();
            let value = f.await;
            let elapsed = stopwatch.into_elapsed();
            Timed { elapsed, value }
        }
    }

    /// Wrap a future to time the execution time of the future and return a [`Timed<T>`] that
    /// includes the execution time as a [`Nanos`] along with the original
    /// value of the future.
    #[inline]
    pub fn with_nanos<T>(f: impl Future<Output = T>) -> impl Future<Output = Timed<Nanos, T>> {
        async move {
            let stopwatch = Stopwatch::<Nanos>::start();
            let value = f.await;
            let elapsed = stopwatch.into_elapsed();
            Timed { elapsed, value }
        }
    }

    /// Wrap a future to time the execution time of the future and return a [`Timed<T>`] that
    /// includes the execution time as a [`Micros`] along with the original
    /// value of the future.
    #[inline]
    pub fn with_micros<T>(f: impl Future<Output = T>) -> impl Future<Output = Timed<Micros, T>> {
        async move { with_nanos(f).await.into_micros() }
    }


    /// Wrap a future to time the execution time of the future and return a [`Timed<T>`] that
    /// includes the execution time as a [`Millis`] along with the original
    /// value of the future.
    #[inline]
    pub fn with_millis<T>(f: impl Future<Output = T>) -> impl Future<Output = Timed<Millis, T>> {
        async move { with_nanos(f).await.into_micros().into_millis() }
    }
}
