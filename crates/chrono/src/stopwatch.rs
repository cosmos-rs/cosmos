use crate::deps::cosmos_crash_macros::crash_on_none;
use crate::Nanos;
use std::hint::unreachable_unchecked;


/// A type that can measure elapsed time like a stopwatch.
pub trait Watch {
    /// The time quanta
    type Time;

    /// The amount of time that has elapsed since the Watch was created
    fn elapsed(&self) -> Self::Time;

    /// Reset the elpased time to 0
    fn reset(&mut self) -> Self::Time;

    /// Consume the Watch and return the elapsed time in terms of `Self::Time` units
    fn into_elapsed(self) -> Self::Time
    where
        Self: Sized,
    {
        self.elapsed()
    }
}


/// The default implementation of `Watch`.
#[repr(transparent)]
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Stopwatch<T> {
    start: T,
}


impl Stopwatch<Nanos> {
    /// Initialize and start Stopwatch that will measure the elapsed time in [`Nanos`]
    pub fn start() -> Self {
        Self { start: Nanos::now() }
    }

    pub fn start_timestamp(&self) -> Nanos {
        self.start.clone()
    }
}

impl Stopwatch<std::time::Instant> {
    /// Initialize and start Stopwatch that will measure the elapsed time in [`Nanos`]
    pub fn start() -> Self {
        Self {
            start: std::time::Instant::now(),
        }
    }
}


impl Watch for Stopwatch<Nanos> {
    type Time = Nanos;

    fn elapsed(&self) -> Self::Time {
        Nanos::between_now_and(&self.start).unwrap_or_else(crash_on_none!())
    }

    fn reset(&mut self) -> Self::Time {
        let now = Nanos::now();
        let elapsed_time = match now.since(&self.start) {
            Some(elapsed_time) => elapsed_time,
            None => unsafe { unreachable_unchecked() },
        };

        self.start = now;
        elapsed_time
    }
}



impl Watch for Stopwatch<std::time::Instant> {
    type Time = std::time::Duration;

    fn elapsed(&self) -> Self::Time {
        std::time::Instant::now().duration_since(self.start)
    }

    fn reset(&mut self) -> Self::Time {
        let now = std::time::Instant::now();
        let elapsed_time = now.duration_since(self.start);
        self.start = now;
        elapsed_time
    }
}
