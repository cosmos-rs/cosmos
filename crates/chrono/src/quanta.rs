#![allow(missing_docs)]
use std::time::Duration;

use crate::deps::cosmos_crash_macros::{
    crash_on_err,
    crash_on_none,
};
use crate::deps::derive_more;
use crate::deps::num_derive;
use crate::deps::num_traits::{
    CheckedMul,
    CheckedSub,
};
use crate::deps::serde;

/// Epoch timestamp in Seconds since the unix epoch
#[repr(transparent)]
#[derive(
    Copy,
    Clone,
    Eq,
    PartialEq,
    Debug,
    Default,
    Hash,
    Ord,
    PartialOrd,
    num_derive::ToPrimitive,
    num_derive::FromPrimitive,
    num_derive::NumOps,
    num_derive::NumCast,
    derive_more::From,
    derive_more::Into,
    serde::Serialize,
    serde::Deserialize,
)]
pub struct Seconds(#[serde(with = "crate::deps::cosmos_units::seconds::serde_proxy::int_no_prefix")] u64);

impl Seconds {
    /// Create a `Seconds` from a u64
    #[inline(always)]
    pub const fn new(n: u64) -> Self {
        Self(n)
    }

    /// return Seconds(1)
    #[inline(always)]
    pub const fn one() -> Self {
        Self(1)
    }

    /// `const fn` version
    pub const fn into_u64(self) -> u64 {
        self.0
    }

    /// `const fn` way to convert the `Seconds` into a [`std::time::Duration`]
    pub const fn into_duration(self) -> std::time::Duration {
        std::time::Duration::from_secs(self.0)
    }
}


impl std::fmt::Display for Seconds {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter,
    ) -> std::fmt::Result {
        write!(f, "{}s", self.0)
    }
}

impl CheckedSub for Seconds {
    #[inline(always)]
    fn checked_sub(
        &self,
        v: &Self,
    ) -> Option<Self> {
        let minuend = self.into_u64();
        let subtrahend = v.into_u64();
        minuend.checked_sub(subtrahend).map(Self::new)
    }
}

impl std::convert::From<Seconds> for std::time::Duration {
    fn from(sec: Seconds) -> Duration {
        Duration::from_secs(sec.into_u64())
    }
}


/// Epoch timestamp in milliseconds since the unix epoch
#[repr(transparent)]
#[derive(
    Copy,
    Clone,
    Eq,
    PartialEq,
    Debug,
    Default,
    Hash,
    Ord,
    PartialOrd,
    num_derive::ToPrimitive,
    num_derive::FromPrimitive,
    num_derive::NumOps,
    num_derive::NumCast,
    derive_more::From,
    derive_more::Into,
    serde::Serialize,
    serde::Deserialize,
)]
pub struct Millis(u64);

impl Millis {
    #[inline(always)]
    pub const fn new(n: u64) -> Self {
        Self(n)
    }

    pub const fn into_u64(self) -> u64 {
        self.0
    }

    pub const fn into_duration(self) -> std::time::Duration {
        std::time::Duration::from_millis(self.0)
    }

    #[inline(always)]
    pub fn now() -> Self {
        let millis_u128 = ::std::time::SystemTime::now()
            .duration_since(::std::time::UNIX_EPOCH)
            .unwrap_or_else(crash_on_err!())
            .as_millis();
        let millis_u64 = millis_u128 as u64;
        debug_assert_eq!(millis_u128, u128::from(millis_u64));
        Millis(millis_u64)
    }
}

impl std::fmt::Display for Millis {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter,
    ) -> std::fmt::Result {
        match self.0 {
            0..=9999 => write!(f, "{}ms", self.0),
            _ => write!(f, "{}s", self.0 / (1_000_000_000)),
        }
    }
}

impl CheckedSub for Millis {
    #[inline(always)]
    fn checked_sub(
        &self,
        v: &Self,
    ) -> Option<Self> {
        let minuend = self.into_u64();
        let subtrahend = v.into_u64();
        minuend.checked_sub(subtrahend).map(Self::new)
    }
}

impl std::convert::From<Millis> for std::time::Duration {
    fn from(millis: Millis) -> Duration {
        Duration::from_millis(millis.into_u64())
    }
}

/// Epoch timestamp in microseconds since the unix epoch
#[repr(transparent)]
#[derive(
    Copy,
    Clone,
    Eq,
    PartialEq,
    Debug,
    Default,
    Hash,
    Ord,
    PartialOrd,
    num_derive::ToPrimitive,
    num_derive::FromPrimitive,
    num_derive::NumOps,
    num_derive::NumCast,
    derive_more::From,
    derive_more::Into,
    serde::Serialize,
)]
pub struct Micros(u64);


impl Micros {
    pub const MICROSECOND: Micros = Micros::new(1);
    pub const MILLISECOND: Micros = Micros::new(1_000 * Micros::MICROSECOND.into_u64());
    pub const SECOND: Micros = Micros::new(1_000 * Micros::MILLISECOND.into_u64());

    #[inline(always)]
    pub const fn new(n: u64) -> Self {
        Self(n)
    }

    #[inline(always)]
    pub fn now() -> Self {
        let micros_u128 = ::std::time::SystemTime::now()
            .duration_since(::std::time::UNIX_EPOCH)
            .unwrap_or_else(crash_on_err!())
            .as_micros();
        let micros_u64 = micros_u128 as u64;
        debug_assert_eq!(micros_u128, u128::from(micros_u64));
        Micros(micros_u64)
    }

    #[inline(always)]
    pub fn from_millis(n: u64) -> Self {
        let micros_u128 = std::time::Duration::from_millis(n).as_micros();
        let micros_u64 = micros_u128 as u64;
        debug_assert_eq!(micros_u128, micros_u64 as u128);
        Self(micros_u64)
    }

    #[inline(always)]
    pub fn between_now_and(then: &Self) -> Option<Self> {
        Self::now().checked_sub(then)
    }

    #[inline(always)]
    pub fn since(
        &self,
        start: &Self,
    ) -> Option<Self> {
        start.checked_sub(self)
    }

    #[inline(always)]
    pub const fn into_u64(self) -> u64 {
        self.0
    }

    pub fn into_millis(self) -> Millis {
        Millis(self.0 / Self::MILLISECOND.0)
    }
}


impl CheckedSub for Micros {
    #[inline(always)]
    fn checked_sub(
        &self,
        v: &Self,
    ) -> Option<Self> {
        let minuend = self.into_u64();
        let subtrahend = v.into_u64();
        minuend.checked_sub(subtrahend).map(Self::new)
    }
}

impl std::fmt::Display for Micros {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter,
    ) -> std::fmt::Result {
        match self.0 {
            0..=999 => write!(f, "{}µs", self.0),
            // let ms go up to about 10 seconds
            1000..=99_999_999 => write!(f, "{}ms", self.0 / 1_000),
            _ => write!(f, "{}s", self.0 / (1_000_000_000)),
        }
    }
}

impl std::convert::From<Micros> for std::time::Duration {
    fn from(micros: Micros) -> Duration {
        Duration::from_micros(micros.into_u64())
    }
}

impl<'de> crate::deps::serde::Deserialize<'de> for Micros {
    fn deserialize<D>(deserializer: D) -> Result<Micros, D::Error>
    where
        D: crate::deps::serde::Deserializer<'de>,
    {
        use crate::deps::{
            const_format::formatcp,
            serde::de::{
                self,
                Visitor,
            },
        };

        use std::marker::PhantomData;
        use std::str::FromStr;

        struct StringOrInt(PhantomData<fn() -> Micros>);

        impl<'de> Visitor<'de> for StringOrInt {
            type Value = Micros;

            fn expecting(
                &self,
                formatter: &mut std::fmt::Formatter,
            ) -> std::fmt::Result {
                formatter.write_str(formatcp!(
                    "type Micros could not be deserialized with either std::str::FromStr or serde::Deserialize",
                ))
            }

            fn visit_str<E>(
                self,
                value: &str,
            ) -> Result<Micros, E>
            where
                E: de::Error,
            {
                let seconds = crate::deps::cosmos_units::seconds::Seconds::from_str(value)
                    .map_err(|_err| de::Error::custom("string value was not parsable as Seconds"))?;
                let value = *seconds.to_number().inner();
                let target_unit = *(crate::deps::cosmos_units::si::u.magnitude().inner());
                let value_as_target = value / target_unit;

                value_as_target
                    .to_integer()
                    .try_into()
                    .map(Micros::new)
                    .map_err(|_err| de::Error::custom("result overflowed u64"))
            }

            fn visit_i64<E: de::Error>(
                self,
                value: i64,
            ) -> Result<Micros, E> {
                u64::try_from(value)
                    .map_err(|_err| de::Error::custom("value was not in range of u64"))
                    .and_then(move |value| self.visit_u64(value))
            }

            fn visit_u64<E>(
                self,
                value: u64,
            ) -> Result<Micros, E> {
                Ok(Micros::new(value))
            }
        }

        deserializer.deserialize_any(StringOrInt(PhantomData))
    }
}

#[test]
fn deserialize_micros() {
    use crate::deps::serde_json;

    let ten_plain: Micros = serde_json::from_str(&"10").unwrap();
    assert_eq!(ten_plain, Micros::new(10));

    let one_expanded: Micros = serde_json::from_str(&"\"1 microsecond\"").unwrap();
    assert_eq!(one_expanded, Micros::new(1));

    let ten_micros_ascii: Micros = serde_json::from_str(&"\"10 us\"").unwrap();
    assert_eq!(ten_micros_ascii, Micros::new(10));

    let twelve_micros: Micros = serde_json::from_str(&"\"12 \u{3bc}s\"").unwrap();
    assert_eq!(twelve_micros, Micros::new(12));

    let hundred_millis: Micros = serde_json::from_str(&"\"100 ms\"").unwrap();
    assert_eq!(hundred_millis, Micros::new(100000));

    let two_seconds: Micros = serde_json::from_str(&"\"2 s\"").unwrap();
    assert_eq!(two_seconds, Micros::new(2000000));

    let bad: Result<Micros, _> = serde_json::from_str(&"\"10 ssss\"");
    assert!(bad.is_err());
}

/// Epoch timestamp in nanoseconds since the unix epoch or a duration
#[repr(transparent)]
#[derive(
    Copy,
    Clone,
    Eq,
    PartialEq,
    Debug,
    Default,
    Hash,
    Ord,
    PartialOrd,
    num_derive::ToPrimitive,
    num_derive::FromPrimitive,
    num_derive::NumOps,
    num_derive::NumCast,
    derive_more::From,
    derive_more::AddAssign,
    derive_more::Into,
    serde::Serialize,
    serde::Deserialize,
)]
pub struct Nanos(u64);


impl Nanos {
    pub const MICROSECOND: Nanos = Nanos::new(1_000 * Nanos::NANOSECOND.into_u64());
    pub const MILLISECOND: Nanos = Nanos::new(1_000 * Nanos::MICROSECOND.into_u64());
    pub const NANOSECOND: Nanos = Nanos::new(1);
    pub const SECOND: Nanos = Nanos::new(1_000 * Nanos::MILLISECOND.into_u64());
    const SECOND_F64: f64 = 1_000_000_000f64;

    #[inline(always)]
    pub const fn new(n: u64) -> Self {
        Self(n)
    }

    #[inline(always)]
    pub fn from_millis(n: u64) -> Self {
        let nanos_u128 = std::time::Duration::from_millis(n).as_nanos();
        let nanos_u64 = u64::try_from(nanos_u128).unwrap_or_else(crash_on_err!());
        Self(nanos_u64)
    }

    #[inline(always)]
    pub fn now() -> Self {
        let nanos_u128 = ::std::time::SystemTime::now()
            .duration_since(::std::time::UNIX_EPOCH)
            .unwrap_or_else(crash_on_err!())
            .as_nanos();
        let nanos_u64 = nanos_u128 as u64;
        debug_assert_eq!(nanos_u128, u128::from(nanos_u64));
        Self(nanos_u64)
    }

    #[inline(always)]
    pub fn between_now_and(then: &Self) -> Option<Self> {
        Self::now().checked_sub(then)
    }

    #[inline(always)]
    pub fn since(
        &self,
        start: &Self,
    ) -> Option<Self> {
        start.checked_sub(self)
    }

    #[inline(always)]
    pub const fn from_nanos(n: u64) -> Self {
        Nanos::new(n)
    }

    #[inline(always)]
    pub fn from_secs(n: u64) -> Self {
        let nanos_u128 = std::time::Duration::from_secs(n).as_nanos();
        Self::try_from(nanos_u128).unwrap_or_else(crash_on_err!())
    }

    #[inline(always)]
    pub fn from_micros(n: u64) -> Self {
        let nanos_u128 = std::time::Duration::from_micros(n).as_nanos();
        Self::try_from(nanos_u128).unwrap_or_else(crash_on_err!())
    }

    #[inline(always)]
    pub const fn into_u64(self) -> u64 {
        self.0
    }

    pub const fn into_duration(self) -> std::time::Duration {
        std::time::Duration::from_nanos(self.0)
    }

    #[inline(always)]
    pub const fn into_micros(self) -> Micros {
        Micros::new(self.0 / Self::MICROSECOND.0)
    }

    #[inline]
    pub fn as_secs_f64(self) -> f64 {
        use crate::deps::num::ToPrimitive;

        self.into_u64()
            .to_f64()
            .map(|nanos| nanos / Self::SECOND_F64)
            .unwrap_or_else(crash_on_none!("could not represent nanos"))
    }
}

impl CheckedMul for Nanos {
    fn checked_mul(
        &self,
        v: &Self,
    ) -> Option<Self> {
        self.0.checked_mul(v.0).map(Nanos::new)
    }
}


impl CheckedSub for Nanos {
    #[inline(always)]
    fn checked_sub(
        &self,
        v: &Self,
    ) -> Option<Self> {
        let minuend = self.into_u64();
        let subtrahend = v.into_u64();
        minuend.checked_sub(subtrahend).map(Self::new)
    }
}

impl std::convert::From<std::time::Duration> for Nanos {
    fn from(value: std::time::Duration) -> Nanos {
        Nanos::try_from(value.as_nanos()).unwrap_or_else(crash_on_err!())
    }
}

impl std::convert::From<Nanos> for std::time::Duration {
    fn from(nanos: Nanos) -> Duration {
        Duration::from_nanos(nanos.into_u64())
    }
}

impl std::convert::From<Millis> for Nanos {
    fn from(value: Millis) -> Nanos {
        Nanos::new(value.into_u64() * Self::MILLISECOND.into_u64())
    }
}

impl std::convert::From<Micros> for Nanos {
    fn from(value: Micros) -> Nanos {
        Nanos::new(value.into_u64() * Self::MICROSECOND.into_u64())
    }
}


impl std::convert::From<Seconds> for Nanos {
    fn from(value: Seconds) -> Nanos {
        Nanos::new(value.into_u64() * Self::SECOND.into_u64())
    }
}


impl std::convert::TryFrom<u128> for Nanos {
    type Error = <u64 as TryFrom<u128>>::Error;

    fn try_from(value: u128) -> core::result::Result<Self, Self::Error> {
        u64::try_from(value).map(Nanos::new)
    }
}

impl std::ops::Div<u64> for Nanos {
    type Output = Nanos;

    fn div(
        self,
        rhs: u64,
    ) -> Self::Output {
        self.0.checked_div(rhs).map(Nanos::new).unwrap_or_default()
    }
}

impl std::ops::Add<std::time::Duration> for Nanos {
    type Output = Nanos;

    fn add(
        self,
        rhs: Duration,
    ) -> Self::Output {
        Nanos::new(self.0 + u64::try_from(rhs.as_nanos()).unwrap_or_else(crash_on_err!()))
    }
}


impl<'a> std::ops::Add<&'a std::time::Duration> for Nanos {
    type Output = Nanos;

    fn add(
        self,
        rhs: &Duration,
    ) -> Self::Output {
        Nanos::new(self.0 + u64::try_from(rhs.as_nanos()).unwrap_or_else(crash_on_err!()))
    }
}

impl std::fmt::Display for Nanos {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter,
    ) -> std::fmt::Result {
        match self.0 {
            0..=999 => write!(f, "{}ns", self.0),
            1000..=999_999 => write!(f, "{}µs", self.0 / 1_000),
            1_000_000..=999_999_999 => write!(f, "{}ms", self.0 / (1_000_000)),
            _ => write!(f, "{} s", self.0 / (1_000_000_000)),
        }
    }
}
