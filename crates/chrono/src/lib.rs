//! # Cosmos Chrono
//!
//! 64 bit and FFI safe Nanos, Micros, Millis, and Seconds Time Quanta with utilities
#![deny(rustdoc::missing_crate_level_docs)]
#![deny(missing_debug_implementations)]

#[doc(hidden)]
pub(crate) mod deps {
    pub use ::cosmos_crash_macros;
    pub use ::cosmos_units;


    pub use ::const_format;
    pub use ::derive_more;
    pub use ::num;
    pub use ::num_derive;
    pub use ::num_traits;
    pub use ::serde;
    pub use ::serde_json;
}

mod quanta;
mod stopwatch;
pub mod task;
pub mod timed;
pub use self::quanta::*;
pub use self::stopwatch::*;
