//! # Logger implementation for services and daemons
#![deny(warnings)]
#![deny(missing_docs)]
#![deny(rustdoc::missing_crate_level_docs)]
#![deny(missing_debug_implementations)]

#[doc(hidden)]
pub(crate) mod deps {
    pub use ::cosmos_logger_core;
}

pub use crate::deps::cosmos_logger_core::{
    Choice,
    Color,
    Config,
    Environment,
    FileTarget,
    Rotate,
    Style,
    Target,
};

/// initialize the service logger, this is safe to call this more than once but only
/// the first configuration will persist
#[cold]
#[inline(never)]
pub fn init(config: Config) {
    crate::deps::cosmos_logger_core::init(config)
}
