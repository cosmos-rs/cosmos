use ::cosmos_proc_macros::cosmos;

#[doc(hidden)]
pub mod private {
    pub use ::cosmos_counter;
    pub use ::crossbeam_utils;
    pub use ::parking_lot;
    pub use ::paste;
    pub use ::tracing as log;
}


/// Generic way to reduce noise of warning where we want to know about a behavior but
/// logging it more than once would cause extra noise.
#[macro_export]
macro_rules! warn_once {
    ($name:ident; $($arg:tt)+) => {{
        use $crate::private::log::warn;

        static $name: $crate::private::parking_lot::Once = $crate::private::parking_lot::Once::new();

        $name.call_once(|| {
             $crate::private::log::warn!("[WARN_ONCE] {}", format_args!($($arg)*))
        });
   }};
}


#[macro_export]
macro_rules! warn_on_err {
        ($fmt:expr, $($arg:tt)*) => {{
          |err| {  $crate::private::log::warn!("{}: error={:?}", format_args!($fmt, $($arg)*), err) }
        }};
        ($msg:expr,) => {{
         |err| {  $crate::private::log::warn!("{}: error={:?}", $msg, err) }
        }};
        ($msg:expr) => {{
            $crate::warn_on_err!($msg,)
        }};
        () => {{
            $crate::warn_on_err!("potentially unrecoverable error encountered")
        }};
    }

/// Periodically warn every N times this macro is executed.
#[macro_export]
macro_rules! warn_every {
    ($n:expr, $($arg:tt)+) => {{
        use $crate::private::log::warn;
        const N: u32 = $n as u32;
        static WARN_EVERY_COUNTER: $crate::private::cosmos_counter::AtomicCounter<::std::sync::atomic::AtomicU32> = $crate::private::cosmos_counter::AtomicCounter::const_new(::std::sync::atomic::AtomicU32::new(0));
        let count = WARN_EVERY_COUNTER.inc();
        if ::std::matches!(count.checked_rem(N), Some(0)) {
            $crate::private::log::warn!(occurences=%count, warn_every=%N, $($arg)*)
        }
   }};
}


/// Only warn if the tracking value has changed since the last time the macro was executed.
#[macro_export]
macro_rules! warn_if_changed {
    ($cell_ty:ty, $value:expr, $($arg:tt)+) => {{
        use $crate::private::log::warn;
        static STORAGE: $crate::private::crossbeam_utils::atomic::AtomicCell<$cell_ty> = $crate::private::crossbeam_utils::atomic::AtomicCell::new(unsafe {  ::std::mem::transmute([0u8; ::std::mem::size_of::<$cell_ty>()]) });
        let stored_value = STORAGE.swap($value);
        if stored_value != $value {
            $crate::private::log::warn!(warn_when="value_changed", $($arg)*);
        }
   }};
}


/// Periodically error every N times this macro is executed.
#[macro_export]
macro_rules! error_every {
    ($n:expr, $($arg:tt)+) => {{
        use $crate::private::log::error;
        const N: u32 = $n as u32;
        static error_EVERY_COUNTER: $crate::private::cosmos_counter::AtomicCounter<::std::sync::atomic::AtomicU32> = $crate::private::cosmos_counter::AtomicCounter::const_new(::std::sync::atomic::AtomicU32::new(0));
        let count = error_EVERY_COUNTER.inc();
        if ::std::matches!(count.checked_rem(N), Some(0)) {
            $crate::private::log::error!(occurences=%count, error_every=%N, $($arg)*)
        }
   }};
}


/// Only error if the tracking value has changed since the last time the macro was executed.
#[macro_export]
macro_rules! error_if_changed {
    ($cell_ty:ty, $value:expr, $($arg:tt)+) => {{
        use $crate::private::log::error;
        static STORAGE: $crate::private::crossbeam_utils::atomic::AtomicCell<$cell_ty> = $crate::private::crossbeam_utils::atomic::AtomicCell::new(unsafe {  std::mem::MaybeUninit::uninit().assume_init() });
        let stored_value = STORAGE.swap($value);
        if stored_value != $value {
            $crate::private::log::error!(error_when="value_changed", $($arg)*);
        }
   }};
}

#[macro_export]
macro_rules! peek_trace {
    ($fmt:expr, $($arg:tt)*) => {{
      |err|{ $crate::private::log::trace!("{} error={:?}", format_args!($fmt, $($arg)*), err);  err }
    }};
    ($msg:expr,) => {{
     |err| { $crate::private::log::trace!("{} error={:?}", $msg, err); err }
    }};
    ($msg:expr) => (
        $crate::peek_trace!($msg,)
    );
    () => {{
        $crate::peek_trace!("potentially unrecoverable error encountered:")
    }};
}



#[macro_export]
macro_rules! peek_warn {
    ($($arg:tt)+) => (
     |err| { $crate::private::log::warn!(error=?err, $($arg)*); err }
    );
    () => (
        $crate::peek_warn!("potentially unrecoverable error encountered");
    );
}


#[macro_export]
macro_rules! peek_warn_every {
    ($n:expr, $($arg:tt)+) => (
     |err| { $crate::warn_every!($n, error=?err, $($arg)*); err }
    );
    ($n:expr ,?) => (
        $crate::peek_warn_every!($n, "potentially unrecoverable error encountered")
    );
}


#[macro_export]
macro_rules! peek_error {
    ($fmt:expr, $($arg:tt)*) => {{
      |err|{ $crate::private::log::error!("{} error={:?}", format_args!($fmt, $($arg)*), err);  err }
    }};
    ($msg:expr,) => {{
     |err| { $crate::private::log::error!("{} error={:?}", $msg, err); err }
    }};
    ($msg:expr) => (
        $crate::peek_error!($msg,)
    );
    () => {{
        $crate::peek_error!("potentially unrecoverable error encountered:")
    }};
}



#[cosmos(unit_test)]
fn warn_every_compiles() {
    for _ in 0..8 {
        warn_every!(4, "warn_every_compiles");
    }
}

#[cosmos(unit_test)]
fn warn_if_changed_compiles() {
    for i in 0..8 {
        warn_if_changed!(i32, i % 2, "warn_if_changed_compiles");
    }
}
