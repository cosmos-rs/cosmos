//! # Common Logger Implementation for Unit Tests
//!
//! Use instead of depending on the clunky ::testing::logger::initialize(). The main difference is
//! that color is "auto", meaning that logs will only be colorized if there the process is attached
//! to a TTY. To force specific color mode set the environment variable
//! `COSMOS_UNIT_TEST_LOGGER_COLOR=<auto|always|never>`. See
//! [`cosmos_logger_core::Color`] for all valid options.
//!
//! **Old crate specific method**
//!
//! ```ignore
//! #[test]
//! fn mytest() {
//!     crate::testing::logger::initialize();
//!     assert!(true);
//! }
//! ```
//!
//! **New shared implementation**
//!
//! ```ignore
//! #[test]
//! fn mytest() {
//!     crate::deps::cosmos_unit_test_logger::init();
//!     assert!(true);
//! }
//! ```
//!
//! **Soon with 100% less redundancy**
//!
//! Will synergize with with a new procedural macro `#[cosmos(unit_test)]` in the very immediate
//! future which allow creating tests that auto-init the logger!
//!
//! ```ignore
//! #[cosmos(unit_test)]
//! fn mytest() {
//!     assert!(true);
//! }
//! ```
#![deny(warnings)]
#![deny(missing_docs)]
#![deny(rustdoc::missing_crate_level_docs)]
#![deny(missing_debug_implementations)]

#[doc(hidden)]
pub(crate) mod deps {
    pub use ::cosmos_logger_core;
}

use crate::deps::cosmos_logger_core::{
    Directives,
    Environment,
    Target,
};


/// initialize the unit test logger, this is safe to call from every test case
#[cold]
#[inline(never)]
pub fn init() {
    use crate::deps::cosmos_logger_core::{
        Color,
        Config,
    };

    let color = std::env::var("COSMOS_UNIT_TEST_LOGGER_COLOR")
        .ok()
        .and_then(|s| s.parse::<Color>().ok())
        .unwrap_or_default();

    crate::deps::cosmos_logger_core::init(Config {
        use_env: Environment::default(),
        directives: Directives::default(),
        style: Default::default(),
        color,
        thread_ids: false,
        thread_names: false,
        filepaths: true,
        line_numbers: true,
        enable_logger_metrics: true,
        target: Target::default(),
        metrics_target: None,
    })
}
