//! Test to verify that the metrics feature for tracking the log volume works as expected
#![deny(unused)]
use cosmos_crash_macros::crash_on_err;
use cosmos_logger_core::{
    Directives,
    Environment,
    Metrics,
    MetricsControl,
    MetricsStore,
    Target,
};

use tracing::info;


/// initialize the unit test logger, this is safe to call from every test case
#[test]
pub fn test_with_metrics() {
    use cosmos_logger_core::{
        Color,
        Config,
    };

    let color = std::env::var("COSMOS_UNIT_TEST_LOGGER_COLOR")
        .ok()
        .and_then(|s| s.parse::<Color>().ok())
        .unwrap_or_default();

    let metrics_storage = MetricsStore::default();
    MetricsControl::initialize(metrics_storage.clone()).unwrap_or_else(crash_on_err!());

    cosmos_logger_core::init(Config {
        use_env: Environment::default(),
        directives: Directives::try_from(vec![format!("{}=trace", module_path!())])
            .unwrap_or_else(crash_on_err!()),
        style: Default::default(),
        color,
        thread_ids: false,
        thread_names: false,
        filepaths: true,
        line_numbers: true,
        enable_logger_metrics: true,
        target: Target::Sink,
        metrics_target: None,
    });

    let metrics = MetricsControl::reset();
    assert_eq!(
        metrics,
        Metrics::default(),
        "no logs have been emitted yet but metrics exist"
    );
    info!(?metrics);

    info!("abc123");

    let metrics = MetricsControl::reset();

    assert_ne!(
        metrics,
        Metrics::default(),
        "logs have been written yet metrics still appear to be zero"
    );
    assert_eq!(metrics.events(), 2);
    info!(?metrics);

    let _ = MetricsControl::reset();
    let metrics = MetricsControl::reset();
    info!(?metrics);
    assert_eq!(
        metrics,
        Metrics::default(),
        "no logs were written between metric resets but metric values were not zeroed "
    )
}
