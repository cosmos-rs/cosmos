use cosmos_crash_macros::crash_on_err;
use cosmos_logger_core::{
    Color,
    Config,
    Directives,
    Environment,
    FileTarget,
    MetricsControl,
    MetricsStore,
    Rotate,
    Target,
};
use criterion::black_box;

use std::path::Path;


/// initialize the unit test logger, this is safe to call from every test case
pub fn log_info(n: u64) {
    tracing::info!(n, "tah dah")
}


pub fn config_with_metrics(enable_metrics: bool) -> Config {
    let color = std::env::var("COSMOS_UNIT_TEST_LOGGER_COLOR")
        .ok()
        .and_then(|s| s.parse::<Color>().ok())
        .unwrap_or_default();

    Config {
        use_env: Environment::default(),
        directives: Directives::try_from(vec![format!("{}=trace", module_path!())])
            .unwrap_or_else(crash_on_err!()),
        style: Default::default(),
        color,
        thread_ids: false,
        thread_names: false,
        enable_logger_metrics: enable_metrics,
        target: Target::Sink,
        metrics_target: None,
        ..Default::default()
    }
}


#[inline(never)]
pub fn threaded<F>(
    func: F,
    iters: u64,
) -> std::time::Duration
where
    F: (Fn(u64) -> ()) + Copy + Send + 'static,
{
    let threads = std::thread::available_parallelism().map(|x| x.get()).unwrap_or(4);
    let threads = if threads > 2 { threads / 2 } else { threads };

    let mut join_handles = Vec::with_capacity(threads);
    let barrier = std::sync::Arc::new(std::sync::Barrier::new(threads + 1));

    for _ in 0..threads {
        let barrier = barrier.clone();
        let handle = std::thread::spawn(move || {
            barrier.wait();
            for _ in 0..iters {
                func(black_box(20));
            }
        });

        join_handles.push(handle);
    }
    barrier.wait();
    let start = std::time::Instant::now();
    join_handles
        .into_iter()
        .for_each(|h| h.join().unwrap_or_default());

    start.elapsed()
}
