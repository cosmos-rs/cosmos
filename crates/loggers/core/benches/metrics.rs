use criterion::{
    black_box,
    criterion_group,
    criterion_main,
    Criterion,
};

use cosmos_crash_macros::crash_on_err;
use cosmos_logger_core::{
    Directives,
    Environment,
    MetricsControl,
    MetricsStore,
    Target,
};

use tracing::info;

mod metrics_common;


fn criterion_benchmark(c: &mut Criterion) {
    cosmos_logger_core::init(metrics_common::config_with_metrics(true));

    c.bench_function("log_with_metrics", |b| {
        b.iter(|| metrics_common::log_info(black_box(20)))
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
