use criterion::{
    black_box,
    criterion_group,
    criterion_main,
    Criterion,
};

use cosmos_crash_macros::crash_on_err;
use cosmos_logger_core::{
    Directives,
    Environment,
    MetricsControl,
    MetricsStore,
    Target,
};

use tracing::info;


mod metrics_common;


fn criterion_benchmark(c: &mut Criterion) {
    cosmos_logger_core::init(metrics_common::config_with_metrics(true));
    c.bench_function("log_with_metrics_threaded", |b| {
        b.iter_custom(|iters| metrics_common::threaded(metrics_common::log_info, iters));
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
