// don't pull in the whole libc crate just to use one function
#[link(name = "c")]
extern "C" {
    fn isatty(fd: std::os::raw::c_int) -> std::os::raw::c_int;
}

/// operations to inspect text streams using the raw os file descriptors
#[derive(Debug)]
pub(crate) struct Stream {
    fd: std::os::raw::c_int,
}


impl Stream {
    /// create a stream from a raw fd
    pub const fn from_fd(fd: std::os::raw::c_int) -> Self {
        Self { fd }
    }

    /// create a stream from a reference to an open file
    #[allow(unused)]
    pub fn from_file(f: &std::fs::File) -> Self {
        use ::std::os::unix::io::AsRawFd;
        Self::from_fd(f.as_raw_fd())
    }

    /// convenience function to get the stream for stdout
    pub const fn stdout() -> Self {
        Self::from_fd(1)
    }

    /// convenience function to get the stream for stderr
    pub const fn stderr() -> Self {
        Self::from_fd(2)
    }

    /// check if the text stream represented by the file descriptor
    /// is a tty.
    pub fn is_tty(&self) -> bool {
        unsafe { isatty(self.fd) != 0 }
    }
}
