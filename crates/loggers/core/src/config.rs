use crate::deps::{
    serde,
    serde_with::{
        serde_as,
        DisplayFromStr,
    },
};
use crate::param::{
    Color,
    Environment,
    Style,
    Target,
};

use crate::Directives;


/// The logger configuration not entirely comprehensive but covers enough for the bespoke
/// loggers use cases.
#[serde_as]
#[derive(Debug, Clone, Default, serde::Serialize, serde::Deserialize)]
pub struct Config {
    /// Choose whether or not to use RUST_LOG environment variable
    #[serde_as(as = "DisplayFromStr")]
    pub use_env:               Environment,
    /// The style of the log lines
    #[serde_as(as = "DisplayFromStr")]
    pub style:                 Style,
    /// Choose whether to use ansi color escapes
    #[serde_as(as = "DisplayFromStr")]
    pub color:                 Color,
    /// include thread ids in log lines
    pub thread_ids:            bool,
    /// include thread names in log lines
    pub thread_names:          bool,
    /// include filepaths of callsites in log lines
    pub filepaths:             bool,
    /// include line numbers of callsites in log lines
    pub line_numbers:          bool,
    /// turn on aggregating metrics about the number of events
    /// and bytes of logs written
    pub enable_logger_metrics: bool,
    /// The output target of the logs
    #[serde(serialize_with = "serialize_target")]
    pub target:                Target,
    /// The output target for metrics
    #[serde(serialize_with = "serialize_opt_target")]
    pub metrics_target:        Option<Target>,
    /// The list of subscriber directives strings like `cosmos_app_sdk=info`
    pub directives:            Directives,
}


fn serialize_target<S>(
    value: &Target,
    serializer: S,
) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    use serde::Serialize;

    match value {
        Target::Stderr => "stderr".serialize(serializer),
        Target::Stdout => "stdout".serialize(serializer),
        Target::Fd(i) => i.serialize(serializer),
        Target::File(target) => target.serialize(serializer),
        Target::Sink => "std::io::sink".serialize(serializer),
    }
}


fn serialize_opt_target<S>(
    value: &Option<Target>,
    serializer: S,
) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    if let Some(value) = value.as_ref() {
        serialize_target(value, serializer)
    } else {
        serializer.serialize_none()
    }
}
