//! # Cosmos Logger Core - Implementation Agnostic Configuration for Logging
#![deny(warnings)]
#![deny(missing_docs)]
#![deny(rustdoc::missing_crate_level_docs)]
#![deny(missing_debug_implementations)]
#![deny(clippy::pedantic)]
#![allow(clippy::module_name_repetitions)]
#![allow(rustdoc::broken_intra_doc_links)]
#[doc(hidden)]
mod deps {
    pub use ::cosmos_build_extras;
    pub use ::cosmos_counter;
    pub use ::cosmos_crash_macros;
    pub use ::cosmos_stdlib_extras;

    pub use ::anyhow;
    pub use ::indexmap;
    pub use ::itertools;
    pub use ::log;
    pub use ::once_cell;
    pub use ::parking_lot;
    pub use ::serde;
    pub use ::serde_with;
    pub use ::tracing;
    pub use ::tracing_appender;
    pub use ::tracing_log;
    pub use ::tracing_subscriber;
}


mod config;
mod directives;
mod ext;
mod imp;
mod level;
mod metrics;
mod param;
#[cfg(target_family = "unix")]
mod stream;
mod workspace;

pub use crate::deps::indexmap::map::IntoIter as MapIntoIter;
pub use crate::deps::indexmap::IndexMap as Map;

pub use crate::{
    config::Config,
    directives::Directives,
    imp::tracing::{
        init,
        COSMOS_METRICS_TARGET,
    },
    level::Level,
    metrics::{
        Metrics,
        MetricsControl,
        MetricsStore,
    },
    param::{
        Choice,
        Color,
        Environment,
        FileTarget,
        Rotate,
        Style,
        Target,
    },
};


#[doc(hidden)]
pub mod backend {
    pub use crate::deps::tracing;
    pub use crate::deps::tracing_subscriber;
}
