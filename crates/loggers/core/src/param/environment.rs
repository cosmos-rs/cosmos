//! environment stuff
use crate::param::Choice;
use ::core::fmt;

/// Choose whether or not to use the environment
#[derive(Copy, Clone, Debug, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct Environment(Choice);

impl Environment {
    /// get the choice represented by this param
    #[must_use]
    pub const fn choice(self) -> Choice {
        self.0
    }
}

impl From<Choice> for Environment {
    fn from(c: Choice) -> Self {
        Environment(c)
    }
}

impl std::str::FromStr for Environment {
    type Err = ParseEnvironmentError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let choice = s.parse().map_err(|_| ParseEnvironmentError::new())?;
        Ok(Environment(choice))
    }
}

impl Default for Environment {
    fn default() -> Self {
        Environment(Choice::Yes)
    }
}

impl fmt::Display for Environment {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        self.choice().as_str().fmt(f)
    }
}

impl_error_type!(
    ParseEnvironmentError,
    "provided string was not `auto`, `always`, or `never`"
);
