//! Select between different styles of logs. The provided choices are the `tracing_subscriber::fmt`
//! defaults but it does not preclude us adding more of our own custom styles.
use crate::ext::StrExtensions;
use ::core::fmt;

/// The general style of a log line
#[derive(Copy, Clone, Debug, PartialEq, serde::Serialize, serde::Deserialize)]
pub enum Style {
    ///  Use the backing implementation default styling. Ok for humans and not too verbose
    /// but not great for machines.
    Vanilla,
    /// Print the log lines as json serialized objects. This is good for cloudwatch
    /// and tools.
    Json,
    /// Use the excessively stylized human representation it's pretty great for
    /// local debugging.
    Pretty,
    /// Use the compact version of the human styling. Which contains less whitespace.
    Compact,
}

impl Style {
    /// get the display string representation of the [`Style`] variant
    #[must_use]
    pub const fn as_str(&self) -> &'static str {
        match self {
            Style::Vanilla => "default",
            Style::Json => "json",
            Style::Pretty => "pretty",
            Style::Compact => "compact",
        }
    }
}


impl std::str::FromStr for Style {
    type Err = ParseStyleError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let value = s.to_normalized_param();
        match value.as_str() {
            "vanilla" | "human" | "default" => Ok(Self::Vanilla),
            "json" => Ok(Self::Json),
            "pretty" => Ok(Self::Pretty),
            "compact" => Ok(Self::Compact),
            _ => Err(ParseStyleError::with_message(value)),
        }
    }
}

impl Default for Style {
    fn default() -> Self {
        Self::Compact
    }
}

impl fmt::Display for Style {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        self.as_str().fmt(f)
    }
}


impl_error_type!(
    ParseStyleError,
    "Style: provided string was not `json`, `default`, `pretty`, or `compact`"
);
