/// ! Choose an output target for the log lines
use crate::deps::tracing_appender::rolling::Rotation as TracingRotation;
use crate::deps::{
    serde,
    serde_with::{
        serde_as,
        DisplayFromStr,
    },
};

use crate::ext::StrExtensions;
use ::core::fmt;
use ::std::str::FromStr;


/// Choose where to write the logs
#[derive(Clone, Debug, PartialEq, serde::Serialize)]
pub enum Target {
    /// use stdout
    Stdout,
    /// use stderr
    Stderr,
    /// use a file descriptor like created from a parent process and passed
    /// to the current process when file descriptors are cloned with dup2().
    Fd(std::os::raw::c_int),
    /// Noop target that just writes to a std::io::sink
    Sink,
    /// use a path to a file for the log output, the parent path to
    /// the file must already exist
    #[serde(rename = "file")]
    File(FileTarget),
}


impl<'de> crate::deps::serde::Deserialize<'de> for Target {
    fn deserialize<D>(deserializer: D) -> Result<Target, D::Error>
    where
        D: crate::deps::serde::Deserializer<'de>,
    {
        use crate::deps::serde::{
            de::{
                self,
                MapAccess,
                Visitor,
            },
            Deserialize,
        };
        use std::marker::PhantomData;

        struct StringOrStruct(PhantomData<fn() -> Target>);

        impl<'de> Visitor<'de> for StringOrStruct {
            type Value = Target;

            fn expecting(
                &self,
                formatter: &mut fmt::Formatter,
            ) -> fmt::Result {
                formatter.write_str("type ")?;
                formatter.write_str(::std::any::type_name::<Target>())?;
                formatter.write_str(
                    " could not be deserialized with either std::str::FromStr or serde::Deserialize",
                )
            }

            fn visit_str<E>(
                self,
                value: &str,
            ) -> Result<Target, E>
            where
                E: de::Error,
            {
                Ok(FromStr::from_str(value).unwrap())
            }

            fn visit_map<M>(
                self,
                map: M,
            ) -> Result<Target, M::Error>
            where
                M: MapAccess<'de>,
            {
                Ok(Target::File(Deserialize::deserialize(
                    de::value::MapAccessDeserializer::new(map),
                )?))
            }
        }

        deserializer.deserialize_any(StringOrStruct(PhantomData))
    }
}


impl std::str::FromStr for Target {
    type Err = std::convert::Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let value = s.to_normalized_param();
        match value.as_str() {
            "stdout" | "1" => Ok(Self::Stdout),
            "stderr" | "2" => Ok(Self::Stderr),
            "std::io::sink" => Ok(Self::Sink),
            other => {
                if let Ok(fd) = other.parse::<std::os::raw::c_int>() {
                    Ok(Self::Fd(fd))
                } else {
                    Ok(Self::File(FileTarget {
                        path:   std::path::PathBuf::from(s),
                        rotate: Rotate::Never,
                    }))
                }
            }
        }
    }
}

impl Default for Target {
    fn default() -> Self {
        Self::Stderr
    }
}


/// The options for a file target
#[serde_as]
#[derive(Clone, Debug, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct FileTarget {
    /// the path prefix to the file
    pub path:   ::std::path::PathBuf,
    /// how often to rotate the logs
    #[serde_as(as = "DisplayFromStr")]
    pub rotate: Rotate,
}


/// Log rotation frequency
#[derive(Clone, Debug, PartialEq, serde::Serialize, serde::Deserialize)]
#[allow(missing_docs)]
pub enum Rotate {
    EveryMinute,
    EveryHour,
    EveryDay,
    Never,
}


impl Rotate {
    /// get the display string of the [`Rotate`] variant
    #[must_use]
    pub const fn as_str(&self) -> &'static str {
        match self {
            Rotate::EveryMinute => "every-minute",
            Rotate::EveryHour => "every-hour",
            Rotate::EveryDay => "every-day",
            Rotate::Never => "never",
        }
    }
}


impl From<TracingRotation> for Rotate {
    fn from(rotation: TracingRotation) -> Self {
        match rotation {
            TracingRotation::MINUTELY => Self::EveryMinute,
            TracingRotation::HOURLY => Self::EveryHour,
            TracingRotation::DAILY => Self::EveryDay,
            TracingRotation::NEVER => Self::Never,
        }
    }
}

impl From<Rotate> for TracingRotation {
    fn from(rotate: Rotate) -> TracingRotation {
        match rotate {
            Rotate::EveryMinute => TracingRotation::MINUTELY,
            Rotate::EveryHour => TracingRotation::HOURLY,
            Rotate::EveryDay => TracingRotation::DAILY,
            Rotate::Never => TracingRotation::NEVER,
        }
    }
}


impl FromStr for Rotate {
    type Err = ParseRotateError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let normalized = s.to_normalized_param();
        let param = normalized.strip_first_matching_prefix(std::iter::IntoIterator::into_iter([
            "every ", "every-", "every_", "every",
        ]));

        match param {
            "minute" | "minutely" | "min" | "m" => Ok(Self::EveryMinute),
            "hour" | "hourly" | "h" | "default" | "yes" | "y" => Ok(Self::EveryHour),
            "day" | "daily" | "d" => Ok(Self::EveryDay),
            "never" | "none" | "no" | "" => Ok(Self::Never),
            _ => Err(ParseRotateError::with_message(normalized)),
        }
    }
}


impl fmt::Display for Rotate {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        self.as_str().fmt(f)
    }
}



impl_error_type!(
    ParseRotateError,
    "Rotate: provided string was not `every-minute`,`every-hour`, `every-day`, or `never`"
);
