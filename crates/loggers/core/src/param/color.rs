//! A choice representing whether or not to use the ansi color escape in the logs
use crate::ext::StrExtensions;
use crate::param::Choice;
use ::core::fmt;

/// The general style of a log line
#[derive(Copy, Clone, Debug, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct Color(Choice);

impl Color {
    /// get the choice represented by this param
    #[must_use]
    pub const fn choice(self) -> Choice {
        self.0
    }
}

impl From<Choice> for Color {
    fn from(c: Choice) -> Self {
        Color(c)
    }
}

impl std::str::FromStr for Color {
    type Err = ParseColorError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let value = s.to_normalized_param();
        let choice = value.parse().map_err(|_| ParseColorError::with_message(value))?;
        Ok(Color(choice))
    }
}

impl Default for Color {
    fn default() -> Self {
        Color(Choice::Auto)
    }
}

impl fmt::Display for Color {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        self.choice().as_str().fmt(f)
    }
}

impl_error_type!(
    ParseColorError,
    "Color: provided string was not `auto`, `always`, or `never`"
);
