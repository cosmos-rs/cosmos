//! Yes, No, or Auto (e.g. use the default Y/N)
use crate::deps::serde;
use crate::ext::StrExtensions;
use ::core::fmt;

/// Enum to store `yes|no|auto` choices and parse them from strings  
#[derive(Copy, Clone, Debug, PartialEq, serde::Serialize, serde::Deserialize)]
pub enum Choice {
    /// use the default behavior for the selection
    Auto,
    /// enable the thing
    Yes,
    /// disables the thing
    No,
}

impl Choice {
    /// get the display string representation of the [`Choice`] variant
    #[must_use]
    pub const fn as_str(&self) -> &'static str {
        match self {
            Choice::Auto => "auto",
            Choice::Yes => "yes",
            Choice::No => "no",
        }
    }
}

impl std::str::FromStr for Choice {
    type Err = ParseChoiceError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let value = s.to_normalized_param();
        match value.as_str() {
            "auto" => Ok(Self::Auto),
            // allow some reasonable `always` synonyms but do not advertise them in the
            // error message
            "always" | "yes" | "Y" | "y" | "1" | "true" => Ok(Self::Yes),
            // allow some reasonable `never` synonyms
            // but do not advertise them in the error message
            "never" | "no" | "n" | "N" | "0" | "false" => Ok(Self::No),
            _ => Err(ParseChoiceError::with_message(value)),
        }
    }
}

impl Default for Choice {
    fn default() -> Self {
        Self::Auto
    }
}

impl fmt::Display for Choice {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        self.as_str().fmt(f)
    }
}

impl_error_type!(
    ParseChoiceError,
    "Choice: provided string was not `auto`, `yes`, or `no`"
);
