pub use self::choice::Choice;
pub use self::color::Color;
pub use self::environment::Environment;
pub use self::style::Style;
pub use self::target::{
    FileTarget,
    Rotate,
    Target,
};

/// Expands to a lightweight parsing error type inspired by the stdlib's str parsing
/// errors like [`ParseBoolError`]. Example expansion of
/// `impl_error_type!(ParseColorError, "provided string was not `auto`, `always`, or `never`");`
/// is below:
///
/// ```rust
/// /// An error returned when parsing a `Color` using [`from_str`] fails
/// ///
/// /// [`from_str`]: super::FromStr::from_str
/// #[derive(Debug, Copy, Clone, PartialEq, Eq)]
/// pub struct ParseColorError {
///     _priv: (),
/// }
///
/// impl ParseColorError {
///     const fn new() -> Self {
///         ParseColorError { _priv: () }
///     }
/// }
///
/// impl ::std::fmt::Display for ParseColorError {
///     fn fmt(
///         &self,
///         f: &mut ::std::fmt::Formatter<'_>,
///     ) -> ::std::fmt::Result {
///         "provided string was not `auto`, `always`, or `never`".fmt(f)
///     }
/// }
/// ```
macro_rules! impl_error_type {
    ($name:ident, $msg:literal) => {
        /// An error returned when parsing a [`Self]` using [`from_str`] fails
        ///
        /// [`from_str`]: std::str::FromStr::from_str
        #[derive(Debug, Clone, PartialEq, Eq)]
        pub struct $name {
            message: String,
        }

        impl $name {
            #[allow(unused)]
            const fn new() -> Self {
                $name {
                    message: String::new(),
                }
            }

            #[allow(unused)]
            fn with_message<S: ToString>(s: S) -> Self {
                $name {
                    message: s.to_string(),
                }
            }
        }

        impl ::std::fmt::Display for $name {
            fn fmt(
                &self,
                f: &mut ::std::fmt::Formatter<'_>,
            ) -> ::std::fmt::Result {
                if self.message.is_empty() {
                    f.write_str($msg)
                } else {
                    write!(f, "{}, message={:?}", $msg, self.message)
                }
            }
        }

        impl ::std::error::Error for $name {}
    };
}


// todo: promote this to a proc macro?
/// Implements a FromStr option for deserialize
#[allow(unused)]
macro_rules! impl_string_or_struct_deserializer {
    ($name:ident, $err:ty) => {
        impl<'de> crate::deps::serde::Deserialize<'de> for $name {
            fn deserialize<D>(deserializer: D) -> Result<$name, D::Error>
            where
                D: crate::deps::serde::Deserializer<'de>,
            {

            use crate::deps::{
                const_format::formatcp,
                serde::{
                    de::{
                        self,
                        MapAccess,
                        Visitor,
                    },
                    Deserialize,
                },
            };
            use std::marker::PhantomData;
            // This is a Visitor that forwards string types to $name's `FromStr` impl and
            // forwards map types to $name's `Deserialize` impl. The `PhantomData` is to
            // keep the compiler from complaining about $name being an unused generic type
            // parameter. We need $name in order to know the Value type for the Visitor
            // impl.
            struct StringOrStruct(PhantomData<fn() -> $name>);

            impl<'de> Visitor<'de> for StringOrStruct
            {
                type Value = $name;

                fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                    formatter.write_str(
                        formatcp!("type {} could not be deserialized with either std::str::FromStr or serde::Deserialize", ::std::any::type_name::<$name>()),
                    )
                }

                fn visit_str<E>(self, value: &str) -> Result<$name, E>
                where
                    E: de::Error,
                {
                    Ok(FromStr::from_str(value).unwrap())
                }

                fn visit_map<M>(self, map: M) -> Result<$name, M::Error>
                where
                    M: MapAccess<'de>,
                {
                    // `MapAccessDeserializer` is a wrapper that turns a `MapAccess`
                    // into a `Deserializer`, allowing it to be used as the input to $name's
                    // `Deserialize` implementation. $name then deserializes itself using
                    // the entries from the map visitor.
                    Deserialize::deserialize(de::value::MapAccessDeserializer::new(map))
                }
            }

            deserializer.deserialize_any(StringOrStruct(PhantomData))
        }
    }
    };
}



mod choice;
mod color;
mod environment;
mod style;
mod target;
