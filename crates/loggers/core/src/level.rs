use crate::deps::{
    log,
    tracing,
};
use std::fmt;

/// standard log level definitions from log::Level but allow Off to be a valid level
/// for the cli and add a FromStr impl that provides the valid choices
/// as feedback when parsing fails.
#[repr(usize)]
#[derive(Copy, Clone, Debug, PartialEq, PartialOrd, Ord, Eq, Hash)]
#[allow(missing_docs)]
pub enum Level {
    Off = 0,
    Error = log::Level::Error as usize,
    Warn = log::Level::Warn as usize,
    Info = log::Level::Info as usize,
    Debug = log::Level::Debug as usize,
    Trace = log::Level::Trace as usize,
}


impl Level {
    /// Return the [`Level`] as the log crate level
    pub const fn to_log_level(self) -> Option<log::Level> {
        match self {
            Level::Off => None,
            Level::Error => Some(log::Level::Error),
            Level::Warn => Some(log::Level::Warn),
            Level::Info => Some(log::Level::Info),
            Level::Debug => Some(log::Level::Debug),
            Level::Trace => Some(log::Level::Trace),
        }
    }

    /// Return the [`Level`] as the tracing crate level
    pub const fn to_tracing_level(self) -> Option<tracing::Level> {
        match self {
            Level::Off => None,
            Level::Error => Some(tracing::Level::ERROR),
            Level::Warn => Some(tracing::Level::WARN),
            Level::Info => Some(tracing::Level::INFO),
            Level::Debug => Some(tracing::Level::DEBUG),
            Level::Trace => Some(tracing::Level::TRACE),
        }
    }

    /// return the string value of the log level
    pub const fn as_str(&self) -> &'static str {
        match self {
            Level::Off => "off",
            Level::Error => "error",
            Level::Warn => "warn",
            Level::Info => "info",
            Level::Debug => "debug",
            Level::Trace => "trace",
        }
    }
}


impl fmt::Display for Level {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        match self.to_log_level() {
            Some(level) => level.fmt(f),
            None => "".fmt(f),
        }
    }
}


impl std::str::FromStr for Level {
    type Err = ParseLevelError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "" | "OFF" | "off" => Ok(Self::Off),
            "TRACE" | "trace" => Ok(Self::Trace),
            "DEBUG" | "debug" => Ok(Self::Debug),
            "INFO" | "info" => Ok(Self::Info),
            "WARN" | "warn" => Ok(Self::Warn),
            "ERROR" | "error" => Ok(Self::Error),
            _ => Err(ParseLevelError::new()),
        }
    }
}


impl From<usize> for Level {
    fn from(value: usize) -> Self {
        match value {
            0 => Level::Off,
            1 => Level::Error,
            2 => Level::Warn,
            3 => Level::Info,
            4 => Level::Debug,
            _ => Level::Trace,
        }
    }
}


/// An error returned when parsing a `Style` using [`from_str`] fails
///
/// [`from_str`]: std::str::FromStr::from_str
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct ParseLevelError {
    _priv: (),
}

impl ParseLevelError {
    const fn new() -> Self {
        ParseLevelError { _priv: () }
    }
}

impl fmt::Display for ParseLevelError {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        f.write_str("Level: provided log level string was not one of `off`, `trace`, `debug`, `info`, `warn`, or `error`")
    }
}

impl std::error::Error for ParseLevelError {}


mod serde_proxy {
    use super::Level;
    use crate::deps::serde::de::Error;
    use crate::deps::serde::{
        Deserialize,
        Deserializer,
        Serialize,
        Serializer,
    };
    use ::std::str::FromStr;
    use std::fmt;


    impl<'de> Deserialize<'de> for Level {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: Deserializer<'de>,
        {
            use crate::deps::serde::de;

            #[derive(Default)]
            struct Visitor {
                marker: ::std::marker::PhantomData<fn() -> Level>,
            }

            impl<'de> de::Visitor<'de> for Visitor {
                type Value = Level;

                fn expecting(
                    &self,
                    formatter: &mut fmt::Formatter,
                ) -> fmt::Result {
                    formatter.write_str("expected a valid string for cosmos_logger_core::Level")
                }

                fn visit_u64<E>(
                    self,
                    v: u64,
                ) -> Result<Self::Value, E>
                where
                    E: Error,
                {
                    Ok(Level::from(v as usize))
                }

                fn visit_str<E>(
                    self,
                    v: &str,
                ) -> Result<Self::Value, E>
                where
                    E: de::Error,
                {
                    Level::from_str(v).map_err(|e| de::Error::custom(e.to_string()))
                }

                fn visit_none<E>(self) -> Result<Self::Value, E>
                where
                    E: de::Error,
                {
                    Ok(Level::Off)
                }

                fn visit_unit<E>(self) -> Result<Self::Value, E>
                where
                    E: de::Error,
                {
                    Ok(Level::Off)
                }
            }

            deserializer.deserialize_any(Visitor::default())
        }
    }
    impl Serialize for Level {
        fn serialize<S>(
            &self,
            serializer: S,
        ) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            self.as_str().serialize(serializer)
        }
    }
}
