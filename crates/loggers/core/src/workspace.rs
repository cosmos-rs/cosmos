use crate::deps::cosmos_build_extras::cargo::Package;
use std::collections::HashSet;

/// get all of the crate names and binaries as tracing target names with the `-` changed to `_`  
pub fn target_names() -> HashSet<String> {
    let packages = crate::deps::cosmos_build_extras::cargo::workspace_packages().unwrap();
    let mut targets = HashSet::new();
    for package in packages {
        let Package { name, binaries, .. } = package;
        targets.insert(name.replace('-', "_"));
        targets.extend(binaries.into_iter().map(|s| s.replace('-', "_")));
    }

    targets
}


#[test]
fn no_dashes_in_target_names() {
    assert!(target_names().into_iter().all(|target| !target.contains('-')))
}
