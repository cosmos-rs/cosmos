use crate::deps::tracing::Event;
use crate::deps::tracing::Subscriber;
use crate::deps::tracing_subscriber::fmt::format::Writer;
use crate::deps::tracing_subscriber::fmt::FmtContext;
use crate::deps::tracing_subscriber::fmt::FormatEvent;
use crate::deps::tracing_subscriber::fmt::FormatFields;
use crate::deps::tracing_subscriber::registry::LookupSpan;
use std::fmt;
use tracing::field::Field;
use tracing_subscriber::field;
use tracing_subscriber::field::{
    VisitFmt,
    VisitOutput,
};

/// # What
///
/// A tracing event formatter that will write field values as individual logs lines without
/// any normal logging info. Things like the callsite and level are wholly discarded.
///
/// ```ignore
/// 
/// info!(x=1, y=?['a','b','c'], v="yolo");
/// ```
///
/// will result in log lines like:
///
/// ```
/// 1
/// ['a','b','c']
/// yolo
/// ```
///
/// # Why
///
/// This is used to make emitting metrics directly from tracing much easier since
/// it gives full control of the formatting to the metrics struct.
#[derive(Default, Copy, Clone)]
pub struct FieldPerLine {
    _p: (),
}

impl<S, N> FormatEvent<S, N> for FieldPerLine
where
    S: Subscriber + for<'lookup> LookupSpan<'lookup>,
    N: for<'writer> FormatFields<'writer> + 'static,
{
    fn format_event(
        &self,
        _ctx: &FmtContext<'_, S, N>,
        writer: Writer<'_>,
        event: &Event<'_>,
    ) -> fmt::Result
    where
        S: Subscriber + for<'a> LookupSpan<'a>,
    {
        let mut visitor = FieldVisitor::new(writer);
        event.record(&mut visitor);
        visitor.finish()
    }
}

impl fmt::Debug for FieldPerLine {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        f.debug_struct("FieldPassthroughFormatter")
            .finish_non_exhaustive()
    }
}


/// Visitor for fields in a tracing event. Write each **FIELD VALUE** as its own
/// line. No other information is preserved,
struct FieldVisitor<'a> {
    writer: Writer<'a>,
    result: fmt::Result,
}

impl<'a> FieldVisitor<'a> {
    /// Returns a new default visitor that formats to the provided `writer`.
    ///
    /// # Arguments
    /// - `writer`: the writer to format to.
    pub fn new(writer: Writer<'a>) -> Self {
        Self {
            writer,
            result: Ok(()),
        }
    }
}


impl<'a> field::Visit for FieldVisitor<'a> {
    fn record_debug(
        &mut self,
        _field: &Field,
        value: &dyn fmt::Debug,
    ) {
        if self.result.is_err() {
            return;
        }

        self.result = writeln!(self.writer, "{:?}", value);
    }
}

impl<'a> VisitOutput<fmt::Result> for FieldVisitor<'a> {
    fn finish(self) -> fmt::Result {
        self.result
    }
}

impl<'a> VisitFmt for FieldVisitor<'a> {
    fn writer(&mut self) -> &mut dyn fmt::Write {
        &mut self.writer
    }
}
