use crate::config::Config;
use crate::deps::parking_lot::Mutex;
use crate::deps::tracing_subscriber::fmt::writer::BoxMakeWriter;
use crate::deps::{
    cosmos_crash_macros::crash_on_err,
    parking_lot::Once,
    tracing::subscriber::set_global_default,
    tracing_appender::rolling::{
        RollingFileAppender,
        Rotation,
    },
    tracing_log::LogTracer,
    tracing_subscriber::EnvFilter,
};
use crate::metrics::InstrumentedWriter;
use crate::param::{
    Choice,
    Style,
    Target,
};
use std::fs::File;
use std::io;
use std::os::unix::io::FromRawFd;
use std::sync::Arc;

use crate::stream::Stream;
use crate::Directives;
use crate::FileTarget;

mod formatters;
use self::formatters::FieldPerLine as MetricsFormatter;

static INITIALIZE_LOGGER_ONCE: Once = Once::new();

/// reserved target name for emitting metrics
pub const COSMOS_METRICS_TARGET: &'static str = "__cosmos_metrics__";


/// almost each step of the builder changes some generic types on the subscriber
/// builder instance which changes the concrete type so I had to develop a macro
/// that is the "template expansion" method for resolving this case
macro_rules! finish_subscriber {
    ($subscriber_1:ident, $writer:expr,$metrics_writer:expr,  $is_tty:expr,  $style:ident, $color:ident,
$filter:ident) => {{
        use crate::deps::tracing_subscriber::{
            self,
            prelude::*,
        };

        let subscriber_2 = match $color.choice() {
            Choice::Auto => {
                if ($is_tty) {
                    $subscriber_1.with_ansi(true)
                } else {
                    $subscriber_1.with_ansi(false)
                }
            }
            Choice::Yes => $subscriber_1.with_ansi(true),
            Choice::No => $subscriber_1.with_ansi(false),
        };

        let subscriber_3 = subscriber_2.with_writer($writer);

        let registry = tracing_subscriber::registry();

        match $style {
            Style::Vanilla => {
                set_global_default(
                    registry.with(subscriber_3.with_filter($filter)).with(
                        tracing_subscriber::fmt::layer()
                            .with_writer($metrics_writer)
                            .event_format(MetricsFormatter::default())
                            .with_filter(tracing_subscriber::filter::filter_fn(|metadata| {
                                metadata.target() == COSMOS_METRICS_TARGET
                            })),
                    ),
                )
            }
            Style::Json => {
                set_global_default(
                    registry.with(subscriber_3.json().with_filter($filter)).with(
                        tracing_subscriber::fmt::layer()
                            .with_writer($metrics_writer)
                            .event_format(MetricsFormatter::default())
                            .with_filter(tracing_subscriber::filter::filter_fn(|metadata| {
                                metadata.target() == COSMOS_METRICS_TARGET
                            })),
                    ),
                )
            }
            Style::Pretty => {
                set_global_default(
                    registry.with(subscriber_3.pretty().with_filter($filter)).with(
                        tracing_subscriber::fmt::layer()
                            .with_writer($metrics_writer)
                            .event_format(MetricsFormatter::default())
                            .with_filter(tracing_subscriber::filter::filter_fn(|metadata| {
                                metadata.target() == COSMOS_METRICS_TARGET
                            })),
                    ),
                )
            }
            Style::Compact => {
                set_global_default(
                    registry.with(subscriber_3.compact().with_filter($filter)).with(
                        tracing_subscriber::fmt::layer()
                            .with_writer($metrics_writer)
                            .event_format(MetricsFormatter::default())
                            .with_filter(tracing_subscriber::filter::filter_fn(|metadata| {
                                metadata.target() == COSMOS_METRICS_TARGET
                            })),
                    ),
                )
            }
        }
    }};
}

/// Initialize the test logger. It is safe to call this from every test since the inner logger
/// initialization logic is wrapped in a [`Once`].
#[cold]
#[inline(never)]
pub fn init(config: Config) {
    INITIALIZE_LOGGER_ONCE.call_once(move || {
        let Config {
            use_env,
            filepaths,
            line_numbers,
            enable_logger_metrics: enable_metrics,
            metrics_target,
            directives,
            style,
            color,
            thread_ids,
            thread_names,
            target,
        } = &config;

        // install the interceptor for `log` crate macros (`info!`, etc) that convert
        // those log lines into tracing events
        LogTracer::init().unwrap_or_else(crash_on_err!(
            "could not initialize the tracing `log` (crate) tracer"
        ));
        let enable_metrics = *enable_metrics;
        // Create the logging directives from the config
        // and optionally from the environment.
        let mut filter = match use_env.choice() {
            Choice::Auto | Choice::Yes => {
                // prime our filter from the filter directives set in RUST_LOG
                EnvFilter::from_default_env()
            }
            Choice::No => EnvFilter::default(),
        };

        let disable_metrics = format!("{}=error", COSMOS_METRICS_TARGET);
        filter = filter.add_directive(disable_metrics.parse().unwrap_or_else(crash_on_err!(
            "could not parse logging subscriber directive: value={:?}",
            disable_metrics
        )));

        let mut workspace_crates_set = crate::workspace::target_names();
        let mut root_level = None;
        let mut cosmos_default_level = None;

        for (path, level) in directives.iter() {
            // if a cosmos crate name is explicitly specified within the directives
            // prefer the explicitly stated level
            workspace_crates_set.remove(path);

            let string = if path == Directives::rust_default_key() {
                root_level = Some(level.clone());
                level.to_string()
            } else if path == Directives::cosmos_default_key() {
                cosmos_default_level = Some(level.clone());
                continue;
            } else {
                format!("{}={}", path, level)
            };

            let directive = string.parse().unwrap_or_else(crash_on_err!(
                "could not parse logging subscriber directive: value={:?}",
                string
            ));

            filter = filter.add_directive(directive);
        }

        let workspace_level = cosmos_default_level.as_ref().or(root_level.as_ref());

        if let Some(level) = workspace_level {
            // default cosmos workspace crates to the cosmos_default_level if not
            // explicitly specified in the directives.
            for crate_name in workspace_crates_set.into_iter() {
                let string = format!("{}={}", crate_name, level);
                let directive = string.parse().unwrap_or_else(crash_on_err!(
                    "could not parse logging subscriber directive: value={:?}",
                    string
                ));

                filter = filter.add_directive(directive);
            }
        }


        let logging_layer = tracing_subscriber::fmt::layer()
            .with_file(*filepaths)
            .with_line_number(*line_numbers)
            .with_thread_names(*thread_names)
            .with_thread_ids(*thread_ids);


        let (writer, is_tty) = new_make_writer(target, enable_metrics);
        let (metrics_writer, _) =
            new_make_writer(metrics_target.as_ref().unwrap_or(&Target::Sink), enable_metrics);

        let logging_layer = match color.choice() {
            Choice::Auto => {
                if is_tty {
                    logging_layer.with_ansi(true)
                } else {
                    logging_layer.with_ansi(false)
                }
            }
            Choice::Yes => logging_layer.with_ansi(true),
            Choice::No => logging_layer.with_ansi(false),
        };

        let result = finish_subscriber!(
            logging_layer,
            writer,
            metrics_writer,
            is_tty,
            style,
            color,
            filter
        );

        result.unwrap_or_else(crash_on_err!(
            "could not setup global log event subscriber: config={:?}",
            config
        ));
    });
}



fn new_make_writer(
    target: &Target,
    enable_metrics: bool,
) -> (BoxMakeWriter, bool) {
    match target {
        Target::Stdout => {
            let writer = if enable_metrics {
                BoxMakeWriter::new(move || InstrumentedWriter::new(std::io::stdout()))
            } else {
                BoxMakeWriter::new(|| std::io::stdout())
            };
            (writer, Stream::stdout().is_tty())
        }
        Target::Stderr => {
            let writer = if enable_metrics {
                BoxMakeWriter::new(move || InstrumentedWriter::new(std::io::stderr()))
            } else {
                BoxMakeWriter::new(|| std::io::stderr())
            };
            (writer, Stream::stderr().is_tty())
        }
        Target::Fd(fd) => {
            let file = unsafe { File::from_raw_fd(*fd) };
            let file_writer = FileWriter::new(file);

            let writer = if enable_metrics {
                BoxMakeWriter::new({
                    let file_writer = file_writer.clone();
                    move || InstrumentedWriter::new(file_writer.clone())
                })
            } else {
                BoxMakeWriter::new({
                    let file_writer = file_writer.clone();
                    move || file_writer.clone()
                })
            };
            (writer, Stream::from_fd(*fd).is_tty())
        }
        Target::File(FileTarget { path, rotate }) => {
            let dir = path
                .parent()
                .map(std::borrow::ToOwned::to_owned)
                .or_else(|| std::env::current_dir().ok())
                .unwrap_or_default();

            let prefix = path
                .file_name()
                .and_then(std::ffi::OsStr::to_str)
                .map(ToString::to_string)
                .unwrap_or_default();

            let rotation: Rotation = rotate.clone().into();

            if enable_metrics {
                let factory = {
                    let prefix = prefix.clone();
                    let dir = dir.clone();
                    let rotation = rotation.clone();
                    move || {
                        InstrumentedWriter::new(RollingFileAppender::new(
                            rotation.clone(),
                            dir.clone(),
                            prefix.clone(),
                        ))
                    }
                };

                (BoxMakeWriter::new(factory), false)
            } else {
                let factory = {
                    let prefix = prefix.clone();
                    let dir = dir.clone();
                    let rotation = rotation.clone();
                    move || RollingFileAppender::new(rotation.clone(), dir.clone(), prefix.clone())
                };

                (BoxMakeWriter::new(factory), false)
            }
        }
        Target::Sink => {
            if enable_metrics {
                let factory = { move || InstrumentedWriter::new(std::io::sink()) };
                (BoxMakeWriter::new(factory), false)
            } else {
                let factory = std::io::sink;
                (BoxMakeWriter::new(factory), false)
            }
        }
    }
}



/// This wrapper is just to define `io::Write` on an `Arc<Mutex<File>>`
#[derive(Debug, Clone)]
struct FileWriter {
    file: Arc<Mutex<File>>,
}

impl FileWriter {
    pub fn new(file: File) -> Self {
        Self {
            file: Arc::new(Mutex::new(file)),
        }
    }
}

impl io::Write for FileWriter {
    fn write(
        &mut self,
        buf: &[u8],
    ) -> io::Result<usize> {
        self.file.lock().write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.file.lock().flush()
    }
}
