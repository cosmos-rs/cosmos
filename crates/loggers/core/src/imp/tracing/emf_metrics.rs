use crate::deps::tracing::Event;
use crate::deps::tracing::Subscriber;
use crate::deps::tracing_subscriber::fmt::format::Writer;
use crate::deps::tracing_subscriber::fmt::FmtContext;
use crate::deps::tracing_subscriber::fmt::FormatEvent;
use crate::deps::tracing_subscriber::fmt::FormatFields;
use crate::deps::tracing_subscriber::registry::LookupSpan;
use std::fmt;
use tracing::field::Field;
use tracing_subscriber::field;
use tracing_subscriber::field::{VisitFmt, VisitOutput};

#[derive(Default, Copy, Clone)]
pub struct Metrics {
    _p: (),
}

impl<S, N> FormatEvent<S, N> for Metrics
where
    S: Subscriber + for<'lookup> LookupSpan<'lookup>,
    N: for<'writer> FormatFields<'writer> + 'static,
{
    fn format_event(&self, _ctx: &FmtContext<'_, S, N>, writer: Writer<'_>, event: &Event<'_>) -> fmt::Result
    where
        S: Subscriber + for<'a> LookupSpan<'a>,
    {
        let mut visitor = MetricsVisitor::new(writer);
        event.record(&mut visitor);
        visitor.finish()
    }
}

impl fmt::Debug for Metrics {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Metrics").finish_non_exhaustive()
    }
}

struct MetricsVisitor<'a> {
    writer: Writer<'a>,
    result: fmt::Result,
}

impl<'a> MetricsVisitor<'a> {
    /// Returns a new default visitor that formats to the provided `writer`.
    ///
    /// # Arguments
    /// - `writer`: the writer to format to.
    pub fn new(writer: Writer<'a>) -> Self {
        Self {
            writer,
            result: Ok(()),
        }
    }
}

impl<'a> field::Visit for MetricsVisitor<'a> {
    fn record_debug(&mut self, _field: &Field, value: &dyn fmt::Debug) {
        if self.result.is_err() {
            return;
        }

        self.result = writeln!(self.writer, "{:?}", value);
    }
}

impl<'a> VisitOutput<fmt::Result> for MetricsVisitor<'a> {
    fn finish(self) -> fmt::Result {
        self.result
    }
}

impl<'a> VisitFmt for MetricsVisitor<'a> {
    fn writer(&mut self) -> &mut dyn fmt::Write {
        &mut self.writer
    }
}
