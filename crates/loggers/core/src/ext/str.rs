/// Adds methods to [`str`] to help with parsing  [`crate::param`]
pub trait StrExtensions: private::Sealed {
    /// Strip the first matching prefix string from `I` from the string
    fn strip_first_matching_prefix<'a, I>(
        &self,
        prefixes: I,
    ) -> &str
    where
        Self: Sized,
        I: Iterator<Item = &'a str>;

    /// normalize the "param string" by trimming preceding and trailing whitespace
    /// and converting to ascii lowercase
    fn to_normalized_param(&self) -> String;
}


impl StrExtensions for &str {
    fn strip_first_matching_prefix<'a, I>(
        &self,
        mut prefixes: I,
    ) -> &str
    where
        Self: Sized,
        I: Iterator<Item = &'a str>,
    {
        prefixes.find_map(|pat| self.strip_prefix(pat)).unwrap_or(self)
    }

    fn to_normalized_param(&self) -> String {
        self.trim().to_ascii_lowercase()
    }
}

impl StrExtensions for String {
    fn strip_first_matching_prefix<'a, I>(
        &self,
        mut prefixes: I,
    ) -> &str
    where
        Self: Sized,
        I: Iterator<Item = &'a str>,
    {
        prefixes
            .find_map(|pat| self.strip_prefix(pat))
            .unwrap_or_else(|| self.as_str())
    }

    fn to_normalized_param(&self) -> String {
        self.as_str().to_normalized_param()
    }
}

mod private {
    pub trait Sealed {}
    impl<'a> Sealed for &'a str {}
    impl Sealed for String {}
}
