use ::core::fmt;

use std::str::FromStr;

use crate::deps::serde;
use crate::{
    Level,
    Map,
    MapIntoIter,
};



/// The list of subscriber directives strings like `cosmos_app_sdk=info`
#[repr(transparent)]
#[derive(Debug, Clone, Default, serde::Serialize)]
pub struct Directives(Map<String, Level>);

impl Directives {
    const COSMOS_DEFAULT_KEY: &'static str = "$cosmos_default$";
    const RUST_DEFAULT_KEY: &'static str = "$rust_default$";

    /// return an iterator over the `(String, Level)` pairs of the directives
    pub fn iter(&self) -> impl Iterator<Item = (&String, &Level)> {
        self.0.iter()
    }

    /// easier to to use than setting each crate and binary sepearately
    /// setting "`$cosmos_default$ = "info"` is equivalent to setting
    /// all of the workspace creates and binary targets to `"info"` unless
    /// they are explicitly specified in the directives.
    pub const fn cosmos_default_key() -> &'static str {
        Self::COSMOS_DEFAULT_KEY
    }

    /// easier to read in configs than `= "ERROR"` and such since the empty path
    /// in the logging parlance generally means the root path, "::".
    pub const fn rust_default_key() -> &'static str {
        Self::RUST_DEFAULT_KEY
    }
}


impl IntoIterator for Directives {
    type IntoIter = MapIntoIter<String, Level>;
    type Item = (String, Level);

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}


impl TryFrom<Vec<String>> for Directives {
    type Error = crate::deps::anyhow::Error;

    fn try_from(directives: Vec<String>) -> Result<Self, Self::Error> {
        Self::try_from(directives.as_slice())
    }
}


impl TryFrom<&Vec<String>> for Directives {
    type Error = crate::deps::anyhow::Error;

    fn try_from(directives: &Vec<String>) -> Result<Self, Self::Error> {
        Self::try_from(directives.as_slice())
    }
}


impl TryFrom<&[String]> for Directives {
    type Error = crate::deps::anyhow::Error;

    fn try_from(directives: &[String]) -> Result<Self, Self::Error> {
        use crate::deps::itertools::Itertools;
        let mut map = Map::default();

        for string in directives.iter() {
            let (module, level) = string
                .trim()
                .splitn(2, '=')
                .collect_tuple::<(&str, &str)>()
                .or_else(|| Some((Self::rust_default_key(), string)))
                .ok_or_else(|| {
                    anyhow::anyhow!(
                    "could not parse logging directive in the form `<path>=<level>` from value: value={:?}",
                    string)
                })?;

            let level = Level::from_str(level).map_err(|err| anyhow::Error::msg(err))?;
            map.insert(module.to_string(), level);
        }

        Ok(Self(map))
    }
}


impl<'de> serde::Deserialize<'de> for Directives {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as serde::Deserializer<'de>>::Error>
    where
        D: serde::Deserializer<'de>,
    {
        use crate::deps::serde::de;

        #[derive(Default)]
        struct Visitor {
            marker: ::std::marker::PhantomData<fn() -> Directives>,
        }

        impl<'de> de::Visitor<'de> for Visitor {
            type Value = Directives;

            fn expecting(
                &self,
                formatter: &mut fmt::Formatter,
            ) -> fmt::Result {
                formatter.write_str("expected a sequence of strings or a map<string, string>")
            }

            fn visit_none<E>(self) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                Ok(Directives::default())
            }

            fn visit_unit<E>(self) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                Ok(Directives::default())
            }

            fn visit_seq<A>(
                self,
                mut seq: A,
            ) -> Result<Self::Value, <A as de::SeqAccess<'de>>::Error>
            where
                A: de::SeqAccess<'de>,
            {
                let mut array = Vec::<String>::new();

                // Update the max while there are additional values.
                while let Some(value) = seq.next_element()? {
                    array.push(value);
                }

                let directives = Directives::try_from(&array[..]).map_err(de::Error::custom)?;
                Ok(directives)
            }

            fn visit_map<A>(
                self,
                mut map: A,
            ) -> Result<Self::Value, <A as de::MapAccess<'de>>::Error>
            where
                A: de::MapAccess<'de>,
            {
                let mut directives = Directives::default();
                while let Some((key, value)) = map.next_entry::<String, String>()? {
                    let level = Level::from_str(value.as_str()).map_err(de::Error::custom)?;

                    directives.0.insert(key, level);
                }

                Ok(directives)
            }
        }

        deserializer.deserialize_any(Visitor::default())
    }
}
