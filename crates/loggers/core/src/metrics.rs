/*!

# Metrics

Tools to track the amount of log events generated and bytes written to the output.

## Behavior

The implementation used to track metrics assumes that there is occasional polling on the order
of seconds or possibly minutes. In order to minimize the additional performance impact on
logging, this implementation uses a single cache padded AtomicU64 to store both the number of
bytes written to the destination (low bytes) and the number of events (high bytes). So the metrics
store must be reset before either u32 rolls over and corrupts the metrics of the other.

In no case do we expect our logging to produce more than 2^32 events or
bytes within the polling interval.

## Performance

 The benchmark results below show that the un-contended case has overhead in the
nanoseconds on our test machine. The threaded benchmarks spawn threads for half of the cores
and call `tracing::info!()` in a loop.  Even with the extreme contention case, this
implementation adds only modest overhead.

`$ cargo --profile bench -- bench --verbose -p cosmos-logger-core -- --nocapture`

```text,ignore
 log_no_metrics            time:   [3.6359 us 3.6389 us 3.6419 us]
 log_with_metrics          time:   [3.6691 us 3.6718 us 3.6748 us]

 log_no_metrics_threaded   time:   [47.192 us 50.090 us 52.722 us]
 log_with_metrics_threaded time:   [55.428 us 58.200 us 60.825 us]
```

!*/

use crate::deps::cosmos_counter::CounterU64;
use crate::deps::once_cell::sync::OnceCell;
use std::fmt;
use std::io;
use std::sync::Arc;


static METRICS: OnceCell<MetricsStore> = OnceCell::new();


/// The non atomic version of the metrics values
#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct Metrics {
    bytes:  u32,
    events: u32,
}

impl Metrics {
    /// the number of bytes since the last reset
    pub const fn bytes(&self) -> u32 {
        self.bytes
    }

    /// the number of events since the last reset
    pub const fn events(&self) -> u32 {
        self.events
    }
}



/// The controller for the statically initialized metrics storage
pub struct MetricsControl {
    _p: (),
}


impl MetricsControl {
    /// Install a specific metrics storage
    pub fn initialize(store: MetricsStore) -> Result<(), &'static str> {
        METRICS
            .set(store)
            .map_err(|_| "Metrics::initialize was already called")
    }

    /// Reset the the metrics storage to zero returning the values before it was reset
    #[inline]
    pub fn reset() -> Metrics {
        Self::instance().reset()
    }

    #[inline(always)]
    pub(crate) fn record_usize(n: usize) -> usize {
        Self::record_u64(n as u64) as usize
    }

    #[inline(always)]
    pub(crate) fn record_u64(n: u64) -> u64 {
        Self::instance().record(n);
        n
    }

    #[inline]
    fn instance() -> &'static MetricsStore {
        METRICS.get_or_init(Default::default)
    }
}

impl fmt::Debug for MetricsControl {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        f.write_str("MetricsControl")
    }
}


/// A wrapper around an Arc<Mutex<Metrics>> for a more friendly naming
#[derive(Clone, Default)]
pub struct MetricsStore {
    store: Arc<Inner>,
}

impl MetricsStore {
    /// Get a copy of the current metric values without resetting the
    /// counters
    #[inline]
    pub fn values(&self) -> Metrics {
        self.store.values()
    }

    /// zero the current accumulated metrics and return the
    /// the last values
    #[inline]
    pub fn reset(&self) -> Metrics {
        self.store.reset()
    }

    #[inline(always)]
    fn record(
        &self,
        n: u64,
    ) {
        self.store.record(n)
    }
}

impl fmt::Debug for MetricsStore {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        let Metrics { bytes, events } = self.values();

        f.debug_struct("MetricsStore")
            .field("bytes", &bytes)
            .field("events", &events)
            .finish()
    }
}


// align to 2 cache lines to prevent false sharing contention between threads
// you can read more about this here: https://docs.rs/crossbeam/latest/crossbeam/utils/struct.CachePadded.html#size-and-alignment
/// The specific metrics storage
#[repr(align(128))]
#[derive(Default)]
#[allow(missing_debug_implementations)]
struct Inner {
    /// CounterU64 is backed by an AtomicU64 and with api exposes
    /// operations that abstract away the need to think about
    /// memory orderings
    ///  
    /// So store the number of events in the high bits and the number of bytes in
    /// the low bits so an update is one cheap-ish atomic operation.
    events_and_bytes: CounterU64,
}

impl Inner {
    /// mask the low bits
    const LOW_BITS_MASK: u64 = 0x0000_0000_ffff_ffff;
    /// add one to the high bits
    const ONE_HIGH_BIT: u64 = 0x0000_0001_0000_0000;

    #[inline(always)]
    fn values(&self) -> Metrics {
        let (bytes, events) = split_u64(self.events_and_bytes.value());
        Metrics { bytes, events }
    }

    #[inline(always)]
    fn record(
        &self,
        size: u64,
    ) {
        self.events_and_bytes
            .add((size & Self::LOW_BITS_MASK) + Self::ONE_HIGH_BIT);
    }

    /// zero the current accumulated metrics and return the
    /// the preivously stored values
    #[inline(always)]
    fn reset(&self) -> Metrics {
        let (bytes, events) = split_u64(self.events_and_bytes.reset());
        Metrics { bytes, events }
    }
}



/// Wrap a writer and keep track of the number of calls
/// to write and the number of bytes written
pub struct InstrumentedWriter<T: io::Write> {
    writer: T,
}


impl<T> InstrumentedWriter<T>
where
    T: io::Write,
{
    pub fn new(writer: T) -> Self {
        Self { writer }
    }
}


impl<T> io::Write for InstrumentedWriter<T>
where
    T: io::Write,
{
    fn write(
        &mut self,
        buf: &[u8],
    ) -> io::Result<usize> {
        self.writer.write(buf).map(MetricsControl::record_usize)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.writer.flush()
    }
}


/// Split a u64 into a tuple of (low bytes, high bytes)
#[inline(always)]
fn split_u64(value: u64) -> (u32, u32) {
    use crate::deps::cosmos_stdlib_extras::split_uint::SplitUInt;
    value.split()
}
