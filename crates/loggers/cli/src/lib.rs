//! # Common Logger Implementation for CLIs
//!
//! You can add this config directly to your other structopt arguments like so:
//!
//! ```ignore
//! #[derive(Parser)]
//! struct MyArgs {
//!     #[arg(flatten)]
//!     pub logger: cosmos_cli_logger::Args,
//! }
//!
//! fn main() {
//!     let cli = MyArgs::from_args();
//!     cli.logger.init();
//! }
//! ```
#![allow(deprecated)]
#![deny(warnings)]
#![deny(missing_docs)]
#![deny(rustdoc::missing_crate_level_docs)]
#![deny(missing_debug_implementations)]

#[doc(hidden)]
pub(crate) mod deps {
    pub use ::clap;
    pub use ::cosmos_crash_macros;
    pub use ::cosmos_logger_core;
}

use crate::deps::clap;
use crate::deps::cosmos_crash_macros::crash_on_err;
pub use crate::deps::cosmos_logger_core::Level;

/// initialize the cli logger using the config parsed
/// from command line args
#[cold]
#[inline(never)]
fn init(config: &Args) {
    use crate::deps::cosmos_logger_core::Config as LogConfig;
    let mut directives = config.log_directives.clone();

    if let Some(level) = config.log_level.to_log_level() {
        // Inject the desired log_level as the "root" directive
        directives.insert(0, level.to_string());

        crate::deps::cosmos_logger_core::init(LogConfig {
            use_env:               config.log_use_env,
            directives:            directives.as_slice().try_into().unwrap_or_else(crash_on_err!()),
            style:                 config.log_style,
            color:                 config.log_color,
            thread_ids:            false,
            thread_names:          true,
            filepaths:             false,
            line_numbers:          false,
            enable_logger_metrics: true,
            target:                config.log_to.clone(),
            metrics_target:        None,
        })
    }
}


// A straightforward log configuration for command line tools
#[allow(missing_docs)]
#[derive(Clone, Debug, clap::Parser)]
#[clap(rename_all = "kebab-case")]
pub struct Args {
    /// the effective global log level of the tool
    #[clap(long, default_value = "info")]
    pub log_level: Level,

    /// the general log line style
    #[clap(long, default_value = "default")]
    pub log_style: crate::deps::cosmos_logger_core::Style,

    /// extra log directives, you can set these with RUST_LOG but there are cases
    /// where manipulating the environment directly is more difficult without creating a launch
    /// wrapper like with containers
    #[clap(long)]
    pub log_directives: Vec<String>,

    /// toggle log colorization behavior
    #[clap(long, default_value = "auto")]
    pub log_color: crate::deps::cosmos_logger_core::Color,

    /// toggle using or ignoring RUST_LOG
    #[clap(long, default_value = "yes")]
    pub log_use_env: crate::deps::cosmos_logger_core::Environment,

    /// choose a different destination for log output. This should be either
    /// `stderr`, `stdout`, a file descriptor, or a path. For tools, note that stdout
    /// almost never makes sense since it conflicts with the output of the tool.
    #[clap(long, default_value = "stderr")]
    pub log_to: crate::deps::cosmos_logger_core::Target,
}


impl Args {
    ///  initialize the cli logger from this instance of args
    pub fn init(&self) {
        crate::init(self)
    }

    /// create an instance of [`Args`] with values created from the clap `default_value`
    /// string attributes
    pub fn with_clap_defaults() -> Self {
        use crate::deps::clap::Parser;
        Self::parse_from(std::iter::empty::<String>())
    }
}
