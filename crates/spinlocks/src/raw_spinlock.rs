use crate::deps::cosmos_intrinsics::likely;
use ::std::fmt;

use std::sync::atomic::{
    AtomicIsize,
    Ordering,
};

#[cfg(feature = "cosmos-spinlocks-debug")]
use crate::deps::tracing;
use crate::utils::{
    new_lock_id,
    CAS,
};

const LOCKED_EXCLUSIVE: isize = -1;
const UNLOCKED_EXCLUSIVE: isize = 0;
const LOAD_ORDERING: Ordering = Ordering::SeqCst;
const SUCCESS_ORDERING: Ordering = Ordering::SeqCst;
const FAIL_ORDERING: Ordering = Ordering::Relaxed;


#[cold]
fn panic_on_bad_unlock_shared() {
    panic!("there should exist a reader when calling unlock_shared");
}


#[cold]
fn panic_on_bad_unlock_exclusive() -> ! {
    panic!("there should exist a writer when trying to unlock exclusive");
}


#[derive(Copy, Clone, Debug)]
#[cfg(feature = "cosmos-spinlocks-debug")]
enum Action {
    Enter,
    Exit,
}


/// Cache padded atomic spinlock
#[repr(C, align(128))]
pub struct RawSpinLock {
    inner: AtomicIsize,
    id:    u64,
}


impl RawSpinLock {
    pub fn new() -> Self {
        RawSpinLock {
            inner: AtomicIsize::new(0),
            id:    new_lock_id(),
        }
    }

    #[inline(always)]
    #[cfg_attr(feature = "cosmos-spinlocks-debug", tracing::instrument(level = "debug"))]
    pub fn unlock_exclusive(&self) {
        let mut cas = CAS::default();

        let mut old = LOCKED_EXCLUSIVE;
        #[cfg(feature = "cosmos-spinlocks-debug")]
        debug!(action = ?Action::Enter, value = LOCKED_EXCLUSIVE);

        cas.exec(|| {
            if likely(old == LOCKED_EXCLUSIVE) {
                return match self.inner.compare_exchange_weak(
                    LOCKED_EXCLUSIVE,
                    UNLOCKED_EXCLUSIVE,
                    SUCCESS_ORDERING,
                    FAIL_ORDERING,
                ) {
                    Ok(_n) => {
                        #[cfg(feature = "cosmos-spinlocks-debug")]
                        debug!(action = ?Action::Exit, value = UNLOCKED_EXCLUSIVE);
                        true
                    }
                    Err(x) => {
                        old = x;
                        false
                    }
                };
            } else {
                panic_on_bad_unlock_exclusive()
            }
        });
    }

    #[inline(always)]
    #[cfg_attr(feature = "cosmos-spinlocks-debug", tracing::instrument(level = "debug"))]
    pub fn try_lock_exclusive(&self) -> Result<(), ()> {
        match self.inner.compare_exchange_weak(
            UNLOCKED_EXCLUSIVE,
            LOCKED_EXCLUSIVE,
            SUCCESS_ORDERING,
            FAIL_ORDERING,
        ) {
            Ok(_) => Ok(()),
            Err(_) => Err(()),
        }
    }

    #[inline(always)]
    #[cfg_attr(feature = "cosmos-spinlocks-debug", tracing::instrument(level = "debug"))]
    pub fn lock_exclusive(&self) {
        let mut cas = CAS::default();

        #[cfg(feature = "cosmos-spinlocks-debug")]
        debug!(action = ?Action::Enter, value = UNLOCKED_EXCLUSIVE);

        cas.exec(|| {
            match self.inner.compare_exchange_weak(
                UNLOCKED_EXCLUSIVE,
                LOCKED_EXCLUSIVE,
                SUCCESS_ORDERING,
                FAIL_ORDERING,
            ) {
                Ok(_n) => {
                    #[cfg(feature = "cosmos-spinlocks-debug")]
                    debug!(action = ?Action::Exit, value = LOCKED_EXCLUSIVE);
                    true
                }
                Err(_x) => false,
            }
        });
    }

    #[inline(always)]
    #[cfg_attr(feature = "cosmos-spinlocks-debug", tracing::instrument(level = "debug"))]
    pub fn unlock_shared(&self) {
        let mut cas = CAS::default();

        let mut old = self.inner.load(LOAD_ORDERING);
        #[cfg(feature = "cosmos-spinlocks-debug")]
        debug!(action = ?Action::Enter, value = old);

        cas.exec(|| {
            if likely(old > 0) {
                match self
                    .inner
                    .compare_exchange_weak(old, old - 1, SUCCESS_ORDERING, FAIL_ORDERING)
                {
                    Ok(_n) => {
                        #[cfg(feature = "cosmos-spinlocks-debug")]
                        debug!(action = ?Action::Exit, value = old - 1);
                        true
                    }
                    Err(x) => {
                        old = x;
                        false
                    }
                }
            } else {
                panic_on_bad_unlock_shared();
                return false;
            }
        })
    }

    #[inline(always)]
    #[cfg_attr(feature = "cosmos-spinlocks-debug", tracing::instrument(level = "debug"))]
    pub fn lock_shared(&self) {
        let mut cas = CAS::default();

        let mut old = self.inner.load(LOAD_ORDERING);

        #[cfg(feature = "cosmos-spinlocks-debug")]
        debug!(action = ?Action::Enter, value = old);
        cas.exec(|| {
            if likely(old >= 0) {
                match self
                    .inner
                    .compare_exchange_weak(old, old + 1, SUCCESS_ORDERING, FAIL_ORDERING)
                {
                    Ok(_n) => {
                        #[cfg(feature = "cosmos-spinlocks-debug")]
                        debug!(action = ?Action::Exit, value = old + 1);
                        true
                    }
                    Err(x) => {
                        old = x;
                        false
                    }
                }
            } else {
                old = self.inner.load(LOAD_ORDERING);
                false
            }
        });
    }

    #[inline(always)]
    pub fn try_lock_shared(&self) -> Result<(), ()> {
        let old = self.inner.load(LOAD_ORDERING);
        if likely(old >= 0) {
            let new = old + 1;
            match self
                .inner
                .compare_exchange_weak(old, new, SUCCESS_ORDERING, LOAD_ORDERING)
            {
                Ok(_) => Ok(()),
                Err(_) => Err(()),
            }
        } else {
            Err(())
        }
    }
}


impl fmt::Debug for RawSpinLock {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        f.debug_struct("Spinlock")
            .field("id", &self.id)
            .field("addr", &(self as *const _))
            .finish()
    }
}
