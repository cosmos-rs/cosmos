use crate::RawSpinLock;
use ::std::cell::UnsafeCell;
use ::std::fmt;
use ::std::ops;

#[repr(C)]
pub struct SpinLock<T: ?Sized> {
    raw:  RawSpinLock,
    data: UnsafeCell<T>,
}

impl<T> std::fmt::Debug for SpinLock<T>
where
    T: std::fmt::Debug,
{
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> std::fmt::Result {
        f.debug_struct("SpinLock")
            .field("data", unsafe { &*self.data.get() })
            .finish()
    }
}

impl<T> SpinLock<T> {
    pub fn new(val: T) -> SpinLock<T> {
        Self {
            raw:  RawSpinLock::new(),
            data: UnsafeCell::new(val),
        }
    }
}

unsafe impl<T: ?Sized + Sync> Sync for SpinLock<T> {}

unsafe impl<T: ?Sized + Send> Send for SpinLock<T> {}

/// Read Guard
pub struct SpinLockReadGuard<'a, T: ?Sized> {
    inner: &'a SpinLock<T>,
}

unsafe impl<'a, T: ?Sized + Sync + 'a> Sync for SpinLockReadGuard<'a, T> {}

impl<'a, T: ?Sized + 'a> ops::Deref for SpinLockReadGuard<'a, T> {
    type Target = T;

    #[inline(always)]
    fn deref(&self) -> &T {
        unsafe { &*self.inner.data.get() }
    }
}

impl<'a, T: ?Sized + 'a> ops::Drop for SpinLockReadGuard<'a, T> {
    #[inline(always)]
    fn drop(&mut self) {
        self.inner.unlock_exclusive();
    }
}

impl<'a, T: std::fmt::Debug + ?Sized + 'a> fmt::Debug for SpinLockReadGuard<'a, T> {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> std::fmt::Result {
        std::fmt::Debug::fmt(&**self, f)
    }
}

/// Write Guard
pub struct SpinLockWriteGuard<'a, T: ?Sized> {
    inner: &'a SpinLock<T>,
}

unsafe impl<'a, T: ?Sized + Sync + 'a> Sync for SpinLockWriteGuard<'a, T> {}

impl<'a, T: ?Sized + 'a> ops::Deref for SpinLockWriteGuard<'a, T> {
    type Target = T;

    #[inline(always)]
    fn deref(&self) -> &T {
        unsafe { &*self.inner.data.get() }
    }
}

impl<'a, T: ?Sized + 'a> ops::DerefMut for SpinLockWriteGuard<'a, T> {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.inner.data.get() }
    }
}

impl<'a, T: ?Sized + 'a> ops::Drop for SpinLockWriteGuard<'a, T> {
    #[inline(always)]
    fn drop(&mut self) {
        self.inner.unlock_exclusive();
    }
}

impl<'a, T: std::fmt::Debug + ?Sized + 'a> fmt::Debug for SpinLockWriteGuard<'a, T> {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> std::fmt::Result {
        std::fmt::Debug::fmt(&**self, f)
    }
}

impl<T: ?Sized> SpinLock<T> {
    #[inline(always)]
    unsafe fn read_guard(&self) -> SpinLockReadGuard<T> {
        SpinLockReadGuard { inner: &self }
    }

    #[inline(always)]
    unsafe fn write_guard(&self) -> SpinLockWriteGuard<T> {
        SpinLockWriteGuard { inner: &self }
    }

    #[inline(always)]
    fn unlock_exclusive(&self) {
        self.raw.unlock_exclusive()
    }

    #[inline(always)]
    fn lock_exclusive(&self) {
        self.raw.lock_exclusive()
    }

    #[inline(always)]
    pub fn read(&self) -> SpinLockReadGuard<'_, T> {
        self.lock_exclusive();
        unsafe { self.read_guard() }
    }

    #[inline(always)]
    pub fn write(&self) -> SpinLockWriteGuard<'_, T> {
        self.lock_exclusive();
        unsafe { self.write_guard() }
    }
}
