use crate::deps::cosmos_intrinsics::likely;
use ::std::fmt;
use std::sync::atomic::{
    AtomicIsize,
    Ordering,
};

#[cfg(feature = "cosmos-spinlocks-debug")]
use crate::deps::tracing;
use crate::utils::{
    new_lock_id,
    CAS,
};

const LOCKED_EXCLUSIVE: isize = -1;
const UNLOCKED_EXCLUSIVE: isize = 0;
const LOAD_ORDERING: Ordering = Ordering::SeqCst;
const SUCCESS_ORDERING: Ordering = Ordering::SeqCst;
const FAIL_ORDERING: Ordering = Ordering::Relaxed;

const WRITER_WAITING_BIT: isize = (1usize << 63) as isize;
const LOCK_VALUE_MASK: isize = !WRITER_WAITING_BIT;

#[cold]
fn panic_on_bad_unlock_shared() {
    panic!("there should exist a reader when calling unlock_shared");
}


#[cold]
fn panic_on_bad_unlock_exclusive() -> ! {
    panic!("there should exist a writer when trying to unlock exclusive");
}


#[derive(Copy, Clone, Debug)]
#[cfg(feature = "cosmos-spinlocks-debug")]
enum Action {
    Enter,
    Exit,
}


/// Spinlock
#[repr(C, align(128))]
pub struct RawSpinLockExclusivePriority {
    inner: AtomicIsize,
    id:    u64,
}


/// Decrement the shared count while preserving the writer bit status
fn decrement_shared_count(old: isize) -> isize {
    let writer_waiting_bit = old & WRITER_WAITING_BIT;
    let new_val = (old & LOCK_VALUE_MASK) - 1;

    new_val | writer_waiting_bit
}

fn extract_shared_count(value: isize) -> isize {
    value & LOCK_VALUE_MASK
}


impl RawSpinLockExclusivePriority {
    pub fn new() -> Self {
        RawSpinLockExclusivePriority {
            inner: AtomicIsize::new(0),
            id:    new_lock_id(),
        }
    }

    #[inline(always)]
    #[cfg_attr(feature = "cosmos-spinlocks-debug", tracing::instrument(level = "debug"))]
    pub fn unlock_exclusive(&self) {
        let mut cas = CAS::default();

        let mut old = LOCKED_EXCLUSIVE;
        #[cfg(feature = "cosmos-spinlocks-debug")]
        debug!(action = ?Action::Enter, value = LOCKED_EXCLUSIVE);

        cas.exec(|| {
            if likely(old == LOCKED_EXCLUSIVE) {
                return match self.inner.compare_exchange_weak(
                    LOCKED_EXCLUSIVE,
                    UNLOCKED_EXCLUSIVE,
                    SUCCESS_ORDERING,
                    FAIL_ORDERING,
                ) {
                    Ok(_n) => {
                        #[cfg(feature = "cosmos-spinlocks-debug")]
                        debug!(action = ?Action::Exit, value = UNLOCKED_EXCLUSIVE);
                        true
                    }
                    Err(x) => {
                        old = x;
                        false
                    }
                };
            } else {
                panic_on_bad_unlock_exclusive()
            }
        });
    }

    #[inline(always)]
    #[cfg_attr(feature = "cosmos-spinlocks-debug", tracing::instrument(level = "debug"))]
    pub fn try_lock_exclusive(&self) -> Result<(), ()> {
        // Don't set the writer waiting bit because we have no guarantee the caller won't "change their
        // mind" and not take the lock on failure.
        let old = self.inner.load(LOAD_ORDERING);

        // check for locks
        if (old & LOCK_VALUE_MASK) != UNLOCKED_EXCLUSIVE {
            return Err(());
        }

        match self
            .inner
            .compare_exchange_weak(old, LOCKED_EXCLUSIVE, SUCCESS_ORDERING, FAIL_ORDERING)
        {
            Ok(_) => Ok(()),
            Err(_) => Err(()),
        }
    }

    #[inline(always)]
    #[cfg_attr(feature = "cosmos-spinlocks-debug", tracing::instrument(level = "debug"))]
    pub fn lock_exclusive(&self) {
        let mut cas = CAS::default();

        // set writer waiting bit
        let _old = self.inner.fetch_or(WRITER_WAITING_BIT, LOAD_ORDERING);

        #[cfg(feature = "cosmos-spinlocks-debug")]
        debug!(action = ?Action::Enter, value = UNLOCKED_EXCLUSIVE);

        cas.exec(|| {
            match self.inner.compare_exchange_weak(
                UNLOCKED_EXCLUSIVE | WRITER_WAITING_BIT,
                LOCKED_EXCLUSIVE,
                SUCCESS_ORDERING,
                FAIL_ORDERING,
            ) {
                Ok(_n) => {
                    #[cfg(feature = "cosmos-spinlocks-debug")]
                    debug!(action = ?Action::Exit, value = LOCKED_EXCLUSIVE);
                    true
                }
                Err(_x) => false,
            }
        });
    }

    #[inline(always)]
    #[cfg_attr(feature = "cosmos-spinlocks-debug", tracing::instrument(level = "debug"))]
    pub fn unlock_shared(&self) {
        let mut cas = CAS::default();

        let mut old = self.inner.load(LOAD_ORDERING);


        #[cfg(feature = "cosmos-spinlocks-debug")]
        debug!(action = ?Action::Enter, value = old);

        cas.exec(|| {
            let new = decrement_shared_count(old);

            if likely(extract_shared_count(old) > 0) {
                match self
                    .inner
                    .compare_exchange_weak(old, new, SUCCESS_ORDERING, FAIL_ORDERING)
                {
                    Ok(_n) => {
                        #[cfg(feature = "cosmos-spinlocks-debug")]
                        debug!(action = ?Action::Exit, value = new);
                        true
                    }
                    Err(x) => {
                        old = x;
                        false
                    }
                }
            } else {
                panic_on_bad_unlock_shared();
                return false;
            }
        })
    }

    #[inline(always)]
    #[cfg_attr(feature = "cosmos-spinlocks-debug", tracing::instrument(level = "debug"))]
    pub fn lock_shared(&self) {
        let mut cas = CAS::default();

        let mut old = self.inner.load(LOAD_ORDERING);

        #[cfg(feature = "cosmos-spinlocks-debug")]
        debug!(action = ?Action::Enter, value = old);
        cas.exec(|| {
            if likely(old >= 0) {
                match self.inner.compare_exchange_weak(
                    old,
                    extract_shared_count(old) + 1,
                    SUCCESS_ORDERING,
                    FAIL_ORDERING,
                ) {
                    Ok(_n) => {
                        #[cfg(feature = "cosmos-spinlocks-debug")]
                        debug!(action = ?Action::Exit, value = old + 1);
                        true
                    }
                    Err(x) => {
                        old = x;
                        false
                    }
                }
            } else {
                old = self.inner.load(LOAD_ORDERING);
                false
            }
        });
    }

    #[inline(always)]
    pub fn try_lock_shared(&self) -> Result<(), ()> {
        let old = extract_shared_count(self.inner.load(LOAD_ORDERING));

        if likely(old >= 0) {
            match self
                .inner
                .compare_exchange_weak(old, old + 1, SUCCESS_ORDERING, LOAD_ORDERING)
            {
                Ok(_) => Ok(()),
                Err(_) => Err(()),
            }
        } else {
            Err(())
        }
    }
}


impl fmt::Debug for RawSpinLockExclusivePriority {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        f.debug_struct("Spinlock")
            .field("id", &self.id)
            .field("addr", &(self as *const _))
            .finish()
    }
}



#[cfg(test)]
mod tests {
    use super::*;
    use ::cosmos_counter::Counter;
    use ::cosmos_crash_macros::crash_on_err;
    use ::cosmos_proc_macros::cosmos;
    use std::time::Duration;

    #[cosmos(unit_test)]
    fn test_exclusive_priority_spinlock() {
        let thread_id = Counter::new();
        let spinlock = std::sync::Arc::new(RawSpinLockExclusivePriority::new());

        let mut handles = Vec::new();

        // initial readers
        handles.push({
            let lock = spinlock.clone();
            let id = thread_id.inc();

            std::thread::spawn(move || {
                debug!("[{}] try read", id);
                lock.lock_shared();
                debug!("[{}] read", id);

                std::thread::sleep(Duration::from_millis(200));

                lock.unlock_shared();
                debug!("[{}] unlock_read", id);
            })
        });
        handles.push({
            let lock = spinlock.clone();
            let id = thread_id.inc();

            std::thread::spawn(move || {
                debug!("[{}] try read", id);
                lock.lock_shared();
                debug!("[{}] read", id);

                std::thread::sleep(Duration::from_millis(200));

                lock.unlock_shared();
                debug!("[{}] unlock_read", id);
            })
        });

        // single writer
        handles.push({
            let lock = spinlock.clone();
            let id = thread_id.inc();

            std::thread::spawn(move || {
                std::thread::sleep(Duration::from_millis(10));

                debug!("[{}] try write", id);
                lock.lock_exclusive();
                debug!("[{}] write", id);

                std::thread::sleep(Duration::from_millis(200));

                lock.unlock_exclusive();
                debug!("[{}] unlock_write", id);
            })
        });

        // Contending readers
        handles.push({
            let lock = spinlock.clone();
            let id = thread_id.inc();

            std::thread::spawn(move || {
                std::thread::sleep(Duration::from_millis(5));

                debug!("[{}] try read", id);
                lock.lock_shared();
                debug!("[{}] read", id);

                std::thread::sleep(Duration::from_millis(200));

                lock.unlock_shared();
                debug!("[{}] unlock_read", id);
            })
        });
        handles.push({
            let lock = spinlock.clone();
            let id = thread_id.inc();

            std::thread::spawn(move || {
                std::thread::sleep(Duration::from_millis(10));

                debug!("[{}] try read", id);
                lock.lock_shared();
                debug!("[{}] read", id);

                std::thread::sleep(Duration::from_millis(200));

                lock.unlock_shared();
                debug!("[{}] unlock_read", id);
            })
        });
        handles.push({
            let lock = spinlock.clone();
            let id = thread_id.inc();

            std::thread::spawn(move || {
                std::thread::sleep(Duration::from_millis(20));

                debug!("[{}] try read", id);
                lock.lock_shared();
                debug!("[{}] read", id);

                std::thread::sleep(Duration::from_millis(200));

                lock.unlock_shared();
                debug!("[{}] unlock_read", id);
            })
        });
        handles.push({
            let lock = spinlock.clone();
            let id = thread_id.inc();

            std::thread::spawn(move || {
                std::thread::sleep(Duration::from_millis(25));

                debug!("[{}] try read", id);
                lock.lock_shared();
                debug!("[{}] read", id);

                std::thread::sleep(Duration::from_millis(200));

                lock.unlock_shared();
                debug!("[{}] unlock_read", id);
            })
        });
        handles.push({
            let lock = spinlock.clone();
            let id = thread_id.inc();

            std::thread::spawn(move || {
                std::thread::sleep(Duration::from_millis(23));

                debug!("[{}] try read", id);
                lock.lock_shared();
                debug!("[{}] read", id);

                std::thread::sleep(Duration::from_millis(200));

                lock.unlock_shared();
                debug!("[{}] unlock_read", id);
            })
        });


        for handle in handles {
            handle.join().unwrap_or_else(crash_on_err!());
        }

        debug!("done");
    }
}
