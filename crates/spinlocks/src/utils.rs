use crate::deps::once_cell::sync::OnceCell;
use std::sync::atomic::{
    AtomicU64,
    Ordering,
};

static LOCKID_COUNTER: OnceCell<AtomicU64> = OnceCell::new();

pub(crate) fn new_lock_id() -> u64 {
    LOCKID_COUNTER
        .get_or_init(|| AtomicU64::new(1))
        .fetch_add(1, Ordering::Relaxed)
}


pub(crate) struct CAS {
    pauses: u32,
}


impl CAS {
    const MAX_PAUSES: u32 = 1 << 10;

    #[inline(always)]
    pub fn exec(
        &mut self,
        mut f: impl (FnMut() -> bool),
    ) {
        while !(f()) {
            pause_n(self.pauses);
            self.pauses = std::cmp::min(self.pauses << 1, Self::MAX_PAUSES);
        }
    }
}

impl Default for CAS {
    #[inline(always)]
    fn default() -> Self {
        CAS { pauses: 1 }
    }
}


#[inline(always)]
pub fn pause() {
    ::core::hint::spin_loop()
}

#[inline(always)]
pub fn pause_n(n: u32) {
    for _ in 0..n {
        pause()
    }
}
