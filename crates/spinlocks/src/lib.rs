//! Spinlocks for Shared Memory Uses
#![warn(rustdoc::missing_crate_level_docs)]
#![warn(missing_debug_implementations)]
#![deny(warnings)]
#![deny(clippy::pedantic)]


mod deps {
    pub use ::cosmos_intrinsics;

    pub use ::once_cell;
}


mod raw_spinlock;
mod raw_spinlock_exclusive_priority;
mod rwspinlock;
mod spinlock;
mod utils;

pub use self::raw_spinlock::RawSpinLock;
pub use self::raw_spinlock_exclusive_priority::RawSpinLockExclusivePriority;
pub use self::rwspinlock::{
    RwSpinLock,
    RwSpinLockReadGuard,
    RwSpinLockWriteGuard,
};

pub use self::spinlock::{
    SpinLock,
    SpinLockReadGuard,
    SpinLockWriteGuard,
};



pub mod mutexes {
    pub type RawLock = super::RawSpinLock;
    pub type Lock<T> = super::SpinLock<T>;
    pub type LockReadGuard<'a, T> = super::SpinLockReadGuard<'a, T>;
    pub type LockWriteGuard<'a, T> = super::SpinLockWriteGuard<'a, T>;
}


pub mod rwlocks {
    pub use super::spin::*;
}


pub mod spin {
    pub type RawLock = super::RawSpinLock;
    pub type RawLockExclusivePriority = super::RawSpinLockExclusivePriority;
    pub type Lock<T> = super::RwSpinLock<T>;
    pub type LockReadGuard<'a, T> = super::RwSpinLockReadGuard<'a, T>;
    pub type LockWriteGuard<'a, T> = super::RwSpinLockWriteGuard<'a, T>;
}
