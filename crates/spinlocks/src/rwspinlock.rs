use crate::RawSpinLock;
use ::std::cell::UnsafeCell;
use ::std::fmt;
use ::std::ops;

/// This lock is in no way fair
#[repr(C)]
pub struct RwSpinLock<T: ?Sized> {
    raw:  RawSpinLock,
    data: UnsafeCell<T>,
}

impl<T> std::fmt::Debug for RwSpinLock<T>
where
    T: std::fmt::Debug,
{
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> std::fmt::Result {
        f.debug_struct("RwSpinLock")
            .field("data", unsafe { &*self.data.get() })
            .finish()
    }
}

impl<T> RwSpinLock<T> {
    pub fn new(val: T) -> RwSpinLock<T> {
        Self {
            raw:  RawSpinLock::new(),
            data: UnsafeCell::new(val),
        }
    }
}

unsafe impl<T: ?Sized + Sync> Sync for RwSpinLock<T> {}

unsafe impl<T: ?Sized + Send> Send for RwSpinLock<T> {}

/// Read Guard
pub struct RwSpinLockReadGuard<'a, T: ?Sized> {
    inner: &'a RwSpinLock<T>,
}

unsafe impl<'a, T: ?Sized + Sync + 'a> Sync for RwSpinLockReadGuard<'a, T> {}

impl<'a, T: ?Sized + 'a> ops::Deref for RwSpinLockReadGuard<'a, T> {
    type Target = T;

    #[inline(always)]
    fn deref(&self) -> &T {
        unsafe { &*self.inner.data.get() }
    }
}

impl<'a, T: ?Sized + 'a> ops::Drop for RwSpinLockReadGuard<'a, T> {
    #[inline(always)]
    fn drop(&mut self) {
        self.inner.unlock_shared();
    }
}

impl<'a, T: std::fmt::Debug + ?Sized + 'a> std::fmt::Debug for RwSpinLockReadGuard<'a, T> {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> std::fmt::Result {
        std::fmt::Debug::fmt(&**self, f)
    }
}

/// Write Guard
pub struct RwSpinLockWriteGuard<'a, T: ?Sized> {
    inner: &'a RwSpinLock<T>,
}

unsafe impl<'a, T: ?Sized + Sync + 'a> Sync for RwSpinLockWriteGuard<'a, T> {}

impl<'a, T: ?Sized + 'a> ops::Deref for RwSpinLockWriteGuard<'a, T> {
    type Target = T;

    #[inline(always)]
    fn deref(&self) -> &T {
        unsafe { &*self.inner.data.get() }
    }
}

impl<'a, T: ?Sized + 'a> ops::DerefMut for RwSpinLockWriteGuard<'a, T> {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.inner.data.get() }
    }
}

impl<'a, T: ?Sized + 'a> ops::Drop for RwSpinLockWriteGuard<'a, T> {
    #[inline(always)]
    fn drop(&mut self) {
        self.inner.unlock_exclusive();
    }
}

impl<'a, T: std::fmt::Debug + ?Sized + 'a> fmt::Debug for RwSpinLockWriteGuard<'a, T> {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> std::fmt::Result {
        std::fmt::Debug::fmt(&**self, f)
    }
}


impl<T: ?Sized> RwSpinLock<T> {
    #[inline(always)]
    pub fn read(&self) -> RwSpinLockReadGuard<'_, T> {
        self.lock_shared();
        unsafe { self.read_guard() }
    }

    #[inline(always)]
    pub fn write(&self) -> RwSpinLockWriteGuard<'_, T> {
        self.lock_exclusive();
        unsafe { self.write_guard() }
    }

    #[inline(always)]
    unsafe fn read_guard(&self) -> RwSpinLockReadGuard<T> {
        RwSpinLockReadGuard { inner: &self }
    }

    #[inline(always)]
    unsafe fn write_guard(&self) -> RwSpinLockWriteGuard<T> {
        RwSpinLockWriteGuard { inner: &self }
    }

    #[inline(always)]
    fn unlock_shared(&self) {
        self.raw.unlock_shared()
    }

    #[inline(always)]
    fn lock_shared(&self) {
        self.raw.lock_shared()
    }

    #[inline(always)]
    fn unlock_exclusive(&self) {
        self.raw.unlock_exclusive()
    }

    #[inline(always)]
    fn lock_exclusive(&self) {
        self.raw.lock_exclusive()
    }
}
