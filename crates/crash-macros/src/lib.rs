//! # Cosmos Crash Macros - Highly Visible Panicking
//!
//! Highly visible, greppable, readable, alternatives to `panic!()` that work well with both
//! `Option::unwrap_or_else()` and `Result::unwrap_or_else()` and will add specific information
//! about the process, thread, location of the unwrap(). This location info was not available with
//! vanilla unwrap until rust 1.46. The vanilla unwraps still use the [`std::fmt::Debug`]
//! formatting in the error messages rather than the [`std::fmt::Display`].
//!
//!   ```text
//!      thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: Os { code: 2,
//!       kind:NotFound, message: "No such file or directory" }', src/main.rs:159:35   
//!   ```
//!
//!   vs
//!
//!   ```text
//!      thread 'main' panicked at:
//!      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!
//!      CRASHING[100-1]: src/main.rs:L#159.35: No such file or directory (os error 2)
//!
//!      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!   ```
#![deny(clippy::pedantic)]
#![deny(warnings)]
#![deny(rustdoc::missing_crate_level_docs)]
#![deny(missing_docs)]

#[doc(hidden)]
mod deps {
    pub use ::static_assertions as sa;
    pub use ::tracing as log;
}

#[doc(hidden)]
pub mod private {
    pub use crate::deps::log::warn;
    pub use ::cosmos_counter::CounterU64 as Counter;
    pub use ::cosmos_intrinsics;
    pub use ::once_cell::sync::OnceCell;
    pub use ::parking_lot;

    /// # Panics
    ///
    /// This function always panics and is meant to be a common entrypoint and panic message
    /// formatter.
    #[cold]
    #[inline(never)]
    #[track_caller]
    pub fn crashing(
        file: &str,
        line: u32,
        message: Option<&str>,
    ) -> ! {
        crate::deps::log::error!(
            "beginning crash; process={}; thread={}; source={}:L#{}; message={}",
            ::std::process::id(),
            crate::private::thread_id(),
            file,
            line,
            message.unwrap_or("unspecified error")
        );

        panic!(
            r#"
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

CRASHING[{}-{}]: {}:L#{}: {}

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"#,
            ::std::process::id(),
            crate::private::thread_id(),
            file,
            line,
            message.unwrap_or("unspecified error")
        );
    }


    #[cfg(target_family = "unix")]
    #[inline(always)]
    fn thread_id() -> u64 {
        crate::deps::sa::assert_eq_size!(::std::thread::ThreadId, u64);
        unsafe { std::mem::transmute(std::thread::current().id()) }
    }
}


/// A better way to panic. See crate documentation.
#[macro_export]
macro_rules! crash {
       () => {{
            $crate::private::crashing(file!(), line!(), None)
        }};
        ($msg:expr) => {{
            $crate::private::crashing(file!(), line!(), Some(stringify!($msg)))
        }};
        ($msg:expr,) => {{
            $crate::private::crashing(file!(), line!(), Some(stringify!($msg)))
        }};
        ($fmt:expr, $($arg:tt)*) => {{
            $crate::private::crashing(file!(), line!(), Some(&format!("{}", format_args!($fmt, $($arg)*))))
        }};
    }

/// A conventional way to unwrap_or_else-ing `Option<T>` without the manual closure building.
///
/// Uses the same convention as `crash!()` to create a closure else handler with the default
/// conventions and formatting to provide the context info about the site of the crash.
///
/// ```ignore
/// 
/// let count_of_a = count_for_kind.get(Kind::A)
///     .unwrap_or_else(crash_on_none!());
/// ```
///
/// This can be easier to reason about and refactor code that mixes unwrapping `Result<T,E>`
/// which requires a closure that takes an `E` parameter an the `Option<T>` which requires a
/// a parameter-less closure.
#[macro_export]
macro_rules! crash_on_none {
        ($fmt:expr, $($arg:tt)*) => {{
            || $crate::crash!($fmt, $($arg)*)
        }};
        ($msg:expr,) => {{
         || $crate::crash!($msg)
        }};
        ($msg:expr) => {{
            $crate::crash_on_none!($msg,)
        }};
        () => {{
            $crate::crash_on_none!("a required value to continue was missing (None)")
        }};
    }

/// A conventional way to unwrap_or_else-ing `Result<T, E>` without the manual closure building.
///
/// Uses the same convention as `crash!()` to create a closure else handler with the default
/// conventions and formatting to provide the context info about the site of the crash. This
/// macro automatically includes the error in the `crash!()` message.
///
/// ```ignore
/// let parsed_value = u32::try_from(value)
///     .unwrap_or_else(crash_on_err!("could not parse {} into a u32", value));
/// ```
///
/// This can be easier to reason about and refactor code that mixes unwrapping `Result<T,E>`
/// which requires a closure that takes an `E` parameter an the `Option<T>` which requires a
/// a parameter-less closure.
#[macro_export]
macro_rules! crash_on_err {
        ($fmt:expr, $($arg:tt)*) => {{
          |err| $crate::crash!("{} - error: {:?}", format_args!($fmt, $($arg)*), err)
        }};
        ($msg:expr,) => {{
         |err| $crate::crash!("{} - error: {:?}", $msg, err)
        }};
        ($msg:expr) => {{
            $crate::crash_on_err!($msg,)
        }};
        () => {{
            $crate::crash_on_err!("unrecoverable error encountered")
        }};
    }

/// A `crash!()` formatted alternative to `assert!(expr)`
#[macro_export]
macro_rules! crash_if_not {
        ($cond:expr) => {{
            if $crate::private::cosmos_intrinsics::unlikely(!$cond) {
                $crate::crash!("crash_if_not assertion failed! ({})", stringify!($cond))
            }
        }};
        ($cond:expr,) => {{
            $crate::crash_if_not!($cond )
        }};
        ($cond:expr, $($arg:tt)+) => {{
            if $crate::private::cosmos_intrinsics::unlikely(!$cond) {
                $crate::crash!(r#"crash_if_not assertion failed! `({})` {}"#, stringify!($cond), format_args!($($arg)*))
            }
      }};
    }

/// A `crash!()` formatted alternative to `assert!(!expr)`
#[macro_export]
macro_rules! crash_if {
        ($cond:expr) => {{
            if $crate::private::cosmos_intrinsics::unlikely($cond) {
                $crate::crash!("assertion failed! `crash_if({})", stringify!($cond))
            }
        }};
        ($cond:expr,) => {{
            $crate::crash_if!($cond )

        }};
        ($cond:expr, $($arg:tt)+) => {{
                    if $crate::private::cosmos_intrinsics::unlikely($cond) {

            $crate::crash!(r#"assertion failed! `crash_if({})` {}"#, stringify!($cond), format_args!($($arg)*))
        }
      }};
    }

/// A `crash!()` formatted alternative to `assert_eq!(a, b)`
/// Note: *style taken from the stdlib asserts*
#[macro_export]
macro_rules! crash_if_not_equal {
    ($left:expr, $right:expr) => ({
        match (&$left, &$right) {
            (left_val, right_val) => {
                if $crate::private::cosmos_intrinsics::unlikely(!(*left_val == *right_val)) {
                    // The reborrows below are intentional. Without them, the stack slot for the
                    // borrow is initialized even before the values are compared, leading to a
                    // noticeable slow down.
                    $crate::crash!(r#"assertion failed! `crash_if_not_equal({}, {})`
  left: `{:?}`,
 right: `{:?}`"#, stringify!($left), stringify!($right), &*left_val, &*right_val)
                }
            }
        }
    });
    ($left:expr, $right:expr,) => ({
        crash_if_not_equal!($left, $right)
    });
    ($left:expr, $right:expr, $($arg:tt)+) => ({
        match (&($left), &($right)) {
            (left_val, right_val) => {
                if $crate::private::cosmos_intrinsics::unlikely(!(*left_val == *right_val)) {
                    // The reborrows below are intentional. Without them, the stack slot for the
                    // borrow is initialized even before the values are compared, leading to a
                    // noticeable slow down.
                    $crate::crash!(r#"assertion failed! `crash_if_not_equal({}, {})`
  left: `{:?}`,
 right: `{:?}`: {}"#,stringify!($left), stringify!($right), &*left_val, &*right_val,
                           format_args!($($arg)+))
                }
            }
        }
    });
}

/// A `crash!()` formatted alternative to `assert_eq!(a, b)`
/// Note: *style taken from the stdlib asserts*
#[macro_export]
macro_rules! crash_if_equal {
    ($left:expr, $right:expr) => ({
        match (&$left, &$right) {
            (left_val, right_val) => {
                if $crate::private::cosmos_intrinsics::unlikely((*left_val == *right_val)) {
                    // The reborrows below are intentional. Without them, the stack slot for the
                    // borrow is initialized even before the values are compared, leading to a
                    // noticeable slow down.
                    $crate::crash!(r#"state assertion failed! `crash_if_equal({}, {})`
  left: `{:?}`,
 right: `{:?}`"#, stringify!($left), stringify!($right), &*left_val, &*right_val)
                }
            }
        }
    });
    ($left:expr, $right:expr,) => ({
        crash_if_equal!($left, $right)
    });
    ($left:expr, $right:expr, $($arg:tt)+) => ({
        match (&($left), &($right)) {
            (left_val, right_val) => {
                if $crate::private::cosmos_intrinsics::unlikely((*left_val == *right_val)) {
                    // The reborrows below are intentional. Without them, the stack slot for the
                    // borrow is initialized even before the values are compared, leading to a
                    // noticeable slow down.
                    $crate::crash!(r#"state assertion failed! `crash_if_equal({}, {})`
  left: `{:?}`,
 right: `{:?}`: {}"#,stringify!($left), stringify!($right), &*left_val, &*right_val,
                           format_args!($($arg)+))
                }
            }
        }
    });
}


/// A `crash!()` formatted alternative to `assert_matches!(expr, pattern)`
/// Note: *style taken from the stdlib asserts*
#[macro_export]
macro_rules! crash_if_not_matches {
    ($left:expr, $(|)? $( $pattern:pat_param )|+ $( if $guard: expr )? $(,)?) => ({
        match $left {
            $( $pattern )|+ $( if $guard )? => {}
            ref left_val => {
                $crate::crash!(
                    r#"match assertion failed! `crash_if_not_matches!({}, {})` (value: {:?})"#,
                    stringify!($left), stringify!($($pattern)|+ $(if $guard)?), left_val
                );
            }
        }
    });
    ($left:expr, $(|)? $( $pattern:pat_param )|+ $( if $guard: expr )?, $($arg:tt)+) => ({
        match $left {
            $( $pattern )|+ $( if $guard )? => {}
            ref left_val => {
                $crate::crash!(
                    r#"match assertion failed! `crash_if_not_matches!({}, {})` (value: {:?}): {}"#,
                    stringify!($left), stringify!($($pattern)|+ $(if $guard)?), left_val,
                    format_args!($($arg)+)
                );
            }
        }
    });
}

/// Always warn about the state assertion failure with the number of times the assertion has failed.
/// Does not panic.
#[macro_export]
macro_rules! warn_if {
    ($cond:expr, $($arg:tt)+) => {{
        if $crate::private::cosmos_intrinsics::unlikely($cond) {
            static COUNTER: $crate::private::OnceCell<$crate::private::Counter> = $crate::private::OnceCell::new();
            $crate::private::warn!(
                failures=COUNTER.get_or_init(|| $crate::private::Counter::with_start(1)).inc(),
                message=?format_args!($($arg)+),
                r#"state assertion would have failed! `crash_if({})`"#,
                stringify!($cond),
            );
        }
    }};
    ($cond:expr$(,)?) => {{
        if $crate::private::cosmos_intrinsics::unlikely($cond) {
            static COUNTER: $crate::private::OnceCell<$crate::private::Counter> = $crate::private::OnceCell::new();
            $crate::private::warn!(
                failures=COUNTER.get_or_init(|| $crate::private::Counter::with_start(1)).inc(),
                r#"state assertion would have failed! `crash_if({})`"#,
                stringify!($cond),
            );
        }
    }};
}

/// Always warn about the state assertion failure with the number of times the assertion has failed.
/// Does not panic.
#[macro_export]
macro_rules! warn_if_not {
    ($cond:expr, $($arg:tt)+) => {{
        if $crate::private::cosmos_intrinsics::unlikely(!($cond)) {
            static COUNTER: $crate::private::OnceCell<$crate::private::Counter> = $crate::private::OnceCell::new();
            $crate::private::warn!(
                failures=COUNTER.get_or_init(|| $crate::private::Counter::with_start(1)).inc(),
                message=?format_args!($($arg)+),
                r#"state assertion would have failed! `crash_if_not({})`"#,
                stringify!($cond),
            );
        }
    }};
    ($cond:expr$(,)?) => {{
        if $crate::private::cosmos_intrinsics::unlikely(!($cond)) {
            static COUNTER: $crate::private::OnceCell<$crate::private::Counter> = $crate::private::OnceCell::new();
            $crate::private::warn!(
                failures=COUNTER.get_or_init(|| $crate::private::Counter::with_start(1)).inc(),
                r#"state assertion would have failed! `crash_if_not({})`"#,
                stringify!($cond),
            );
        }
    }};
}

/// Always warn about the state assertion failure with the number of times the assertion has failed.
/// Does not panic.
#[macro_export]
macro_rules! warn_if_eq {
    ($left:expr, $right:expr) => {{
        match (&$left, &$right) {
            (left_val, right_val) => {
                if $crate::private::cosmos_intrinsics::unlikely((*left_val == *right_val)) {
                    static COUNTER: $crate::private::OnceCell<$crate::private::Counter> =
                        $crate::private::OnceCell::new();

                    // The reborrows below are intentional. Without them, the stack slot for the
                    // borrow is initialized even before the values are compared, leading to a
                    // noticeable slow down.
                    $crate::private::warn!(
                        failures = COUNTER
                            .get_or_init(|| $crate::private::Counter::with_start(1))
                            .inc(),
                        r#"state assertion would have failed! `crash_if_equal({}, {})`
  left: `{:?}`,
 right: `{:?}`"#,
                        stringify!($left),
                        stringify!($right),
                        &*left_val,
                        &*right_val
                    );
                }
            }
        }
    }};
}

/// Always warn about the state assertion failure with the number of times the assertion has failed.
/// Does not panic.
#[macro_export]
macro_rules! warn_if_not_eq {
    ($left:expr, $right:expr) => {{
        match (&$left, &$right) {
            (left_val, right_val) => {
                if $crate::private::cosmos_intrinsics::unlikely(!(*left_val == *right_val)) {
                    static COUNTER: $crate::private::OnceCell<$crate::private::Counter> =
                        $crate::private::OnceCell::new();

                    // The reborrows below are intentional. Without them, the stack slot for the
                    // borrow is initialized even before the values are compared, leading to a
                    // noticeable slow down.
                    $crate::private::warn!(
                        failures = COUNTER
                            .get_or_init(|| $crate::private::Counter::with_start(1))
                            .inc(),
                        r#"state assertion would have failed! `crash_if_not_equal({}, {})`
  left: `{:?}`,
 right: `{:?}`"#,
                        stringify!($left),
                        stringify!($right),
                        &*left_val,
                        &*right_val
                    );
                }
            }
        }
    }};
}


/// A `crash!()` formatted alternative to `debug_assert!(expr)`
#[macro_export]
macro_rules! dbg_crash_if_not {
    ($cond:expr) => {{
        if cfg!(debug_assertions) && $crate::private::cosmos_intrinsics::unlikely(!$cond) {
            $crate::crash!("dbg_crash_if_not assertion failed! ({})", stringify!($cond))
        }
        $crate::warn_if_not!($cond);
    }};
    ($cond:expr,) => {{
        $crate::dbg_crash_if_not!($cond )
    }};
    ($cond:expr, $($arg:tt)+) => {{
        if cfg!(debug_assertions) && $crate::private::cosmos_intrinsics::unlikely(!$cond) {
            $crate::crash!(r#"dbg_crash_if_not assertion failed! `({})` {}"#, stringify!($cond), format_args!($($arg)*))
        }
        $crate::warn_if_not!($cond, $($arg)*);
    }};
}

/// A `crash!()` formatted alternative to `assert!(!expr)`
#[macro_export]
macro_rules! dbg_crash_if {
    ($cond:expr) => {{
        if cfg!(debug_assertions) && $crate::private::cosmos_intrinsics::unlikely($cond) {
            $crate::crash!("assertion failed! `dbg_crash_if({})", stringify!($cond))
        }
        $crate::warn_if!($cond);
    }};
    ($cond:expr,) => {{
        $crate::dbg_crash_if!($cond )

    }};
    ($cond:expr, $($arg:tt)+) => {{
        if cfg!(debug_assertions) && $crate::private::cosmos_intrinsics::unlikely($cond) {
            $crate::crash!(r#"assertion failed! `dbg_crash_if({})` {}"#, stringify!($cond), format_args!($($arg)*))
        }
        $crate::warn_if!($cond, $($arg)*);
    }};
}


/// A `crash!()` formatted alternative to `assert!(expr)`
#[macro_export]
macro_rules! dbg_crash_if_not_equal {
    ($($arg:tt)+) => {{
        if cfg!(debug_assertions)  {
            $crate::crash_if_not_equal!($($arg)*)
        }
        $crate::warn_if_not_eq!($($arg)*);
    }};
}

/// A `crash!()` formatted alternative to `debug_assert_eq!(expr)`
#[macro_export]
macro_rules! dbg_crash_if_not_eq {
    ($($arg:tt)+) => {{
        $crate::dbg_crash_if_not_equal!($($arg)*)
    }};
}

/// A `crash!()` formatted alternative to `assert!(!expr)`
#[macro_export]
macro_rules! dbg_crash_if_equal {
    ($($arg:tt)+) => {{
        if cfg!(debug_assertions)  {
            $crate::crash_if_equal!($($arg)*)
        }
        $crate::warn_if_eq!($($arg)*);
    }};
}

/// A `crash!()` formatted alternative to `assert!(expr)`
#[macro_export]
macro_rules! dbg_crash_if_eq {
    ($($arg:tt)+) => {{
        $crate::dbg_crash_if_equal!($($arg)*)
    }};
}


/// Evaluate the expression of the assertion exactly once at runtime. This can be used to
/// evaluate logically static expressions at runtime the first time the expression is
/// evaluated potentially reducing the overhead of evaluating the expression of the
/// assertion which is replaced with a simple integer comparison.
#[macro_export]
macro_rules! assert_once {
($name:ident; $($arg:tt)*) => {{
    static $name: $crate::private::parking_lot::Once = $crate::private::parking_lot::Once::new();

    $name.call_once(|| {
        ::std::assert!($($arg)*)
    })
}};
}

/// Evaluate the expression of the assertion exactly once at runtime. This can be used to
/// evaluate logically static expressions at runtime the first time the expression is
/// evaluated potentially reducing the overhead of evaluating the expression of the
/// assertion which is replaced with a simple integer comparison.
#[macro_export]
macro_rules! assert_once_eq {
($name:ident; $($arg:tt)*) => {{
    static $name: $crate::private::parking_lot::Once = $crate::private::parking_lot::Once::new();


    $name.call_once(|| {
        assert_eq!($($arg)*)
    })

}};
}

/// Evaluate the expression of the assertion exactly once at runtime. This can be used to
/// evaluate logically static expressions at runtime the first time the expression is
/// evaluated potentially reducing the overhead of evaluating the expression of the
/// assertion which is replaced with a simple integer comparison.
#[macro_export]
macro_rules! assert_once_ne {
($name:ident; $($arg:tt)*) => {{
    static ref $name: $crate::private::parking_lot::Once = $crate::private::parking_lot::Once::new();

    $name.call_once(|| {
        assert_ne!($($arg)*)
    })

}};
}

/// Evaluate the expression of the assertion exactly once at runtime if and only if debug
/// assertions are enabled. This can be used to evaluate logically static expressions at
/// runtime the first time the expression is evaluated potentially reducing the overhead of
/// evaluating the expression of the assertion which is replaced with a simple integer
/// comparison.
#[macro_export]
macro_rules! debug_assert_once {
($name:ident; $($arg:tt)*) => {{
    if cfg!(debug_assertions) {
        use $crate::assert_once;
        assert_once!($name; $($arg)*)
    }
}};
}

/// Evaluate the expression of the assertion exactly once at runtime if and only if debug
/// assertions are enabled. This can be used to evaluate logically static expressions at
/// runtime the first time the expression is evaluated potentially reducing the overhead of
/// evaluating the expression of the assertion which is replaced with a simple integer
/// comparison.
#[macro_export]
macro_rules! debug_assert_once_eq {
($name:ident; $($arg:tt)*) => {{
    if cfg!(debug_assertions) {
        use $crate::assert_once_eq;
        assert_once_eq!($name; $($arg)*)
    }
}};
}


/// Evaluate the expression of the assertion exactly once at runtime if and only if debug
/// assertions are enabled. This can be used to evaluate logically static expressions at
/// runtime the first time the expression is evaluated potentially reducing the overhead of
/// evaluating the expression of the assertion which is replaced with a simple integer
/// comparison.
#[macro_export]
macro_rules! debug_assert_once_ne {
($name:ident; $($arg:tt)*) => {{
    if cfg!(debug_assertions) {
        use $crate::assert_once_ne;
        assert_once_ne!($name; $($arg)*)
    }
}};
}
