use cosmos_crash_macros::{
    crash,
    crash_on_err,
    crash_on_none,
};

#[test]
#[should_panic]
fn crash_macro_causes_panic() {
    crash!("boom, it died")
}


#[test]
#[should_panic]
fn crash_on_err_causes_panic() {
    let err: Result<(), &'static str> = Err("I am an error that you caused");
    err.unwrap_or_else(crash_on_err!(
        "and this error message can be used to add context, the error is included for free!"
    ));
}


#[test]
#[should_panic]
fn crash_on_none_causes_panic() {
    let err: Option<()> = None;
    err.unwrap_or_else(crash_on_none!(
        "and this error message can be used to add context about what whs none."
    ));
}
