#!/usr/bin/env bash

# Wrapper around cargo to place build artifacts in a non-snapshotted btrfs subvolume

set -xeuo pipefail

volatile_data_root="/volatile-data"
build_dir=build
private_dir=private
cargo_target_dir="${private_dir}/cargo-target"

this-file() (
  local filename;
  local path;

  filename="$(basename "${BASH_SOURCE[0]}")"
  [[ $? -eq 0 ]] || return 1

  path="$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && (pwd --physical 2> /dev/null || pwd))"
  [[ $? -eq 0 ]] || return 1

  echo "${path}/${filename}"
)

workspace-root() {
  local filepath;

  filepath="$(this-file)"
  [[ $? -eq 0 ]] || return 1

  dirname "$(dirname "${filepath}")"
}

# This function will create a source info file at build/private/source-info.txt
#
create-source-info-file() {
    private_build_dir="$1"
    mkdir -p "${private_build_dir}"

    local event_id="${COSMOS_BUILD_EVENT_ID:-}"
    local -r commit_date="$(git log -1 --date=short --pretty=format:%cd || echo -n "" )"
    local commit_hash="$(git rev-parse --short HEAD || echo -n "x" )"

    if [[ "${commit_hash}" == "x" ]] && [[ -n "${event_id}" ]] ; then
        commit_hash="${event_id:0:12}"
    fi

    cat <<EOF | tee "${private_build_dir}/source-info.txt"
${commit_hash}
${commit_date}
EOF

}

create-volatile-cache() {

  if [[ ! -d "${volatile_data_root}" ]]; then
    echo "${volatile_data_root} does not exist, assuming no-op" 1>&2
    return 0
  fi

  local -r real_path="$1"
  local -r absolute_real_path="$(readlink -f "$(pwd)")/${real_path}"

  local -r volatile_path="/volatile-data/build-cache${absolute_real_path}"

  if [[ -e "${real_path}" ]] ; then  # if real_path exists
    if [[ -L "${real_path}" ]] ; then # if real_path is a symlink
      # resolve the symlink into an absolute abs_path
      local -r abs_path="$(readlink -f "${real_path}")"

      # does the symlink point to the proposed build cache already? no problem
      if [[ "${abs_path}" == ${volatile_path} ]]; then
        echo "already linked: ${real_path} -> ${volatile_path}" >&2
        return 0
      else # it is a symlink but it points some place that we did not expect, fail
        echo "already linked to another location: ${real_path} -> ${abs_path}" >&2
        return 1
      fi
    else # real_path is a non-symlink file
      echo "${real_path}: already exists and is not a symlink" >&2
      return 1
    fi
  fi

  # make the volatile cache directory
  mkdir -pv "${volatile_path}"
  # symlink it to the desired location
  ln -s "${volatile_path}" "${real_path}"
}


main() {

  if [[ -d "${volatile_data_root}" ]]; then
    # The /volatile-data directory exists so assume we want to use that as the build cache
    # since on dev systems configured with btrfs it is a non-snapshotted subvolume
    # which reduces disk space of periodic snapshots
    create-volatile-cache "${build_dir}"
    build_abspath="$(readlink -f "${build_dir}")"
    mkdir -p "${build_abspath}/${private_dir}"
    mkdir -p "${build_abspath}/${cargo_target_dir}"
    [[ -e target ]] || ln -s "${build_abspath}/${cargo_target_dir}" target
  else
    # otherwise fallback to using a real directory in the cwd
    mkdir -p "${build_dir}"
    build_abspath="$(readlink -f "${build_dir}")"
  fi

  create-source-info-file "${build_abspath}/${private_dir}"
  export CARGO_TARGET_DIR="${build_abspath}/${cargo_target_dir}"
  COSMOS_WORKSPACE_ROOT="$(workspace-root)"
  export COSMOS_WORKSPACE_ROOT

  exec cargo "$@"
}



main "$@"
